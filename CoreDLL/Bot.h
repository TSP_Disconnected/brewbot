#pragma once
#include <Windows.h>
#include "stdafx.h"
#include <TlHelp32.h>
#include <time.h>
#include <atlstr.h>
#include "Utility.h"
#include "Misc.h"
#include <thread>
#include <map>

#define BASE_UI_Y 302
#define BASE_UI_X 20
#define MAX_UI_X 470
#define MAX_UI_Y 495

typedef char* (__stdcall *ImageSearchHWND)(HWND, int, int, int, int, char *); //Searches HWND
typedef char* (__stdcall* ImageSearchBMP)(int aLeft, int aTop, int aRight, int aBottom, HBITMAP hbitmap_screen2, char* srcImageFile); //Searches HWND
typedef char* (__stdcall *ImageSearchE)(int x, int y, int w, int h, char *searchImg, char *destImage); //Searches External Image(Image File) and returns first result
typedef std::vector<ImgXY>(__stdcall *ImageSearchEMR)(int x, int y, int w, int h, char *searchImg, char *destImage); //Searches External Image and returns ALL Results

struct ThreadInfo {
	std::string functionName;
	int threadID = -1;
	bool shouldRun = false;

	ThreadInfo(std::string function, int id, bool loop) {
		functionName = function;
		threadID = id;
		shouldRun = loop;
	}
};

struct WindowData {
	HWND h;
	std::string className;
};

struct WindowEnum {
	std::vector<WindowData> windows;
	DWORD procID;
};

struct UIElementS {
	int x;
	int y;
	int width;
	int height;
	int type;
	int id;
	std::string text;
};

struct Bot {
public:
	//ADB Variables
	HANDLE adbInRead;
	HANDLE adbInWrite;
	HANDLE adbOutRead;
	HANDLE adbOutWrite;
	PROCESS_INFORMATION adbInfo;
	PROCESS_INFORMATION recordInfo;

	//Global Script Variable
	std::map<std::string, std::string> globalVar;
	std::mutex globalMutex;

	//Main GUI Window
	HWND mainWin;

	//Image variables
	ImageSearchHWND pImageSearch;
	ImageSearchBMP pImageSearchBMP;
	ImageSearchE pImageSearchEx;
	ImageSearchEMR pImageSearchExM;

	//UI Variables
	std::vector<int> UIEventQueue;
	std::mutex EventMutex;
	std::vector<int> UIElements;
	std::mutex UIElementMutex;
	std::vector<UIElementS> UIElementsQueue;
	std::mutex UIElementsQueueMutex;

	//Bot created threads
	std::vector<ThreadInfo> botThreads;
	int threadIDCounter = 0;
	std::mutex ThreadMutex;
	std::mutex ADBComMutex;

	//Timer variables
	int runTimeSec = 0;
	int runTimeMin = 0;
	int runTimeHrs = 0;

	int w = 900;
	int h = 900;

	int IDC_CONSOLE = FB_ID_CONSOLE;
	int defaultTol = 5;

	bool startHasRun = false;

	int elementIDOffset = 5000;

	int adbPort = 0;

	bool isRunning = true;
	bool isPlaying = false;
	bool isDebug = false;
	bool isLocal;

	std::string botID;

	std::wstring bsInstallDir;

	HINSTANCE imageDLL;

	//Other
	int PlatformID = -1;

	enum PLATFORM {
		PLATFORM_BLUESTACKS1,
		PLATFORM_BLUESTACKS2,
		PLATFORM_BLUESTACKS3,
		PLATFORM_NOX
	};

	enum CONTROLS {
		LABEL,
		BUTTON,
		CHECKBOX,
		RADIO_BUTTON
	};

	void setStatusBar(std::string msg) {
		HWND hWndStatusBar = GetDlgItem(mainWin, 157);
		SendMessageA(hWndStatusBar, SB_SETTEXTA, 0, (LPARAM)msg.c_str());
	}

	std::string getRunTimeStr() {
		std::string second;
		std::string minute;
		if (runTimeSec < 10)
			second = "0" + std::to_string(runTimeSec);
		else
			second = std::to_string(runTimeSec);

		if (runTimeMin < 10)
			minute = "0" + std::to_string(runTimeMin);
		else
			minute = std::to_string(runTimeMin);

		std::string runTime = std::to_string(runTimeHrs) + ":" + minute + ":" + second;

		return runTime;
	}

	void addSecond() {
		if (!getPlaying())
			return;
		runTimeSec += 1;
		if (runTimeSec >= 60) {
			runTimeSec = 0;
			runTimeMin += 1;
		}
		if (runTimeMin >= 60) {
			runTimeMin = 0;
			runTimeHrs += 1;
		}
	}

	static BOOL CALLBACK enumWindowsProc(
		__in  HWND hWnd,
		__in  LPARAM lParam
	) {

		WindowEnum& windows =
			*reinterpret_cast<WindowEnum*>(lParam);

		DWORD procID;
		DWORD procRet = GetWindowThreadProcessId(hWnd, &procID);

		if (procID != windows.procID)
			return TRUE;

		WindowData we;
		we.className = "";
		we.h = NULL;
		char buffer[512];
		int length = GetClassNameA(hWnd, buffer, sizeof(buffer));

		std::string windowTitle = std::string(buffer);

		we.className = windowTitle;
		we.h = hWnd;

		windows.windows.push_back(we);
		//wcout << hWnd << TEXT(": ") << windowTitle << std::endl;

		return TRUE;
	}

	void setGlobal(std::string key, std::string val) {
		globalMutex.lock();
		
		globalVar[key] = val;

		globalMutex.unlock();
	}

	std::string getGlobal(std::string key) {
		globalMutex.lock();
		std::string ret;
		if(globalVar.count(key) > 0)
			ret = globalVar.at(key);

		globalMutex.unlock();
		return ret;

	}

	//Initial functions
	void init(HWND h, std::string id, bool local) {
		imageDLL = LoadLibraryA("ImageSearchDLL.dll");
		if (imageDLL) {
			pImageSearch = (ImageSearchHWND)GetProcAddress(imageDLL, "ImageSearch");
			pImageSearchEx = (ImageSearchE)GetProcAddress(imageDLL, "ImageSearchEx");
			pImageSearchExM = (ImageSearchEMR)GetProcAddress(imageDLL, "ImageSearchExMult");
			pImageSearchBMP = (ImageSearchBMP)GetProcAddress(imageDLL, "ImageSearchBMP");
		}
		else {
			MessageBoxA(NULL, "Failed to load Image DLL", "ERROR", NULL);
		}
		mainWin = h;
		botID = id;
		isLocal = local;
	}

	void restartAdb() {
		wchar_t buf[512];
		int len2 = swprintf(buf, 512, L"\"%s\\HD-Adb.exe\" kill-server", bsInstallDir.c_str());

		std::string ret = Util::execCommand(buf);

		wchar_t buf2[512];
		len2 = swprintf(buf2, 512, L"\"%s\\HD-Adb.exe\" start-server", bsInstallDir.c_str());

		ret = Util::execCommand(buf);

		initADB();
	}

	void stopRecord() {
		TerminateProcess(recordInfo.hProcess, 0);
	}

	bool startRecord()
	{
		wchar_t buf[512];
		int len2 = swprintf(buf, 512, L"\"%s\\HD-Adb.exe\" -s emulator-5554 shell echo test", bsInstallDir.c_str());

		std::string ret = Util::execCommand(buf);

		const char* result;
		if ((result = strstr(ret.c_str(), "error: device 'emulator-5554' not found")) != NULL || (result = strstr(ret.c_str(), "error: closed")) != NULL)
			return false;

		//MessageBoxA(NULL, ret.c_str(), "H", NULL);

		SECURITY_ATTRIBUTES saAttr;
		saAttr.nLength = sizeof(SECURITY_ATTRIBUTES);
		saAttr.bInheritHandle = TRUE;
		saAttr.lpSecurityDescriptor = NULL;
		HANDLE aiR, aiW, aoR, aoW;

		if (!CreatePipe(&aiR, &aiW, &saAttr, 0))
			return false;

		if (!SetHandleInformation(aiW, HANDLE_FLAG_INHERIT, 0))
			return false;

		if (!CreatePipe(&aoR, &aoW, &saAttr, 0))
			return false;

		if (!SetHandleInformation(aoR, HANDLE_FLAG_INHERIT, 0))
			return false;

		//Start process
		PROCESS_INFORMATION ProcessInfo;

		STARTUPINFO StartupInfo;

		ZeroMemory(&StartupInfo, sizeof(StartupInfo));
		StartupInfo.cb = sizeof(StartupInfo);
		StartupInfo.hStdError = aoW;
		StartupInfo.hStdOutput = aoW;
		StartupInfo.hStdInput = aiR;
		StartupInfo.dwFlags |= STARTF_USESTDHANDLES;
		wchar_t buffer[512];
		int len = swprintf(buffer, 512, L"\"%s\\HD-Adb.exe\" -s emulator-5554 shell screenrecord /sdcard/windows/%s/run1.mp4", bsInstallDir.c_str(), Util::getBluestacksSharedFolderAndroid().c_str());
		//MessageBox(NULL, buffer, buffer, NULL);
		if (CreateProcessW(NULL, buffer,
			NULL, NULL, TRUE, CREATE_NO_WINDOW, NULL,
			NULL, &StartupInfo, &ProcessInfo))
		{
			//CloseHandle(ProcessInfo.hThread);
			//CloseHandle(ProcessInfo.hProcess);
			recordInfo = ProcessInfo;
		}
		else {
			MessageBoxA(NULL, "FAIL", "FAIL", NULL);
			return false;
		}


		//sendADB("wm density 160", sizeof("wm density 160"));
		//sendADB("wm size reset", sizeof("wm size reset"));
		//sendADB("reboot", sizeof("reboot"));
		return true;
	}

	//ADB Functions
	bool initADB()
	{
		wchar_t buf[512];
		int len2 = swprintf(buf, 512, L"\"%s\\HD-Adb.exe\" kill-server", bsInstallDir.c_str());

		std::string ret = Util::execCommand(buf);

		len2 = swprintf(buf, 512, L"\"%s\\HD-Adb.exe\" start-server", bsInstallDir.c_str());

		ret = Util::execCommand(buf);

		len2 = swprintf(buf, 512, L"\"%s\\HD-Adb.exe\" -s emulator-5554 shell echo test", bsInstallDir.c_str());

		ret = Util::execCommand(buf);

		const char *result;
		if ((result = strstr(ret.c_str(), "error: device 'emulator-5554' not found")) != NULL || (result = strstr(ret.c_str(), "error: closed")) != NULL)
			return false;

		//MessageBoxA(NULL, ret.c_str(), "H", NULL);

		SECURITY_ATTRIBUTES saAttr;
		saAttr.nLength = sizeof(SECURITY_ATTRIBUTES);
		saAttr.bInheritHandle = TRUE;
		saAttr.lpSecurityDescriptor = NULL;

		if (!CreatePipe(&adbInRead, &adbInWrite, &saAttr, 0))
			return false;

		if (!SetHandleInformation(adbInWrite, HANDLE_FLAG_INHERIT, 0))
			return false;

		if (!CreatePipe(&adbOutRead, &adbOutWrite, &saAttr, 0))
			return false;

		if (!SetHandleInformation(adbOutRead, HANDLE_FLAG_INHERIT, 0))
			return false;

		//Start process
		PROCESS_INFORMATION ProcessInfo;

		STARTUPINFO StartupInfo;

		ZeroMemory(&StartupInfo, sizeof(StartupInfo));
		StartupInfo.cb = sizeof(StartupInfo);
		StartupInfo.hStdError = adbOutWrite;
		StartupInfo.hStdOutput = adbOutWrite;
		StartupInfo.hStdInput = adbInRead;
		StartupInfo.dwFlags |= STARTF_USESTDHANDLES;
		wchar_t buffer[512];
		int len = swprintf(buffer, 512, L"\"%s\\HD-Adb.exe\" -s emulator-5554 shell", bsInstallDir.c_str());
		//MessageBox(NULL, buffer, buffer, NULL);
		if (CreateProcessW(NULL, buffer,
			NULL, NULL, TRUE, CREATE_NO_WINDOW, NULL,
			NULL, &StartupInfo, &ProcessInfo))
		{
			//CloseHandle(ProcessInfo.hThread);
			//CloseHandle(ProcessInfo.hProcess);
			adbInfo = ProcessInfo;
		}
		else {
			MessageBoxA(NULL, "FAIL", "FAIL", NULL);
			return false;
		}

		
		//sendADB("wm density 160", sizeof("wm density 160"));
		//sendADB("wm size reset", sizeof("wm size reset"));
		//sendADB("reboot", sizeof("reboot"));
		return true;
	}
	bool sendADB(const char* cmd, int size)
	{
		std::lock_guard<std::mutex> guard(ADBComMutex);
		if (!isAlive()) {
			while (!initADB()) {
				Sleep(2000);
			}
		}
		DWORD wrote;
		if (!WriteFile(adbInWrite, cmd, size, &wrote, NULL)) {
			return false;
		}
		Sleep(450);
		return true;
	}
	bool isAlive() {
		DWORD dwExitCode;
		if (GetExitCodeProcess(adbInfo.hProcess, &dwExitCode)) {
			if (dwExitCode == STILL_ACTIVE) {
				return true;
			}
			else {
				return false;
			}
		}
		else {
			return false;
		}
	}
	void click(int x, int y)
	{

		std::string screenDevice = "/dev/input/event8";

		//char buf[100];
		//sendevent /dev/input/event7 3 57 0; sendevent /dev/input/event7 3 48 9; sendevent /dev/input/event7 3 58 50; sendevent /dev/input/event7 3 53 9504; sendevent /dev/input/event7 3 54 11161; sendevent /dev/input/event7 0 2 0; sendevent /dev/input/event7 0 0 0; sendevent /dev/input/event7 0 2 0; sendevent /dev/input/event7 0 0 0;
		//int s = snprintf(buf, 100, "input tap %d %d\n", x, y);
		
		//TODO make emulator specific

		//displayX = (x - minX) * displayWidth / (maxX - minX + 1)
		//displayY = (y - minY) * displayHeight / (maxY - minY + 1)
		int newX = (x - 0) * 32729 / (860 - 0 + 1);
		int newY = (y - 0) * 32723 / (732 - 0 + 1);
		std::string buf = "sendevent " + screenDevice + " 3 53 " + std::to_string(newX) + "; sendevent " + screenDevice + " 3 54 " + std::to_string(newY) + "; sendevent " + screenDevice + " 0 2 0; sendevent " + screenDevice + " 0 0 0; sendevent " + screenDevice + " 0 2 0; sendevent " + screenDevice + " 0 0 0;\n";
		
		if(isDebug)
			AppendTextA(mainWin, buf.c_str(), IDC_CONSOLE);
		//std::string buf = "input tap " + std::to_string(x) + " " + std::to_string(y) + "\n";
		sendADB(buf.c_str(), buf.length());
	}

	//Main Android ADB Functions
	void startApp(std::string package) {
		char buf[50];
		int s = sprintf(buf, "monkey -p %s 1\n", package.c_str());
		sendADB(buf, s);
	}

	//Platform Functions
	void bootPlatform()
	{
		PlatformID = PLATFORM_BLUESTACKS2; //TODO fix to grab from settings
		if (PlatformID == PLATFORM_BLUESTACKS2)
			startBluestacks2();
		else if(PlatformID == PLATFORM_NOX)
			startNox();
	}
	void waitForPlatform() {
		//TODO Platform specific
		while (getAndroidWindow() == NULL) {
			Sleep(1000);
		}
		//TODO Remove and make auto detect
		MessageBoxA(NULL, "Click OK when BlueStacks2 has loaded the launcher!", "Click OK when BlueStacks2 has loaded!", NULL);
	}
	//TODO: Complete function
	void killPlatform()
	{
		int retval = ::_tsystem(_T("taskkill /F /T /IM HD-Adb.exe"));
		retval = ::_tsystem(_T("taskkill /F /T /IM HD-Player.exe"));
		retval = ::_tsystem(_T("taskkill /F /T /IM HD-Agent.exe"));
		retval = ::_tsystem(_T("taskkill /F /T /IM BstkSVC.exe"));

		wchar_t buf[512];
		int len2 = swprintf(buf, 512, L"\"%s\\HD-Adb.exe\" kill-server", bsInstallDir.c_str());

		std::string ret = Util::execCommand(buf);
	}

	bool startNox() {
		HKEY hKey;
		ULONG lPath;
		WCHAR szBuffer[512];
		DWORD dwBufferSize = sizeof(szBuffer);

		long lKey = RegOpenKeyExW(HKEY_LOCAL_MACHINE, L"SOFTWARE\\WOW6432Node\\DuoDianOnline\\SetupInfo", NULL, KEY_READ, &hKey);
		if (lKey == ERROR_FILE_NOT_FOUND) {
			//AppendText(hWnd, L"Please install BlueStacks!\r\n", ID_CONSOLE);
			return false;
		}

		lPath = RegQueryValueExW(hKey, L"InstallPath", NULL, NULL, (LPBYTE)szBuffer, &dwBufferSize);
		if (lPath == ERROR_SUCCESS) {
			PROCESS_INFORMATION ProcessInfo;

			STARTUPINFO StartupInfo;

			ZeroMemory(&StartupInfo, sizeof(StartupInfo));
			StartupInfo.cb = sizeof(StartupInfo);
			wchar_t buffer[512];
			int len = swprintf(buffer, 512, L"\"%s\\bin\\Nox.exe\" -resolution:860x732 -dpi:160", szBuffer);
			if (CreateProcessW(NULL, buffer,
				NULL, NULL, FALSE, NULL, NULL,
				NULL, &StartupInfo, &ProcessInfo))
			{
				CloseHandle(ProcessInfo.hThread);
				CloseHandle(ProcessInfo.hProcess);
				while (!getNox())
					Sleep(100);
				Sleep(5000);
			}
			else {
				RegCloseKey(hKey);
				return false;
			}
			RegCloseKey(hKey);
			return true;
		}
		RegCloseKey(hKey);
		return false;
	}

	bool startBluestacks2()
	{
		//AppendText(hWnd, L"Starting BlueStacks!\r\n", ID_CONSOLE);
		HKEY hKey, fKey, cKey;
		long lKey = RegOpenKeyExW(HKEY_LOCAL_MACHINE, L"SOFTWARE\\BlueStacks", NULL, KEY_READ | KEY_WOW64_64KEY, &hKey);
		if (lKey == ERROR_FILE_NOT_FOUND) {
			//AppendText(hWnd, L"Please install BlueStacks!\r\n", ID_CONSOLE);
			return false;
		}
		long lKey2 = RegOpenKeyExW(HKEY_LOCAL_MACHINE, L"SOFTWARE\\BlueStacks\\Guests\\Android\\FrameBuffer\\0", NULL, KEY_ALL_ACCESS | KEY_WOW64_64KEY, &fKey);
		if (lKey2 == ERROR_FILE_NOT_FOUND) {
			//AppendText(hWnd, L"Please install BlueStacks!\r\n", ID_CONSOLE);
			return false;
		}
		long lKey3 = RegOpenKeyExW(HKEY_LOCAL_MACHINE, L"SOFTWARE\\BlueStacks\\Guests\\Android\\Config", NULL, KEY_ALL_ACCESS | KEY_WOW64_64KEY, &cKey);
		if (lKey3 == ERROR_FILE_NOT_FOUND) {
			//AppendText(hWnd, L"Please install BlueStacks!\r\n", ID_CONSOLE);
			return false;
		}
		DWORD gH(0), gW(0), wH(0), wW(0), aPort(0);
		DWORD dwBufferSz(sizeof(DWORD));
		ULONG lPath = RegQueryValueExW(fKey, L"GuestWidth", NULL, NULL, reinterpret_cast<LPBYTE>(&gW), &dwBufferSz);
		if (lPath == ERROR_SUCCESS) {
			if (gW != 860) {
				DWORD val = 860;
				LSTATUS ls = RegSetValueExW(fKey, L"GuestWidth", NULL, REG_DWORD, (const BYTE*)&val, sizeof(DWORD));
				if (ls != ERROR_SUCCESS) {
					//AppendText(hWnd, L"Error setting BlueStacks width and height!\r\n", ID_CONSOLE);
				}
			}
		}
		lPath = RegQueryValueExW(cKey, L"BstAdbPort", NULL, NULL, reinterpret_cast<LPBYTE>(&aPort), &dwBufferSz);
		if (lPath == ERROR_SUCCESS) {
			adbPort = aPort;
		}
		lPath = RegQueryValueExW(fKey, L"GuestHeight", NULL, NULL, reinterpret_cast<LPBYTE>(&gH), &dwBufferSz);
		if (lPath == ERROR_SUCCESS) {
			if (gH != 732) {
				DWORD val = 732;
				LSTATUS ls = RegSetValueExW(fKey, L"GuestHeight", NULL, REG_DWORD, (const BYTE*)&val, sizeof(DWORD));
				if (ls != ERROR_SUCCESS) {
					//AppendText(hWnd, L"Error setting BlueStacks width and height!\r\n", ID_CONSOLE);
				}
			}
		}
		lPath = RegQueryValueExW(fKey, L"WindowHeight", NULL, NULL, reinterpret_cast<LPBYTE>(&wH), &dwBufferSz);
		if (lPath == ERROR_SUCCESS) {
			if (wH != 732) {
				DWORD val = 732;
				LSTATUS ls = RegSetValueExW(fKey, L"WindowHeight", NULL, REG_DWORD, (const BYTE*)&val, sizeof(DWORD));
				if (ls != ERROR_SUCCESS) {
					//AppendText(hWnd, L"Error setting BlueStacks width and height!\r\n", ID_CONSOLE);
				}
			}
		}
		lPath = RegQueryValueExW(fKey, L"WindowWidth", NULL, NULL, reinterpret_cast<LPBYTE>(&wW), &dwBufferSz);
		if (lPath == ERROR_SUCCESS) {
			if (wW != 860) {
				DWORD val = 860;
				LSTATUS ls = RegSetValueExW(fKey, L"WindowWidth", NULL, REG_DWORD, (const BYTE*)&val, sizeof(DWORD));
				if (ls != ERROR_SUCCESS) {
					//AppendText(hWnd, L"Error setting BlueStacks width and height!\r\n", ID_CONSOLE);
				}
			}
		}
		RegCloseKey(cKey);
		RegCloseKey(fKey);
		WCHAR szBuffer[512];
		DWORD dwBufferSize = sizeof(szBuffer);
		lPath = RegQueryValueExW(hKey, L"InstallDir", NULL, NULL, (LPBYTE)szBuffer, &dwBufferSize);
		bsInstallDir = szBuffer;
		if (lPath == ERROR_SUCCESS) {
			PROCESS_INFORMATION ProcessInfo;

			STARTUPINFO StartupInfo;

			ZeroMemory(&StartupInfo, sizeof(StartupInfo));
			StartupInfo.cb = sizeof(StartupInfo);
			wchar_t buffer[512];
			//HD-StartLauncher = bluestacks 1
			//HD-Frontend = Bluestacks 2
			//HD-Player = Bluestacks 4
			int len = swprintf(buffer, 512, L"\"%sHD-Player.exe\" Android", szBuffer); 
			if (CreateProcessW(NULL, buffer,
				NULL, NULL, FALSE, NULL, NULL,
				NULL, &StartupInfo, &ProcessInfo))
			{
				CloseHandle(ProcessInfo.hThread);
				CloseHandle(ProcessInfo.hProcess);
				while (!getBluestacks2())
					Sleep(100);
				Sleep(5000);
			}
			else {
				RegCloseKey(hKey);
				return false;
			}
		}
		RegCloseKey(hKey);
		return true;
	}

	HWND getBluestacks2()
	{
		HWND hWnd = FindWindow(NULL, TEXT("BlueStacks Android PluginAndroid"));
		return hWnd;
	}

	HWND getNox()
	{
		HWND hWnd = FindWindow(NULL, TEXT("NoxPlayer"));
		DWORD procID;
		DWORD procRet = GetWindowThreadProcessId(hWnd, &procID);

		WindowEnum windows;

		windows.procID = procID;

		BOOL windowsB = EnumWindows(enumWindowsProc, reinterpret_cast<LPARAM>(&windows));

		for each(WindowData wd in windows.windows) {
			RECT pos;
			GetClientRect(wd.h, &pos);

			//if(wd.className == std::string("Qt5QWindowToolSaveBits") && pos.right == 32)


			MessageBoxA(NULL, wd.className.c_str(), wd.className.c_str(), NULL);
		}


		return hWnd;
	}

	HWND getAndroidWindow() {
		HWND win = NULL;
		/*switch (PlatformID) {
		case PLATFORM_NOX:
			win = getNox();
			break;
		case PLATFORM_BLUESTACKS2:
			win = getBluestacks2();
			break;
		}*/
		if (PlatformID == PLATFORM_NOX) {
			win = getNox();
		}else if(PlatformID == PLATFORM_BLUESTACKS2){
			win = getBluestacks2();
		}
		return getBluestacks2();
	}

	//Bot functions
	void clickXY(int x, int y)
	{
		if (getDebug()) {
			std::string msg = "CLICK_XY: X - " + std::to_string(x) + " Y - " + std::to_string(y) + "\n";
			AppendTextA(mainWin, msg, IDC_CONSOLE);
		}
		click(x, y);
	}
	bool findAndClickImage(std::string img, int left = 0, int top = 0, int right = 0, int bottom = 0)
	{
		if (getDebug()) {
			std::string msg = "FIND_CLICK_IMAGE: " + img + "\n";
			AppendTextA(mainWin, msg, IDC_CONSOLE);
		}
		std::tuple<bool, int, int> image;
		image = findImageWithXY(img, -1, left, top, right, bottom);
		if (std::get<0>(image)) {
			clickXY(std::get<1>(image), std::get<2>(image));
			return true;
		}
		return false;
	}
	bool findAndClickImageWithTol(std::string img, int tol, int left = 0, int top = 0, int w = 0, int h = 0)
	{
		if (getDebug()) {
			std::string msg = "FIND_CLICK_IMAGE_WITH_TOL: " + img + "\n";
			AppendTextA(mainWin, msg, IDC_CONSOLE);
		}
		std::tuple<bool, int, int> image;
		image = findImageWithXY(img, tol, left, top, w, h);
		if (std::get<0>(image)) {
			clickXY(std::get<1>(image), std::get<2>(image));
			return true;
		}
		return false;
	}
	std::tuple<bool, int, int> findImageWithXY(std::string img, int tol, int left = 0, int top = 0, int right = 0, int bottom = 0)
	{
		//TODO Platform specific
		HWND window = getAndroidWindow();
		char buffer[1024];
		if (bottom == 0)
			bottom = h;
		else
			bottom += top;
		if (right == 0)
			right = w;
		else
			right += left;
		if (tol < 0)
			tol = defaultTol;
		if(isLocal)
			sprintf(buffer, "*%d %s\\scripts\\local\\%s\\imgs\\%s", tol, Util::getCurrentDirectoryA(), botID.c_str(), img.c_str());
		else
			sprintf(buffer, "*%d %s\\scripts\\%s\\imgs\\%s", tol, Util::getCurrentDirectoryA(), botID.c_str(), img.c_str());
		char* result = (*pImageSearch)(window, left, top, right, bottom, buffer);

		if (strcmp(result, "0") != 0) {
			struct ImgXY resXY = Util::getXY(result);
			return std::make_tuple(true, resXY.x, resXY.y);
		}
		return std::make_tuple(false, -1, -1);
	}

	std::vector<ImgXY> searchImageForImages(std::string bigTarget, std::string smallImg, bool filter, int filterDist) {
		if (isLocal) {
			bigTarget = Util::getCurrentDirectoryS() + "\\scripts\\local\\" + botID + +"\\Captures\\" + bigTarget;
			smallImg = Util::getCurrentDirectoryS() + "\\scripts\\local\\" + botID + +"\\imgs\\" + smallImg;
		}
		else {
			bigTarget = Util::getCurrentDirectoryS() + "\\scripts\\" + botID + +"\\Captures\\" + bigTarget;
			smallImg = Util::getCurrentDirectoryS() + "\\scripts\\" + botID + +"\\imgs\\" + smallImg;
		}
		std::vector<ImgXY> result = (*pImageSearchExM)(0, 0, 900, 900, &smallImg[0u], &bigTarget[0u]);

		if (filter) {
			for (unsigned int i = 0; i < result.size(); i++) {
				if (i > (result.size() - 1))
					break;
				for (unsigned int k = 0; k < result.size(); k++) {
					if (i == k)
						continue;
					int distX = std::abs(result.at(i).x - result.at(k).x);
					int distY = std::abs(result.at(i).y - result.at(k).y);
					if (distX < filterDist && distY < filterDist) {
						result.erase(result.begin() + k);
					}
				}
			}
		}

		std::string stringResult;

		for (unsigned int i = 0; i < result.size(); i++) {
			stringResult += std::to_string(result.at(i).x) + "|" + std::to_string(result.at(i).y);
			if (i != (result.size() - 1))
				stringResult += ";";
		}


		//return stringResult;
		return result;
	}

	std::tuple<bool, int, int> searchImageForImage(std::string searchTarget, std::string itemToSearchImg) {
		HWND window = getAndroidWindow();
		char buffer[1024];
		char buffer2[1024];
		if (isLocal) {
			sprintf(buffer, "*5 %s\\scripts\\local\\%s\\imgs\\%s", Util::getCurrentDirectoryA(), botID.c_str(), itemToSearchImg.c_str());
			sprintf(buffer2, "%s\\scripts\\local\\%s\\Captures\\%s", Util::getCurrentDirectoryA(), botID.c_str(), searchTarget.c_str());
		}
		else {
			sprintf(buffer, "*5 %s\\scripts\\%s\\imgs\\%s", Util::getCurrentDirectoryA(), botID.c_str(), itemToSearchImg.c_str());
			sprintf(buffer2, "%s\\scripts\\%s\\Captures\\%s", Util::getCurrentDirectoryA(), botID.c_str(), searchTarget.c_str());
		}
		char* result = (*pImageSearchEx)(0, 0, w, h, buffer, buffer2);

		if (strcmp(result, "0") != 0) {
			struct ImgXY resXY = Util::getXY(result);
			return std::make_tuple(true, resXY.x, resXY.y);
		}
		return std::make_tuple(false, -1, -1);
	}

	std::tuple<bool, int, int> searchBMPForImage(HBITMAP searchTarget, std::string itemToSearchImg) {
		char buffer[1024];
		char buffer2[1024];
		if (isLocal) {
			sprintf(buffer, "*150 %s\\scripts\\local\\%s\\imgs\\%s", Util::getCurrentDirectoryA(), botID.c_str(), itemToSearchImg.c_str());
		}
		else {
			sprintf(buffer, "*5 %s\\scripts\\%s\\imgs\\%s", Util::getCurrentDirectoryA(), botID.c_str(), itemToSearchImg.c_str());
		}
		char* result = (*pImageSearchBMP)(0, 0, w, h, searchTarget, buffer2);

		if (strcmp(result, "0") != 0) {
			struct ImgXY resXY = Util::getXY(result);
			return std::make_tuple(true, resXY.x, resXY.y);
		}
		return std::make_tuple(false, -1, -1);
	}

	bool findImage(std::string img)
	{
		if (getDebug()) {
			std::string msg = "FIND_IMAGE: " + img + "\n";
			AppendTextA(mainWin, msg, IDC_CONSOLE);
		}
		//TODO Platform specific
		HWND window = getAndroidWindow();

		char buffer[1024];

		if(isLocal)
			sprintf(buffer, "*5 %s\\scripts\\local\\%s\\imgs\\%s", Util::getCurrentDirectoryA(), botID.c_str(), img.c_str());
		else
			sprintf(buffer, "*5 %s\\scripts\\%s\\imgs\\%s", Util::getCurrentDirectoryA(), botID.c_str(), img.c_str());
		char* result = (*pImageSearch)(window, 0, 0, w, h, buffer);

		if (strcmp(result, "0") != 0) {
			return true;
		}
		return false;
	}
	bool waitForImage(std::string img) {
		if (getDebug()) {
			std::string msg = "WAIT_IMAGE: " + img + "\n";
			AppendTextA(mainWin, msg, IDC_CONSOLE);
		}
		while (!findImage(img)) {
			wait(10);
		}
		return true;
	}
	bool waitClickImage(std::string img, int timeout) {
		if (getDebug()) {
			std::string msg = "WAIT_CLICK_IMAGE: " + img + "\n";
			AppendTextA(mainWin, msg, IDC_CONSOLE);
		}
		int i345 = 0;
		while (!findAndClickImage(img)) {
			i345++;
			if (timeout > 0) {
				if (timeout <= (i345 / 10))
					return false;
			}
			wait(100);
		}
		return true;
	}
	bool findAndClickImageRect(std::string img, int timeout, int left, int top, int w, int h) {
		if (getDebug()) {
			std::string msg = "FIND_CLICK_IMAGE_RECT: " + img + "\n";
			AppendTextA(mainWin, msg, IDC_CONSOLE);
		}
		return findAndClickImage(img, left, top, w, h);
	}

	void createThread(std::string script, bool loop) {
		ThreadMutex.lock();
		threadIDCounter++;
		botThreads.push_back(ThreadInfo(script, threadIDCounter, loop));
		ThreadMutex.unlock();
	}

	std::vector<ThreadInfo> getThreadQueue() {
		ThreadMutex.lock();
		std::vector<ThreadInfo> threads = botThreads;
		ThreadMutex.unlock();
		return threads;
	}

	//UI Functions
	void AppendTextA(const HWND & hwnd, std::string newText, int item)
	{
		// get edit control from dialog
		HWND hwndOutput = GetDlgItem(hwnd, item);

		// get the current selection
		DWORD StartPos, EndPos;
		SendMessage(hwndOutput, EM_GETSEL, reinterpret_cast<WPARAM>(&StartPos), reinterpret_cast<WPARAM>(&EndPos));

		// move the caret to the end of the text
		int outLength = GetWindowTextLength(hwndOutput);
		SendMessage(hwndOutput, EM_SETSEL, outLength, outLength);

		// insert the text at the new caret position
		TCHAR txt[512];
		USES_CONVERSION;
		_tcscpy(txt, A2T(newText.c_str()));
		SendMessage(hwndOutput, EM_REPLACESEL, TRUE, reinterpret_cast<LPARAM>(txt));

		// restore the previous selection
		//SendMessage(hwndOutput, EM_SETSEL, StartPos, EndPos);

		int line = SendMessage(hwndOutput, EM_LINEFROMCHAR, -1, NULL);
		if (line > 60) {
			int lLen = SendMessage(hwndOutput, EM_LINELENGTH, 0, NULL);
			SendMessage(hwndOutput, EM_SETSEL, 0, lLen + 2);
			SendMessage(hwndOutput, EM_REPLACESEL, TRUE, reinterpret_cast<LPARAM>(L""));
			outLength = GetWindowTextLength(hwndOutput);
			SendMessage(hwndOutput, EM_SETSEL, outLength, outLength);
			SendMessage(hwndOutput, EM_SCROLLCARET, NULL, NULL);
		}
	}

	//Misc Functions
	void wait(int MS)
	{
		Sleep(MS);
	}

	void setRunning(bool value) {
		isRunning = value;
	}
	bool getRunning() const {
		return isRunning;
	}

	void setPlaying(bool value) {
		isPlaying = value;
	}
	bool getPlaying() const {
		return isPlaying;
	}

	void setDebug(bool value) {
		isDebug = value;
	}
	bool getDebug() const {
		return isDebug;
	}

	int getTime() {
		return (static_cast<long int> (time(NULL)));
	}

	void addUIEvent(int ElementID) {
		std::lock_guard<std::mutex> EventGuard(EventMutex);
		UIEventQueue.push_back(ElementID);
	}

	int processNextUIEvent() {
		std::lock_guard<std::mutex> EventGuard(EventMutex);
		int ElementID;
		if (UIEventQueue.size() > 0) {
			ElementID = UIEventQueue.at(0);
			UIEventQueue.erase(UIEventQueue.begin());
		}
		else {
			ElementID = 0;
		}
		return ElementID;
	}

	void toggleControl(int id, bool toggle) {
		//bool checked = IsDlgButtonChecked(mainWin, id);
		CheckDlgButton(mainWin, id, toggle);
	}

	bool hasUIElement(int ElementID) {
		std::lock_guard<std::mutex> ElementGuard(UIElementMutex);
		for each(int e in UIElements) {
			if (e == ElementID)
				return true;
		}
		return false;
	}

	void deleteUIElements() {
		std::lock_guard<std::mutex> ElementGuard(UIElementMutex);
		for each(int e in UIElements) {
			if (!DestroyWindow(GetDlgItem(mainWin, e))) {
				DWORD error = GetLastError();
			}
		}
	}

	void setControlText(int id, std::string text) {
		SetDlgItemTextA(mainWin, id, text.c_str());
	}

	int createControlFixedID(int id, int type, int x, int y, int width, int height, std::string text) {
		std::lock_guard<std::mutex> UIGaurd(UIElementsQueueMutex);
		int ControlID = id + 70000;
		UIElementS s;

		if (x < 0 || y < 0 || width < 0 || height < 0) {
			AppendTextA(mainWin, "Error: Control Creation Failed: Height, width, x or y value is less than 0\n", IDC_CONSOLE);
			return -1;
		}

		int newX = x + BASE_UI_X;
		int newY = y + BASE_UI_Y;

		if (newX + width > MAX_UI_X || newY + height > MAX_UI_Y) {
			AppendTextA(mainWin, "Error: Control Creation Failed: Control is too big\n", IDC_CONSOLE);
			return -1;
		}

		s.height = height;
		s.width = width;
		s.x = newX;
		s.y = newY;
		s.type = type;
		s.id = ControlID;
		s.text = text;

		UIElementsQueue.push_back(s);
		return ControlID;
	}

	int createControl(int type, int x, int y, int width, int height, std::string text) {
		std::lock_guard<std::mutex> UIGaurd(UIElementsQueueMutex);
		int ControlID = elementIDOffset + UIElementsQueue.size();
		UIElementS s;

		if (x < 0 || y < 0 || width < 0 || height < 0) {
			AppendTextA(mainWin, "Error: Control Creation Failed: Height, width, x or y value is less than 0\n", IDC_CONSOLE);
			return -1;
		}

		int newX = x + BASE_UI_X;
		int newY = y + BASE_UI_Y;

		if (newX + width > MAX_UI_X || newY + height > MAX_UI_Y) {
			AppendTextA(mainWin, "Error: Control Creation Failed: Control is too big\n", IDC_CONSOLE);
			return -1;
		}

		s.height = height;
		s.width = width;
		s.x = newX;
		s.y = newY;
		s.type = type;
		s.id = ControlID;
		s.text = text;

		UIElementsQueue.push_back(s);
		return ControlID;
	}

	std::vector<int> getUIElements() {
		std::lock_guard<std::mutex> UIGaurd(UIElementMutex);
		return UIElements;
	}

	std::vector<UIElementS> getUIQueue() {
		std::lock_guard<std::mutex> UIGaurd(UIElementsQueueMutex);
		return UIElementsQueue;
	}

	void clearUIQueue() {
		std::lock_guard<std::mutex> UIGaurd(UIElementsQueueMutex);
		UIElementsQueue.clear();
	}

	void addToUIVector(int id) {
		std::lock_guard<std::mutex> UIGaurd(UIElementMutex);
		UIElements.push_back(id);
	}

	void clearUIVector() {
		std::lock_guard<std::mutex> UIGaurd(UIElementMutex);
		UIElements.clear();
	}

	void updateGUI() {
		PostMessage(mainWin, BB_UPDATE_GUI, NULL, NULL);
		Sleep(100);
	}

	void freeLibraries() {
		FreeLibrary(imageDLL);
	}
};

