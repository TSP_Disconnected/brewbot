#pragma once

#include <windows.h>

#ifndef DLL_H_
#define DLL_H_

typedef int(__stdcall *RunPtr)(void);
typedef bool(__stdcall *SetRunPtr)(bool);
typedef void(__stdcall *PausePtr)(bool);
typedef void(__stdcall *InitWindowPtr)(HWND, bool);
typedef void(__stdcall *DestroyWindowPtr)(void);
typedef void(__stdcall *WmPaint)(HDC hdc);
typedef bool(__stdcall *IsPausedPtr)(void);
typedef LRESULT(__stdcall *WndProcPtr)(HWND, UINT, WPARAM, LPARAM);

#endif
