#include "stdafx.h"
#include "ADB.h"
#include "Utility.h"
#include <fstream>

ADB::ADB()
{
	adbInRead = NULL;
	adbInWrite = NULL;
	adbOutRead = NULL;
	adbOutWrite = NULL;
	MessageBox(NULL, L"", L"ayyye", NULL);
	while (!initADB()) {
		connectADB();
		Sleep(2000);
	}
}

ADB::~ADB()
{
	closeADB();
	//disconnectADB();
}

void ADB::Clear()
{
	sendADB("\n", sizeof("\n"));
}

void ADB::Click(int x, int y)
{
	char buf[50];
	int s = sprintf(buf, "input tap %d %d\n", x, y);
	sendADB(buf, s);
}

void ADB::startApp(const char* package)
{
	char buf[50];
	int s = sprintf(buf, "monkey -p %s 1\n", package);
	sendADB(buf, s);
}

bool ADB::sendADB(char* cmd, int size)
{
	DWORD wrote;
	if (!WriteFile(adbInWrite, cmd, size, &wrote, NULL))
		return false;
	Sleep(100);
	return true;
}

void ADB::closeADB()
{
	//Send exit command
	sendADB("exit\n", sizeof("exit\n"));
	//Close STD handles
	CloseHandle(adbInRead);
	CloseHandle(adbOutRead);
	CloseHandle(adbOutWrite);
	CloseHandle(adbInWrite);
}

void ADB::disconnectADB()
{
	//Disconnect ADB
	PROCESS_INFORMATION ProcessInfo;

	STARTUPINFO StartupInfo;

	ZeroMemory(&StartupInfo, sizeof(StartupInfo));
	StartupInfo.cb = sizeof StartupInfo;
	wchar_t buffer[512];
	int len = swprintf(buffer, 512, L"\"%s\\adb.exe\" disconnect localhost:5555", Util::getCurrentDirectoryW());
	if (CreateProcessW(0, buffer,
		NULL, NULL, FALSE, CREATE_NO_WINDOW, NULL,
		NULL, &StartupInfo, &ProcessInfo))
	{
		WaitForSingleObject(ProcessInfo.hProcess, INFINITE);
		CloseHandle(ProcessInfo.hThread);
		CloseHandle(ProcessInfo.hProcess);
	}
	else
	{
		MessageBox(NULL, L"ERROR: 1001", L"Oops", NULL);
	}
}

bool ADB::connectADB()
{
	PROCESS_INFORMATION ProcessInfo;

	STARTUPINFO StartupInfo;

	ZeroMemory(&StartupInfo, sizeof(StartupInfo));
	StartupInfo.cb = sizeof StartupInfo;
	wchar_t buffer[512];
	int len = swprintf(buffer, 512, L"\"%s\\adb.exe\" connect localhost:5555", Util::getCurrentDirectoryW());
	if (CreateProcessW(0, buffer,
		NULL, NULL, FALSE, CREATE_NO_WINDOW, NULL,
		NULL, &StartupInfo, &ProcessInfo))
	{
		WaitForSingleObject(ProcessInfo.hProcess, INFINITE);
		CloseHandle(ProcessInfo.hThread);
		CloseHandle(ProcessInfo.hProcess);
		return true;
	}
	else
	{
		MessageBox(NULL, L"ERROR: 1001", L"Oops", NULL);
	}
	return false;
}

bool ADB::initADB() {
	SECURITY_ATTRIBUTES saAttr;
	saAttr.nLength = sizeof(SECURITY_ATTRIBUTES);
	saAttr.bInheritHandle = TRUE;
	saAttr.lpSecurityDescriptor = NULL;

	if (!CreatePipe(&adbInRead, &adbInWrite, &saAttr, 0))
		return false;

	if (!SetHandleInformation(adbInWrite, HANDLE_FLAG_INHERIT, 0))
		return false;

	if (!CreatePipe(&adbOutRead, &adbOutWrite, &saAttr, 0))
		return false;

	if (!SetHandleInformation(this->adbOutRead, HANDLE_FLAG_INHERIT, 0))
		return false;

	//Start process
	PROCESS_INFORMATION ProcessInfo;

	STARTUPINFO StartupInfo;

	ZeroMemory(&StartupInfo, sizeof(StartupInfo));
	StartupInfo.cb = sizeof(StartupInfo);
	StartupInfo.hStdError = adbOutWrite;
	StartupInfo.hStdOutput = adbOutWrite;
	StartupInfo.hStdInput = adbInRead;
	StartupInfo.dwFlags |= STARTF_USESTDHANDLES;
	wchar_t buffer[512];
	int len = swprintf(buffer, 512, L"\"%s\\adb.exe\" -s emulator-5554 shell", Util::getCurrentDirectoryW());
	//MessageBox(NULL, buffer, buffer, NULL);
	if (CreateProcessW(NULL, buffer,
		NULL, NULL, TRUE, CREATE_NO_WINDOW, NULL,
		NULL, &StartupInfo, &ProcessInfo))
	{
		CloseHandle(ProcessInfo.hThread);
		CloseHandle(ProcessInfo.hProcess);
	}
	else {
		MessageBoxA(NULL, "FAIL", "FAIL", NULL);
		return false;
	}
	//sendADB("wm density 160", sizeof("wm density 160"));
	//sendADB("wm size reset", sizeof("wm size reset"));
	//sendADB("reboot", sizeof("reboot"));
	CHAR chBuf[4096];
	BOOL bSuccess = FALSE;
	DWORD dwRead;
	char *result;
	for (;;)
	{
		bSuccess = ReadFile(adbOutRead, chBuf, 4096, &dwRead, NULL);
		if (!bSuccess || dwRead == 0)
			break;
		//MessageBoxA(NULL, chBuf, chBuf, NULL);
		if ((result = strstr(chBuf, "error: device 'emulator-5554' not found")) != NULL)
			return false;
		else
			break;
	}
	return true;
}
