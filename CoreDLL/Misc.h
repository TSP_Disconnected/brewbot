#pragma once
#include <Windows.h>
#include "stdafx.h"
#include <TlHelp32.h>
#include <time.h>
#include <fstream>
#include <tesseract/baseapi.h>
#include <leptonica/allheaders.h>
#include "Utility.h"

typedef std::string (__stdcall *Tess_GetTextFromImage)(std::string, std::string, std::string);

struct Misc {
public:
	std::string botID;
	bool isLocal;

	Misc(std::string id, bool local) {
		botID = id;
		isLocal = local;
	}

	HDC getWindowHDC(HWND hWnd){
		return GetDC(hWnd);
	}

	void deleteDC(HDC hdc) {
		DeleteDC(hdc);
	}

	std::string  GetTextFromHDC(HDC hdc, std::string dataPath, std::string dataFile) {
		char* outText;

		tesseract::TessBaseAPI* api = new tesseract::TessBaseAPI();
		// Initialize tesseract-ocr with English, without specifying tessdata path
		if (api->Init(dataPath.c_str(), dataFile.c_str())) {
			fprintf(stderr, "Could not initialize tesseract.\n");
			exit(1);
		}

		//Convert HDC to std::vector<unsighned char>
		std::vector<unsigned char> memBmp = Util::HDCtoMemBmp(hdc);

		// Open input image with leptonica library
		Pix* image = pixReadMemBmp(&memBmp[0], memBmp.size());
		api->SetImage(image);
		// Get OCR result
		outText = api->GetUTF8Text();
		OutputDebugStringA(outText);

		std::string ret = std::string(outText);

		delete[] outText;

		// Destroy used object and release memory
		api->End();
		//delete[] outText; //TODO Mermory leak due to incompatible tesseract DLL; recompile tesseract for vs2017?
		pixDestroy(&image);

		return ret;
	}

	std::string  GetTextFromPicture(std::string imgPath, std::string dataPath, std::string dataFile) {
		char* outText;

		tesseract::TessBaseAPI* api = new tesseract::TessBaseAPI();
		// Initialize tesseract-ocr with English, without specifying tessdata path
		if (api->Init(dataPath.c_str(), dataFile.c_str())) {
			fprintf(stderr, "Could not initialize tesseract.\n");
			exit(1);
		}

		// Open input image with leptonica library
		Pix* image = pixRead(imgPath.c_str());
		api->SetImage(image);
		// Get OCR result
		outText = api->GetUTF8Text();
		OutputDebugStringA(outText);

		std::string ret = std::string(outText);

		delete[] outText;

		// Destroy used object and release memory
		api->End();
		//delete[] outText; //TODO Mermory leak due to incompatible tesseract DLL; recompile tesseract for vs2017?
		pixDestroy(&image);

		return ret;
	}

	HDC LoadImg(std::string imgFile, bool crop, int x, int y, int w, int h) {
		// From File:
		if (isLocal)
			imgFile = Util::getCurrentDirectoryS() + "\\scripts\\local\\" + botID + "\\Captures\\" + imgFile;
		else
			imgFile = Util::getCurrentDirectoryS() + "\\scripts\\" + botID + "\\Captures\\" + imgFile;
		HBITMAP hBitmap = (HBITMAP)LoadImageA(NULL, imgFile.c_str(), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE | LR_CREATEDIBSECTION);
		HGDIOBJ sdc_orig_select;
		//Top Left = 130,290
		//Bottom Right = 660,610

		BITMAP BMP;
		GetObject(hBitmap, sizeof(BMP), &BMP); // Here we get the BMP header info.

		HDC BMPDC = CreateCompatibleDC(NULL); // This will hold the BMP image itself.

		SelectObject(BMPDC, hBitmap); // Put the image into the DC.

		HDC newHDC = CreateCompatibleDC(BMPDC);
		HBITMAP hBscreen = CreateCompatibleBitmap(BMPDC, w, h);
		if (crop) {
			if (!newHDC || !hBscreen)
				return NULL;
			if (!(sdc_orig_select = SelectObject(newHDC, hBscreen)))
				return NULL;
			if (!(BitBlt(newHDC, 0, 0, w, h, BMPDC, x, y, SRCCOPY)))
				return NULL;
			DeleteDC(BMPDC);
		}


		HDC hDC = GetDC(0);

		/*if(crop)
			BitBlt(hDC, 0, 0, w, h, newHDC, 0, 0, SRCCOPY); // Finally, Draw it 
		else
			BitBlt(hDC, 0, 0, BMP.bmWidth, BMP.bmHeight, BMPDC, 0, 0, SRCCOPY); // Finally, Draw it */

		ReleaseDC(NULL, hDC);

		// Don't forget to clean up!
		if (crop) {
			DeleteDC(BMPDC);
			DeleteObject(hBitmap);
			DeleteObject(hBscreen);
			return newHDC;
		}
		else {
			DeleteDC(newHDC);
			DeleteObject(hBitmap);
			DeleteObject(hBscreen);
			return BMPDC;
		}
	}

	void captureScreen(HWND hwnd, std::string file) {
		std::string capDir = Util::getCurrentDirectoryS() + "\\scripts\\local\\" + botID + "\\Captures";
		
		CreateDirectoryA(capDir.c_str(), NULL);
		if(isLocal)
			file = Util::getCurrentDirectoryS() + "\\scripts\\local\\" + botID + "\\Captures\\" + file;
		else
			file = Util::getCurrentDirectoryS() + "\\scripts\\" + botID + "\\Captures\\" + file;
		Util::CaptureScreen(hwnd, file.c_str());
	}

	void saveHDCToBitmap(HDC hdc, std::string filename) {
		std::string path;
		if(isLocal)
			path = Util::getCurrentDirectoryS() + "\\scripts\\local\\" + botID + "\\Captures\\" + filename + ".bmp";
		else
			path = Util::getCurrentDirectoryS() + "\\scripts\\" + botID + "\\Captures\\" + filename + ".bmp";
		Util::CaptureHDC(hdc, path.c_str());
	}

	int Distance(int x1, int y1, int x2, int y2) {
		if (x1 == x2 && y1 == y2) {
			return 0;
		}
		else if (x1 == x2) {
			return (int)abs(y1 - y2);
		}
		else if (y1 == y2) {
			return (int)abs(x1 - x2);
		}
		int x3 = x2 - x1;
		int y3 = y2 - y1;
		int x4 = x3 ^ 2;
		int y4 = y3 ^ 2;
		int xy = x4 + y4;
		double ans = sqrt(xy);
		return (int)std::round(sqrt((x2 - x1) ^ 2 + (y2 - y1) ^ 2));
	}

	void copyHDCToScreen(HDC hdc) {
		HDC screenHDC = GetDC(0);

		BITMAP structBitmapHeader;
		memset(&structBitmapHeader, 0, sizeof(BITMAP));

		HGDIOBJ hBitmap = GetCurrentObject(hdc, OBJ_BITMAP);
		GetObject(hBitmap, sizeof(BITMAP), &structBitmapHeader);

		// bitmap dimensions
		int bitmap_dx = structBitmapHeader.bmWidth;
		int bitmap_dy = structBitmapHeader.bmHeight;

		BitBlt(screenHDC, 0, 0, bitmap_dx, bitmap_dy, hdc, 0, 0, SRCCOPY); // Finally, Draw it 

		ReleaseDC(NULL, screenHDC);
	}

	HDC cropHDC(HDC hdc, int x, int y, int sWidth, int sHeight) {
		HGDIOBJ sdc_orig_select;
		HDC newHDC = CreateCompatibleDC(hdc);
		HBITMAP hBscreen = CreateCompatibleBitmap(hdc, sWidth, sHeight);
		if (!newHDC || !hBscreen)
			return NULL;
		if (!(sdc_orig_select = SelectObject(newHDC, hBscreen)))
			return NULL;

		// Copy the pixels in the search-area of the screen into the DC to be searched:
		if (!(BitBlt(newHDC, 0, 0, sWidth, sHeight, hdc, x, y, SRCCOPY)))
			return NULL;
		DeleteDC(hdc);
		return newHDC;
	}

	COLORREF createColor(int r, int g, int b) {
		return RGB(r, g, b);
	}

	void replacePixel(HDC hdc, int x, int y, COLORREF newColor) {
		SetPixel(hdc, x, y, newColor);
	}

	void replaceColor(HDC hdc, COLORREF oldColor, COLORREF newColor) {
		WORD oH, oL, oS, h, l, s;
		ColorRGBToHLS(oldColor, &oH, &oL, &oS);
		
		BITMAP structBitmapHeader;
		memset(&structBitmapHeader, 0, sizeof(BITMAP));

		HGDIOBJ hBitmap = GetCurrentObject(hdc, OBJ_BITMAP);
		GetObject(hBitmap, sizeof(BITMAP), &structBitmapHeader);

		// bitmap dimensions
		int width = structBitmapHeader.bmWidth;
		int height = structBitmapHeader.bmHeight;

		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				COLORREF pix = GetPixel(hdc, x, y);
				ColorRGBToHLS(pix, &h, &l, &s);
				int dH = abs(h - oH);
				int dL = abs(l - oL);
				int dS = abs(s - oS);
				if (pix == oldColor || (dS < 5 && dH < 5 && dL < 5)) {
					SetPixel(hdc, x, y, newColor);
				}
			}
		}
	}

	void replaceColorWithTol(HDC hdc, COLORREF oldColor, COLORREF newColor, int tol) {
		WORD oH, oL, oS, h, l, s;
		ColorRGBToHLS(oldColor, &oH, &oL, &oS);

		BITMAP structBitmapHeader;
		memset(&structBitmapHeader, 0, sizeof(BITMAP));

		HGDIOBJ hBitmap = GetCurrentObject(hdc, OBJ_BITMAP);
		GetObject(hBitmap, sizeof(BITMAP), &structBitmapHeader);

		if (tol < 0)
			tol = 0;

		// bitmap dimensions
		int width = structBitmapHeader.bmWidth;
		int height = structBitmapHeader.bmHeight;

		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				COLORREF pix = GetPixel(hdc, x, y);
				ColorRGBToHLS(pix, &h, &l, &s);
				int dH = abs(h - oH);
				int dL = abs(l - oL);
				int dS = abs(s - oS);
				if (pix == oldColor || (dS < tol && dH < tol && dL < tol)) {
					SetPixel(hdc, x, y, newColor);
				}
			}
		}
	}

	std::string readImageText(std::string img, std::string dataFile) {
		std::string dataPath;
		if (isLocal)
			dataPath = Util::getCurrentDirectoryS() + "\\scripts\\local\\" + botID + "\\tessdata";
		else
			dataPath = Util::getCurrentDirectoryS() + "\\scripts\\" + botID + "\\tessdata";

		std::string imgPath;
		if (isLocal)
			imgPath = Util::getCurrentDirectoryS() + "\\scripts\\local\\" + botID + "\\Captures\\" + img + ".bmp";
		else
			imgPath = Util::getCurrentDirectoryS() + "\\scripts\\" + botID + "\\Captures\\" + img + ".bmp";

		std::string result = GetTextFromPicture(imgPath, dataPath, dataFile);

		return result;
	}

	std::string readHDCText(HDC hdc, std::string dataFile) {
		std::string dataPath;
		if (isLocal)
			dataPath = Util::getCurrentDirectoryS() + "\\scripts\\local\\" + botID + "\\tessdata";
		else
			dataPath = Util::getCurrentDirectoryS() + "\\scripts\\" + botID + "\\tessdata";

		std::string result = GetTextFromHDC(hdc, dataPath, dataFile);

		return result;
	}

	void replaceAllColorsExcept(HDC hdc, COLORREF color, COLORREF rColor) {
		BITMAP structBitmapHeader;
		memset(&structBitmapHeader, 0, sizeof(BITMAP));

		HGDIOBJ hBitmap = GetCurrentObject(hdc, OBJ_BITMAP);
		GetObject(hBitmap, sizeof(BITMAP), &structBitmapHeader);

		// bitmap dimensions
		int width = structBitmapHeader.bmWidth;
		int height = structBitmapHeader.bmHeight;

		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				if (GetPixel(hdc, x, y) != color) {
					SetPixel(hdc, x, y, rColor);
				}
			}
		}
	}
};