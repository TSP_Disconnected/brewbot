#pragma once
#include "stdafx.h"
#include <opencv2/opencv.hpp>
#include <opencv2/tracking.hpp>
#include <opencv2/tracking/tracker.hpp>

#include "Utility.h"

struct OpenCV {



public:
	bool isLocal = false;
	std::string botID;
	cv::VideoCapture cap;
	HWND mainWin;

	OpenCV(std::string id, bool local, HWND mh) {
		botID = id;
		isLocal = local;
		mainWin = mh;
	}

	cv::Mat hwnd2mat(HWND hwnd) {

		HDC hwindowDC, hwindowCompatibleDC;

		int height, width, srcheight, srcwidth;
		HBITMAP hbwindow;
		cv::Mat src;
		BITMAPINFOHEADER  bi;

		hwindowDC = GetDC(hwnd);
		hwindowCompatibleDC = CreateCompatibleDC(hwindowDC);
		SetStretchBltMode(hwindowCompatibleDC, COLORONCOLOR);

		RECT windowsize;    // get the height and width of the screen
		GetClientRect(hwnd, &windowsize);

		srcheight = windowsize.bottom;
		srcwidth = windowsize.right;
		height = windowsize.bottom;  //change this to whatever size you want to resize to
		width = windowsize.right;

		src.create(height, width, CV_8UC4);

		// create a bitmap
		hbwindow = CreateCompatibleBitmap(hwindowDC, width, height);
		bi.biSize = sizeof(BITMAPINFOHEADER);    //http://msdn.microsoft.com/en-us/library/windows/window/dd183402%28v=vs.85%29.aspx
		bi.biWidth = width;
		bi.biHeight = -height;  //this is the line that makes it draw upside down or not
		bi.biPlanes = 1;
		bi.biBitCount = 32;
		bi.biCompression = BI_RGB;
		bi.biSizeImage = 0;
		bi.biXPelsPerMeter = 0;
		bi.biYPelsPerMeter = 0;
		bi.biClrUsed = 0;
		bi.biClrImportant = 0;

		// use the previously created device context with the bitmap
		SelectObject(hwindowCompatibleDC, hbwindow);
		// copy from the window device context to the bitmap device context
		StretchBlt(hwindowCompatibleDC, 0, 0, width, height, hwindowDC, 0, 0, srcwidth, srcheight, SRCCOPY); //change SRCCOPY to NOTSRCCOPY for wacky colors !
		GetDIBits(hwindowCompatibleDC, hbwindow, 0, height, src.data, (BITMAPINFO*)& bi, DIB_RGB_COLORS);  //copy from hwindowCompatibleDC to hbwindow

		// avoid memory leak
		DeleteObject(hbwindow); DeleteDC(hwindowCompatibleDC); ReleaseDC(hwnd, hwindowDC);

		return src;
	}

	std::tuple<bool, std::vector<cv::Rect>> multiTemplateMatchVideo(std::string tempFile, int max_boxes, double thresh = 0.8) {
		cv::Mat mainImage;

		double cFrame = cap.get(cv::CAP_PROP_POS_FRAMES);
		double tFrame = cap.get(cv::CAP_PROP_FRAME_COUNT);
		std::string message = "MultiTemplateMatchVideo: Checking frame " + std::to_string((int)cFrame) + " of " + std::to_string((int)tFrame) + ".\n";
		Util::AppendTextA(mainWin, message.c_str(), FB_ID_CONSOLE);

		std::vector<cv::Rect> boxes;

		cap >> mainImage;

		if (mainImage.empty())
			return std::make_tuple(false, boxes);

		cv::Mat templateImage = openImage(tempFile);

		cv::Mat gref, gtpl;
		cv::cvtColor(mainImage, gref, CV_BGR2GRAY);
		cv::cvtColor(templateImage, gtpl, CV_BGR2GRAY);

		cv::Mat res(mainImage.rows - templateImage.rows + 1, mainImage.cols - templateImage.cols + 1, CV_32FC1);
		cv::matchTemplate(gref, gtpl, res, CV_TM_CCOEFF_NORMED);
		cv::threshold(res, res, 0.8, 1., CV_THRESH_TOZERO);

		int count = 0;

		while (true)
		{
			double minval, maxval, threshold = thresh;
			cv::Point minloc, maxloc;
			cv::minMaxLoc(res, &minval, &maxval, &minloc, &maxloc);

			for (int i = 0; i < boxes.size(); i++) {
				if (boxes[i].x == maxloc.x && boxes[i].y == maxloc.y) {
					maxval = 0.0;
					break;
				}
			}

			if (maxval >= threshold)
			{
				count++;
				cv::rectangle(
					mainImage,
					maxloc,
					cv::Point(maxloc.x + templateImage.cols, maxloc.y + templateImage.rows),
					CV_RGB(0, 255, 0), 2
				);
				cv::floodFill(res, maxloc, cv::Scalar(0), 0, cv::Scalar(.1), cv::Scalar(1.));
				cv::Rect rect(maxloc.x, maxloc.y, templateImage.cols, templateImage.rows);
				boxes.push_back(rect);
				if (count == max_boxes)
					break;
			}
			else
				break;
		}

		//cv::imshow("reference", mainImage);

		//cv::destroyWindow("reference");

		return std::make_tuple(true, boxes);
	}

	std::tuple<bool, int, int> templateMatchVideo(std::string templateFile/*, int match_method*/) {
		cv::Mat src_img;

		double cFrame = cap.get(cv::CAP_PROP_POS_FRAMES);
		double tFrame = cap.get(cv::CAP_PROP_FRAME_COUNT);
		std::string message = "TemplateMatchVideo: Checking frame " + std::to_string((int)cFrame) + " of " + std::to_string((int)tFrame) + ".\n";
		Util::AppendTextA(mainWin, message.c_str(), FB_ID_CONSOLE);
		
		cap >> src_img;

		if (src_img.empty())
			return std::make_tuple(true, -1, -1);

		cv::Mat template_img;
		cv::Mat result_mat;
		cv::Mat debug_img;

		template_img = openImage(templateFile);
		if (template_img.data == NULL) {
			//printf("cv::imread() failed...\n");
			return std::make_tuple(false, -1, -1);
		}

		int match_method = CV_TM_CCORR_NORMED;
		cv::matchTemplate(src_img, template_img, result_mat, match_method);
		//cv::normalize(result_mat, result_mat, 0, 1, cv::NORM_MINMAX, -1, cv::Mat());

		//cv::threshold(result_mat, result_mat, 0.80, 1., CV_THRESH_TOZERO);

		std::tuple<bool, int, int> ret;

		double minVal; double maxVal;
		cv::Point minLoc, maxLoc, matchLoc;
		cv::minMaxLoc(result_mat, &minVal, &maxVal, &minLoc, &maxLoc, cv::Mat());
		if (match_method == CV_TM_SQDIFF || match_method == CV_TM_SQDIFF_NORMED)  matchLoc = minLoc;
		else matchLoc = maxLoc;

		if (maxVal >= 0.98)
		{
				src_img.copyTo(debug_img);
				cv::rectangle(
					debug_img,
					matchLoc,
					cv::Point(matchLoc.x + template_img.cols, matchLoc.y + template_img.rows),
					CV_RGB(255, 0, 0),
					3);
				//cv::imshow("debug_img", debug_img);
				ret = std::make_tuple(true, matchLoc.x, matchLoc.y);
		}
		else {
			ret = std::make_tuple(false, -1, -1);
		}

		//cv::destroyWindow("debug_img");

		src_img.release();
		result_mat.release();
		debug_img.release();
		template_img.release();

		return ret;
	}

	bool NMultipleTemplateMatching(cv::Mat mInput, cv::Mat mTemplate, float Threshold, float Closeness, std::vector<cv::Point2f>& List_Matches)
	{
		cv::Mat mResult;
		cv::Size szTemplate = mTemplate.size();
		cv::Size szTemplateCloseRadius((szTemplate.width / 2) * Closeness, (szTemplate.height / 2) * Closeness);

		cv::matchTemplate(mInput, mTemplate, mResult, cv::TM_CCOEFF_NORMED);
		cv::threshold(mResult, mResult, Threshold, 1.0, cv::THRESH_TOZERO);
		while (true)
		{
			double minval, maxval;
			cv::Point minloc, maxloc;
			cv::minMaxLoc(mResult, &minval, &maxval, &minloc, &maxloc);

			if (maxval >= Threshold)
			{
				List_Matches.push_back(maxloc);
				cv::rectangle(mResult, cv::Point2f(maxloc.x - szTemplateCloseRadius.width, maxloc.y - szTemplateCloseRadius.height), cv::Point2f(maxloc.x + szTemplateCloseRadius.width, maxloc.y + szTemplateCloseRadius.height), cv::Scalar(0), -1);
			}
			else
				break;
		}
		//imshow("reference", mDebug_Bgr);
		return true;
	}

	std::tuple<bool, int, int> templateMatch(HWND hwnd, std::string templateFile/*, int match_method*/) {
		cv::Mat src_img;
		std::string message = "TemplateMatch: Scanning window.\n";
		Util::AppendTextA(mainWin, message.c_str(), FB_ID_CONSOLE);

		src_img = hwnd2mat(hwnd);

		if (src_img.empty())
			return std::make_tuple(true, -1, -1);

		cv::Mat template_img, template_img_gray, src_img_gray;
		cv::Mat result_mat;
		cv::Mat debug_img;
		std::tuple<bool, int, int> ret;
		std::vector<cv::Point2f> List_Matches;

		template_img = openImage(templateFile);
		if (template_img.data == NULL) {
			//printf("cv::imread() failed...\n");
			ret = std::make_tuple(false, -1, -1);
		}
		else {

			cvtColor(src_img, src_img_gray, cv::COLOR_BGR2GRAY);
			cvtColor(template_img, template_img_gray, cv::COLOR_BGR2GRAY);

			NMultipleTemplateMatching(src_img_gray, template_img_gray, 0.9, 0.9, List_Matches);

			//cv::destroyWindow("debug_img");

			if (List_Matches.size() > 0)
				ret = std::make_tuple(true, List_Matches[0].x, List_Matches[0].y);
			else
				ret = std::make_tuple(false, 0, 0);
		}
		src_img.release();
		result_mat.release();
		debug_img.release();
		template_img.release();
		template_img_gray.release();
		src_img_gray.release();

		return ret;
	}

	cv::Ptr<cv::Tracker> createTrackerByName(std::string trackerType)
	{
		std::vector<std::string> trackerTypes = { "BOOSTING", "MIL", "KCF", "TLD", "MEDIANFLOW", "GOTURN", "MOSSE", "CSRT" };
		cv::Ptr<cv::Tracker> tracker;
		if (trackerType == trackerTypes[0])
			tracker = cv::TrackerBoosting::create();
		else if (trackerType == trackerTypes[1])
			tracker = cv::TrackerMIL::create();
		else if (trackerType == trackerTypes[2])
			tracker = cv::TrackerKCF::create();
		else if (trackerType == trackerTypes[3])
			tracker = cv::TrackerTLD::create();
		else if (trackerType == trackerTypes[4])
			tracker = cv::TrackerMedianFlow::create();
		else if (trackerType == trackerTypes[5])
			tracker = cv::TrackerGOTURN::create();
		else if (trackerType == trackerTypes[6])
			tracker = cv::TrackerMOSSE::create();
		else if (trackerType == trackerTypes[7])
			tracker = cv::TrackerCSRT::create();
		
		return tracker;
	}
	void getRandomColors(std::vector<cv::Scalar>& colors, int numColors)
	{
		cv::RNG rng(time(NULL));
		for (int i = 0; i < numColors; i++)
			colors.push_back(cv::Scalar(rng.uniform(0, 255), rng.uniform(0, 255), rng.uniform(0, 255)));
	}
	void openVideo(std::string file) {
		std::string fullPath = Util::getBluestacksSharedFolderWindows() + file;

		cap = cv::VideoCapture(fullPath);

		if (!cap.isOpened()) {
			//ERROR
			MessageBoxA(NULL, "Could not open video with OpenCV", "OpenCV Error", NULL);
		}
	}
	void closeVideo() {
		if (cap.isOpened())
			cap.~VideoCapture();
	}
	cv::Mat nextFrame() {
		cv::Mat frame;
		cap >> frame;
		return frame;
	}
	cv::Mat openImage(std::string file) {

		std::string path;

		if (isLocal)
			path = Util::getCurrentDirectoryS() + "\\scripts\\local\\" + botID + "\\imgs\\" + file;
		else
			path = Util::getCurrentDirectoryS() + "\\scripts\\" + botID + "\\imgs\\" + file;

		return cv::imread(path, cv::IMREAD_COLOR);
	}
	bool frameEmpty(cv::Mat frame) {
		return frame.empty();
	}
	bool isCapOpened() {
		return cap.isOpened();
	}
	void waitKey(int delay) {
		cv::waitKey(delay);
	}

	std::tuple<int, std::map<int, std::map<std::string, int>>> RectArrayToMap(std::vector<cv::Rect> boxes) {
		std::map<int, std::map<std::string, int>> nestedmap;

		for (int i = 0; i < boxes.size(); i++) {
			std::map<std::string, int> innermap;
			innermap["x"] = boxes[i].x;
			innermap["y"] = boxes[i].y;

			nestedmap[i + 1] = innermap;
		}

		int length = boxes.size();

		return std::make_tuple(length, nestedmap);
	}

	std::map<std::string, int> RectToMap(cv::Rect rect) {
		std::map<std::string, int> map;

		map["x"] = rect.x;
		map["y"] = rect.y;

		return map;
	}

	std::map<int, std::map<std::string, int>> runMultiTrackerVideo(std::vector<cv::Rect> boxes) {
		cv::Mat currentFrame;

		cap.retrieve(currentFrame, cap.get(cv::CAP_PROP_POS_FRAMES));
		
		std::vector<cv::Scalar> colors;
		getRandomColors(colors, boxes.size());

		// Specify the tracker type
		std::string trackerType = "CSRT";
		// Create multitracker
		cv::Ptr<cv::MultiTracker> multiTracker = cv::MultiTracker::create();

		// Initialize multitracker
		for (int i = 0; i < boxes.size(); i++)
			multiTracker->add(createTrackerByName(trackerType), currentFrame, cv::Rect2d(boxes[i]));

		currentFrame.release();

		cv::Mat frame;

		while (cap.isOpened())
		{

			double cFrame = cap.get(cv::CAP_PROP_POS_FRAMES);
			double tFrame = cap.get(cv::CAP_PROP_FRAME_COUNT);
			std::string message = "MultiTrackerVideo: Checking frame " + std::to_string((int)cFrame) + " of " + std::to_string((int)tFrame) + ".\n";

			Util::AppendTextA(mainWin, message.c_str() , FB_ID_CONSOLE);

			cap >> frame;

			if (frame.empty()) break;

			//std::cout << cap.get(CAP_PROP_POS_FRAMES) << "/" << cap.get(CAP_PROP_FRAME_COUNT) << endl;

			//Update the tracking result with new frame
			multiTracker->update(frame);

			// Draw tracked objects
			for (unsigned i = 0; i < multiTracker->getObjects().size(); i++)
			{
				rectangle(frame, multiTracker->getObjects()[i], colors[i], 2, 1);
			}

			// Show frame
			imshow("MultiTracker", frame);

			frame.release();

			// quit on x button
			if (cv::waitKey(1) == 27) break;
		}
		std::map<int, std::map<std::string, int>> nestedmap;

		std::vector<cv::Rect2d> newboxes = multiTracker->getObjects();

		for (int i = 0; i < newboxes.size(); i++) {
			std::map<std::string, int> innermap;
			innermap["x"] = newboxes[i].x;
			innermap["y"] = newboxes[i].y;

			nestedmap[i + 1] = innermap;
		}

		cv::destroyWindow("MultiTracker");

		return nestedmap;
	}

	bool captureNextFrame() {
		double cFrame = cap.get(cv::CAP_PROP_POS_FRAMES);
		double tFrame = cap.get(cv::CAP_PROP_FRAME_COUNT);
		std::string message = "CaptureNextFrame: Capturing frame " + std::to_string((int)cFrame) + " of " + std::to_string((int)tFrame) + ".\n";
		Util::AppendTextA(mainWin, message.c_str(), FB_ID_CONSOLE);

		HBITMAP frame = ConvertCVMatToBMP();

		if (frame == nullptr) {
			std::string message = "CaptureNextFrame: Failed to capture frame " + std::to_string((int)cFrame) + " of " + std::to_string((int)tFrame) + ".\n";
			Util::AppendTextA(mainWin, message.c_str(), FB_ID_CONSOLE);
			message = "CaptureNextFrame: Closing video.\n";
			Util::AppendTextA(mainWin, message.c_str(), FB_ID_CONSOLE);
			cap.release();
			return false;
		}

		HDC cDC = CreateCompatibleDC(NULL);
		HGDIOBJ old = SelectObject(cDC, frame);
		std::string path;
		if (isLocal)
			path = Util::getCurrentDirectoryS() + "\\scripts\\local\\" + botID + "\\Captures\\frame.bmp";
		else
			path = Util::getCurrentDirectoryS() + "\\scripts\\" + botID + "\\Captures\\frame.bmp";

		HDC screen = GetDC(NULL);

		//BitBlt(screen, 0, 0, 732, 860, cDC, 0, 0, SRCCOPY);

		Util::CaptureHDC(cDC, path.c_str());

		ReleaseDC(NULL, screen);
		SelectObject(cDC, old);
		DeleteDC(cDC);

		DeleteObject(frame);
		return true;
	}

	auto ConvertCVMatToBMP() -> HBITMAP
	{
		auto convertOpenCVBitDepthToBits = [](const int32_t value)
		{
			auto regular = 0u;

			switch (value)
			{
			case CV_8U:
			case CV_8S:
				regular = 8u;
				break;

			case CV_16U:
			case CV_16S:
				regular = 16u;
				break;

			case CV_32S:
			case CV_32F:
				regular = 32u;
				break;

			case CV_64F:
				regular = 64u;
				break;

			default:
				regular = 0u;
				break;
			}

			return regular;
		};

		cv::Mat frame;

		if(cap.isOpened())
			cap >> frame;

		if (frame.empty())
			return nullptr;

		auto imageSize = frame.size();
		assert(imageSize.width && "invalid size provided by frame");
		assert(imageSize.height && "invalid size provided by frame");

		if (imageSize.width && imageSize.height)
		{
			auto headerInfo = BITMAPINFOHEADER{};
			ZeroMemory(&headerInfo, sizeof(headerInfo));

			headerInfo.biSize = sizeof(headerInfo);
			headerInfo.biWidth = imageSize.width;
			headerInfo.biHeight = -(imageSize.height); // negative otherwise it will be upsidedown
			headerInfo.biPlanes = 1;// must be set to 1 as per documentation frame.channels();

			const auto bits = convertOpenCVBitDepthToBits(frame.depth());
			headerInfo.biBitCount = frame.channels() * bits;

			auto bitmapInfo = BITMAPINFO{};
			ZeroMemory(&bitmapInfo, sizeof(bitmapInfo));

			bitmapInfo.bmiHeader = headerInfo;
			bitmapInfo.bmiColors->rgbBlue = 0;
			bitmapInfo.bmiColors->rgbGreen = 0;
			bitmapInfo.bmiColors->rgbRed = 0;
			bitmapInfo.bmiColors->rgbReserved = 0;

			auto dc = GetDC(nullptr);
			assert(dc != nullptr && "Failure to get DC");
			auto bmp = CreateDIBitmap(dc,
				&headerInfo,
				CBM_INIT,
				frame.data,
				&bitmapInfo,
				DIB_RGB_COLORS);
			assert(bmp != nullptr && "Failure creating bitmap from captured frame");

			frame.release();

			return bmp;
		}
		else
		{
			return nullptr;
		}
	}
};