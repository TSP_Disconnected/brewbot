#pragma once

#include <thread>

class ThreadGuard
{
public:
	ThreadGuard(std::thread& t)
		: thread(t)
	{ }

	~ThreadGuard()
	{
		if (thread.joinable())
		{
			thread.join();
		}
	}

	ThreadGuard(ThreadGuard& other) = delete;
	ThreadGuard& operator=(const ThreadGuard& rhs) = delete;

private:
	std::thread& thread;

};

