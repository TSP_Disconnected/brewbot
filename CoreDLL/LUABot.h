#pragma once

#include "stdafx.h"
#include "BotWrapper.h"

struct ThreadArgs {
	std::string script;
	std::string botID;
	bool local;
	bool loop;
};

class LUABot
{
public:
	LUABot(char* script, HWND mh, char* id);
	void threadFunction(Bot & bot, ThreadArgs args);
	static int my_exception_handler(lua_State * L, sol::optional<const std::exception&> maybe_exception, sol::string_view description);
	inline static void my_panic(sol::optional<std::string> maybe_msg);
	~LUABot();
	bool GetPlaying();
	void Stop();
	void Play();
	void clearUIQueue();
	void clearUIElements();
	std::vector<int> getUIElements();
	std::vector<UIElementS> getUIQueue();
	void addToUIVector(int id);
	void Pause();
	void QueueUIEvent(int ElementID);
	void DeleteUIElements();
	bool HasUIElement(int ElementID);
	void loop();
	void addSecond();

	//DWORD __stdcall runThread(LPVOID funcptr);

private:
	std::vector<int> uiControls;
	bool bStop;
	bool bPlay;
	sol::state lua;
	std::string scriptName;
	Bot mBot;
	std::string botID;
	std::string scriptFolder;
	bool local;
	bool error = false;

};

