#pragma once
#include "stdafx.h"
#include "Bot.h"


struct BotWrapper {
public:
	Bot &mainBot;

	int dTol;
	int ID_CONSOLE;

	BotWrapper(Bot &b) : mainBot(b) {
		dTol = mainBot.defaultTol;
		ID_CONSOLE = mainBot.IDC_CONSOLE;
	}

	//ADB Functions
	bool initADB()
	{
		return mainBot.initADB();
	}
	void restartAdb() {
		mainBot.restartAdb();
	}
	bool sendADB(char* cmd, int size)
	{
		return mainBot.sendADB(cmd, size);
	}
	bool isAlive() {
		return mainBot.isAlive();
	}
	void click(int x, int y)
	{
		mainBot.click(x, y);
	}

	void setGlobal(std::string key, std::string val) {
		mainBot.setGlobal(key, val);
	}

	std::string getGlobal(std::string key) {
		std::string ret = mainBot.getGlobal(key);
		return ret;
	}

	//Main Android ADB Functions
	void startApp(std::string package) {
		mainBot.startApp(package);
	}

	//Platform Functions
	void bootPlatform()
	{
		mainBot.bootPlatform();
	}
	void waitForPlatform() {
		mainBot.waitForPlatform();
	}
	void killPlatform()
	{
		mainBot.killPlatform();
	}
	bool startBluestacks2()
	{
		return mainBot.startBluestacks2();
	}
	HWND getAndroidWindow()
	{
		return mainBot.getAndroidWindow();
	}

	//Bot functions
	void clickXY(int x, int y)
	{
		mainBot.clickXY(x, y);
	}
	bool findAndClickImage(std::string img)
	{
		return mainBot.findAndClickImage(img);
	}
	bool findAndClickImageWithTol(std::string img, int tol, int left = 0, int top = 0, int w = 0, int h = 0)
	{
		return mainBot.findAndClickImageWithTol(img, tol, left, top, w, h);
	}
	std::tuple<bool, int, int> findImageWithXY(std::string img, int tol)
	{
		return mainBot.findImageWithXY(img, tol);
	}

	std::vector<ImgXY> searchImageForImages(std::string bigTarget, std::string smallImg, bool filter, int filterDist) {
		return mainBot.searchImageForImages(bigTarget, smallImg, filter, filterDist);
	}

	std::tuple<bool, int, int> searchImageForImage(std::string searchTarget, std::string itemToSearchImg) {
		return mainBot.searchImageForImage(searchTarget, itemToSearchImg);
	}

	std::tuple<bool, int, int> searchBMPForImage(HBITMAP searchTarget, std::string itemToSearchImg) {
		return mainBot.searchBMPForImage(searchTarget, itemToSearchImg);
	}

	bool findImage(std::string img)
	{
		return mainBot.findImage(img);
	}
	bool waitForImage(std::string img) {
		return mainBot.waitForImage(img);
	}
	bool waitClickImage(std::string img) {
		return mainBot.waitClickImage(img, 0);
	}
	bool waitClickImageWithTimeout(std::string img, int to) {
		return mainBot.waitClickImage(img, to);
	}
	bool findAndClickImageRect(std::string img, int timeout, int left, int top, int w, int h) {
		return mainBot.findAndClickImageRect(img, timeout, left, top, w, h);
	}
	void createThread(std::string function, bool loop) {
		mainBot.createThread(function, loop);
	}

	//UI Functions
	void AppendTextA(const HWND & hwnd, std::string newText, int item)
	{
		mainBot.AppendTextA(hwnd, newText, item);
	}

	void toggleControl(int id, bool toggle) {
		mainBot.toggleControl(id, toggle);
	}

	//Misc Functions
	void print(std::string msg) {
		mainBot.AppendTextA(mainBot.mainWin, msg, mainBot.IDC_CONSOLE);
	}
	void wait(int MS)
	{
		mainBot.wait(MS);
	}

	void setRunning(bool value) {
		mainBot.setRunning(value);
	}
	bool getRunning() const {
		return mainBot.getRunning();
	}

	void setPlaying(bool value) {
		mainBot.setPlaying(value);
	}
	bool getPlaying() const {
		return mainBot.getPlaying();
	}

	void setDebug(bool value) {
		mainBot.setDebug(value);
	}
	bool getDebug() const {
		return mainBot.getDebug();
	}

	int getTime() {
		return mainBot.getTime();
	}

	void setStatusBar(std::string msg) {
		mainBot.setStatusBar(msg);
	}

	std::string getRunTimeStr() {
		return mainBot.getRunTimeStr();
	}

	HWND getMainWin() {
		return mainBot.mainWin;
	}

	void updateGUI() {
		mainBot.updateGUI();
	}

	void setControlText(int id, std::string text) {
		mainBot.setControlText(id, text);
	}

	int createControlFixedID(int id, int type, int x, int y, int width, int height, std::string text) {
		return mainBot.createControlFixedID(id, type, x, y, width, height, text);
	}

	int createControl(int type, int x, int y, int width, int height, std::string text) {
		return mainBot.createControl(type, x, y, width, height, text);
	}

	int processNextUIEvent() {
		return mainBot.processNextUIEvent();
	}

	bool startRecord() {
		return mainBot.startRecord();
	}

	void stopRecord() {
		mainBot.stopRecord();
	}

	//Temporary function for diamond
	void solveDiamond(int low, int high, int diamondCup) {
		//mainBot.solveDiamond(low, high, diamondCup);
	}
};