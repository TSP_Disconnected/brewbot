#include "stdafx.h"
#include "Utility.h"
#include <fstream>
#include <Windows.h>
#include <atlstr.h>

std::vector<unsigned char> Util::HDCtoMemBmp(HDC hdc)
{
	BITMAP structBitmapHeader;
	memset(&structBitmapHeader, 0, sizeof(BITMAP));

	HGDIOBJ hBitmap = GetCurrentObject(hdc, OBJ_BITMAP);
	GetObject(hBitmap, sizeof(BITMAP), &structBitmapHeader);

	// bitmap dimensions
	int bitmap_dx = structBitmapHeader.bmWidth;
	int bitmap_dy = structBitmapHeader.bmHeight;

	std::vector<unsigned char> memBmp;

	// save bitmap file headers
	BITMAPFILEHEADER fileHeader;
	BITMAPINFOHEADER infoHeader;

	fileHeader.bfType = 0x4d42;
	fileHeader.bfSize = 0;
	fileHeader.bfReserved1 = 0;
	fileHeader.bfReserved2 = 0;
	fileHeader.bfOffBits = sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER);

	infoHeader.biSize = sizeof(infoHeader);
	infoHeader.biWidth = bitmap_dx;
	infoHeader.biHeight = bitmap_dy;
	infoHeader.biPlanes = 1;
	infoHeader.biBitCount = 24;
	infoHeader.biCompression = BI_RGB;
	infoHeader.biSizeImage = 0;
	infoHeader.biXPelsPerMeter = 0;
	infoHeader.biYPelsPerMeter = 0;
	infoHeader.biClrUsed = 0;
	infoHeader.biClrImportant = 0;

	// dibsection information
	BITMAPINFO info;
	info.bmiHeader = infoHeader;

	// ------------------
	// THE IMPORTANT CODE
	// ------------------
	// create a dibsection and blit the window contents to the bitmap
	HDC winDC;
	winDC = hdc;
	if (winDC == NULL)
		MessageBoxA(NULL, "ERROR1", "ERROR1", NULL);
	//HDC winDC = CreateCompatibleDC(GetWindowDC(window));
	//PrintWindow(window, winDC, NULL);
	HDC memDC = CreateCompatibleDC(winDC);
	if (memDC == NULL)
		MessageBoxA(NULL, "ERROR2", "ERROR1", NULL);
	BYTE* memory = 0;
	HBITMAP bitmap = CreateDIBSection(winDC, &info, DIB_RGB_COLORS, (void**)& memory, 0, 0);
	SelectObject(memDC, bitmap);
	BitBlt(memDC, 0, 0, bitmap_dx, bitmap_dy, winDC, 0, 0, SRCCOPY);
	DeleteDC(memDC);
	DeleteDC(winDC);

	// save dibsection data
	int dwBmpSize = (((24 * bitmap_dx + 31) & (~31)) / 8) * bitmap_dy;

	std::vector<unsigned char> buffer(sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER) + dwBmpSize);
	std::copy(reinterpret_cast<unsigned char*>(&fileHeader), reinterpret_cast<unsigned char*>(&fileHeader) + sizeof(BITMAPFILEHEADER), buffer.begin());
	std::copy(reinterpret_cast<unsigned char*>(&infoHeader), reinterpret_cast<unsigned char*>(&infoHeader) + sizeof(BITMAPINFOHEADER), buffer.begin() + sizeof(BITMAPFILEHEADER));
	std::copy(memory, memory + dwBmpSize, buffer.begin() + sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER));

	// HA HA, forgot paste in the DeleteObject lol, happy now ;)?
	DeleteObject(bitmap);

	return buffer;
}

void Util::CaptureHDC(HDC hdc, const char * filename)
{
	BITMAP structBitmapHeader;
	memset(&structBitmapHeader, 0, sizeof(BITMAP));

	HGDIOBJ hBitmap = GetCurrentObject(hdc, OBJ_BITMAP);
	GetObject(hBitmap, sizeof(BITMAP), &structBitmapHeader);

	// bitmap dimensions
	int bitmap_dx = structBitmapHeader.bmWidth;
	int bitmap_dy = structBitmapHeader.bmHeight;

	// create file
	std::ofstream file(filename, std::ios::binary);
	if (!file) return;

	// save bitmap file headers
	BITMAPFILEHEADER fileHeader;
	BITMAPINFOHEADER infoHeader;

	fileHeader.bfType = 0x4d42;
	fileHeader.bfSize = 0;
	fileHeader.bfReserved1 = 0;
	fileHeader.bfReserved2 = 0;
	fileHeader.bfOffBits = sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER);

	infoHeader.biSize = sizeof(infoHeader);
	infoHeader.biWidth = bitmap_dx;
	infoHeader.biHeight = bitmap_dy;
	infoHeader.biPlanes = 1;
	infoHeader.biBitCount = 24;
	infoHeader.biCompression = BI_RGB;
	infoHeader.biSizeImage = 0;
	infoHeader.biXPelsPerMeter = 0;
	infoHeader.biYPelsPerMeter = 0;
	infoHeader.biClrUsed = 0;
	infoHeader.biClrImportant = 0;

	file.write((char*)&fileHeader, sizeof(fileHeader));
	file.write((char*)&infoHeader, sizeof(infoHeader));

	// dibsection information
	BITMAPINFO info;
	info.bmiHeader = infoHeader;

	// ------------------
	// THE IMPORTANT CODE
	// ------------------
	// create a dibsection and blit the window contents to the bitmap
	HDC winDC;
	winDC = hdc;
	if (winDC == NULL)
		MessageBoxA(NULL, "ERROR1", "ERROR1", NULL);
	//HDC winDC = CreateCompatibleDC(GetWindowDC(window));
	//PrintWindow(window, winDC, NULL);
	HDC memDC = CreateCompatibleDC(winDC);
	if (memDC == NULL)
		MessageBoxA(NULL, "ERROR2", "ERROR1", NULL);
	BYTE* memory = 0;
	HBITMAP bitmap = CreateDIBSection(winDC, &info, DIB_RGB_COLORS, (void**)&memory, 0, 0);
	SelectObject(memDC, bitmap);
	BitBlt(memDC, 0, 0, bitmap_dx, bitmap_dy, winDC, 0, 0, SRCCOPY);
	DeleteDC(memDC);
	DeleteDC(winDC);

	// save dibsection data
	int bytes = (((24 * bitmap_dx + 31) & (~31)) / 8)*bitmap_dy;
	file.write((char *)memory, bytes);

	// HA HA, forgot paste in the DeleteObject lol, happy now ;)?
	DeleteObject(bitmap);
}

std::wstring Util::getBluestacksSharedFolderAndroid() {
	HKEY cKey;

	long lKey3 = RegOpenKeyExW(HKEY_LOCAL_MACHINE, L"SOFTWARE\\BlueStacks\\Guests\\Android\\SharedFolder\\0", NULL, KEY_ALL_ACCESS | KEY_WOW64_64KEY, &cKey);
	if (lKey3 == ERROR_FILE_NOT_FOUND) {
		//AppendText(hWnd, L"Please install BlueStacks!\r\n", ID_CONSOLE);
		return std::wstring();
	}

	WCHAR szBuffer[512];
	DWORD dwBufferSize = sizeof(szBuffer);
	ULONG lPath = RegQueryValueExW(cKey, L"Name", NULL, NULL, (LPBYTE)szBuffer, &dwBufferSize);

	return std::wstring(szBuffer);
}

std::string Util::getBluestacksSharedFolderWindows() {
	HKEY cKey;

	long lKey3 = RegOpenKeyExW(HKEY_LOCAL_MACHINE, L"SOFTWARE\\BlueStacks\\Guests\\Android\\SharedFolder\\0", NULL, KEY_ALL_ACCESS | KEY_WOW64_64KEY, &cKey);
	if (lKey3 == ERROR_FILE_NOT_FOUND) {
		//AppendText(hWnd, L"Please install BlueStacks!\r\n", ID_CONSOLE);
		return std::string();
	}

	CHAR szBuffer[512];
	DWORD dwBufferSize = sizeof(szBuffer);
	ULONG lPath = RegQueryValueExA(cKey, "Path", NULL, NULL, (LPBYTE)szBuffer, &dwBufferSize);

	return std::string(szBuffer);
}

void Util::AppendTextA(const HWND& hwnd, std::string newText, int item)
{

		// get edit control from dialog
		HWND hwndOutput = GetDlgItem(hwnd, item);

		// get the current selection
		DWORD StartPos, EndPos;
		SendMessage(hwndOutput, EM_GETSEL, reinterpret_cast<WPARAM>(&StartPos), reinterpret_cast<WPARAM>(&EndPos));

		// move the caret to the end of the text
		int outLength = GetWindowTextLength(hwndOutput);
		SendMessage(hwndOutput, EM_SETSEL, outLength, outLength);

		// insert the text at the new caret position
		TCHAR txt[512];
		USES_CONVERSION;
		_tcscpy(txt, A2T(newText.c_str()));
		SendMessage(hwndOutput, EM_REPLACESEL, TRUE, reinterpret_cast<LPARAM>(txt));

		// restore the previous selection
		//SendMessage(hwndOutput, EM_SETSEL, StartPos, EndPos);

		int line = SendMessage(hwndOutput, EM_LINEFROMCHAR, -1, NULL);
		if (line > 60) {
			int lLen = SendMessage(hwndOutput, EM_LINELENGTH, 0, NULL);
			SendMessage(hwndOutput, EM_SETSEL, 0, lLen + 2);
			SendMessage(hwndOutput, EM_REPLACESEL, TRUE, reinterpret_cast<LPARAM>(L""));
			outLength = GetWindowTextLength(hwndOutput);
			SendMessage(hwndOutput, EM_SETSEL, outLength, outLength);
			SendMessage(hwndOutput, EM_SCROLLCARET, NULL, NULL);
		}

}

void Util::CaptureScreen(HWND window, const char * filename)
{
	// get screen rectangle
	RECT windowRect;
	GetClientRect(window, &windowRect);
	//DwmGetWindowAttribute(window, DWMWA_EXTENDED_FRAME_BOUNDS, &windowRect, sizeof(windowRect));

	// bitmap dimensions
	int bitmap_dx = windowRect.right - windowRect.left;
	int bitmap_dy = windowRect.bottom - windowRect.top;

	// create file
	std::ofstream file(filename, std::ios::binary);
	if (!file) return;

	// save bitmap file headers
	BITMAPFILEHEADER fileHeader;
	BITMAPINFOHEADER infoHeader;

	fileHeader.bfType = 0x4d42;
	fileHeader.bfSize = 0;
	fileHeader.bfReserved1 = 0;
	fileHeader.bfReserved2 = 0;
	fileHeader.bfOffBits = sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER);

	infoHeader.biSize = sizeof(infoHeader);
	infoHeader.biWidth = bitmap_dx;
	infoHeader.biHeight = bitmap_dy;
	infoHeader.biPlanes = 1;
	infoHeader.biBitCount = 24;
	infoHeader.biCompression = BI_RGB;
	infoHeader.biSizeImage = 0;
	infoHeader.biXPelsPerMeter = 0;
	infoHeader.biYPelsPerMeter = 0;
	infoHeader.biClrUsed = 0;
	infoHeader.biClrImportant = 0;

	file.write((char*)&fileHeader, sizeof(fileHeader));
	file.write((char*)&infoHeader, sizeof(infoHeader));

	// dibsection information
	BITMAPINFO info;
	info.bmiHeader = infoHeader;

	// ------------------
	// THE IMPORTANT CODE
	// ------------------
	// create a dibsection and blit the window contents to the bitmap
	HDC winDC = GetDC(window);
	if (winDC == NULL)
		MessageBoxA(NULL, "ERROR1", "ERROR1", NULL);
	//HDC winDC = CreateCompatibleDC(GetWindowDC(window));
	//PrintWindow(window, winDC, NULL);
	HDC memDC = CreateCompatibleDC(winDC);
	if (memDC == NULL)
		MessageBoxA(NULL, "ERROR2", "ERROR1", NULL);
	BYTE* memory = 0;
	HBITMAP bitmap = CreateDIBSection(winDC, &info, DIB_RGB_COLORS, (void**)&memory, 0, 0);
	SelectObject(memDC, bitmap);
	BitBlt(memDC, 0, 0, bitmap_dx, bitmap_dy, winDC, 0, 0, SRCCOPY);
	DeleteDC(memDC);
	ReleaseDC(window, winDC);

	// save dibsection data
	int bytes = (((24 * bitmap_dx + 31) & (~31)) / 8)*bitmap_dy;
	file.write((char *)memory, bytes);

	// HA HA, forgot paste in the DeleteObject lol, happy now ;)?
	DeleteObject(bitmap);
}

TCHAR * Util::getCurrentDirectoryW()
{
	TCHAR currentDirectory[4096];
	GetCurrentDirectory(4096, currentDirectory);
	return (TCHAR *)currentDirectory;
}

char * Util::getCurrentDirectoryA()
{
	char currentDirectory[4096];
	GetCurrentDirectoryA(4096, currentDirectory);
	return (char*)currentDirectory;
}

const std::string Util::BotURL = "https://www.freebrew.org/brewbot/v2/";

std::string Util::getCurrentDirectoryS()
{
	char currentDirectory[4096];
	GetCurrentDirectoryA(4096, currentDirectory);
	std::string ret = currentDirectory;
	return ret;
}

std::string Util::execCommand(const std::wstring cmd)
{
		std::string strResult;
		HANDLE hPipeRead, hPipeWrite;

		SECURITY_ATTRIBUTES saAttr = { sizeof(SECURITY_ATTRIBUTES) };
		saAttr.bInheritHandle = TRUE; // Pipe handles are inherited by child process.
		saAttr.lpSecurityDescriptor = NULL;

		// Create a pipe to get results from child's stdout.
		if (!CreatePipe(&hPipeRead, &hPipeWrite, &saAttr, 0))
			return strResult;

		STARTUPINFOW si = { sizeof(STARTUPINFOW) };
		si.dwFlags = STARTF_USESHOWWINDOW | STARTF_USESTDHANDLES;
		si.hStdOutput = hPipeWrite;
		si.hStdError = hPipeWrite;
		si.wShowWindow = SW_HIDE; // Prevents cmd window from flashing.
								  // Requires STARTF_USESHOWWINDOW in dwFlags.

		PROCESS_INFORMATION pi = { 0 };

		BOOL fSuccess = CreateProcessW(NULL, (LPWSTR)cmd.c_str(), NULL, NULL, TRUE, CREATE_NEW_CONSOLE, NULL, NULL, &si, &pi);
		if (!fSuccess)
		{
			CloseHandle(hPipeWrite);
			CloseHandle(hPipeRead);
			return strResult;
		}

		bool bProcessEnded = false;
		for (; !bProcessEnded;)
		{
			// Give some timeslice (50 ms), so we won't waste 100% CPU.
			bProcessEnded = WaitForSingleObject(pi.hProcess, 50) == WAIT_OBJECT_0;

			// Even if process exited - we continue reading, if
			// there is some data available over pipe.
			for (;;)
			{
				char buf[1024];
				DWORD dwRead = 0;
				DWORD dwAvail = 0;

				if (!::PeekNamedPipe(hPipeRead, NULL, 0, NULL, &dwAvail, NULL))
					break;

				if (!dwAvail) // No data available, return
					break;

				if (!::ReadFile(hPipeRead, buf, min(sizeof(buf) - 1, dwAvail), &dwRead, NULL) || !dwRead)
					// Error, the child process might ended
					break;

				buf[dwRead] = 0;
				strResult += buf;
			}
		} //for

		CloseHandle(hPipeWrite);
		CloseHandle(hPipeRead);
		CloseHandle(pi.hProcess);
		CloseHandle(pi.hThread);
		return strResult;
}

ImgXY Util::getXY(char *result) {
	struct ImgXY ret = ImgXY(0, 0);

	char ** res = NULL;
	char *  p = strtok(result, "|");
	int n_spaces = 0;


	/* split string and append tokens to 'res' */

	while (p) {
		res = (char**)realloc(res, sizeof(char*) * ++n_spaces);

		if (res == NULL)
			exit(-1); /* memory allocation failed */

		res[n_spaces - 1] = p;

		p = strtok(NULL, "|");
	}

	/* realloc one extra element for the last NULL */

	res = (char**)realloc(res, sizeof(char*) * (n_spaces + 1));
	res[n_spaces] = 0;
	//TODO: Get random loc in square
	int x = atoi(res[1]);
	int y = atoi(res[2]);
	int width = atoi(res[3]);
	int height = atoi(res[4]);

	int centerX = ((x + width) - x) / 2;
	int centerY = ((y + height) - y) / 2;

	ret.x = x + centerX;
	ret.y = y + centerY;

	free(res);

	return ret;
}