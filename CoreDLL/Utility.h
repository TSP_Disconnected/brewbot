#pragma once

#define BB_CLOSED 50084
#define BB_UPDATE_GUI 50085

#define FB_ID_CONSOLE 170

constexpr auto NORTH = 1;
constexpr auto SOUTH = 2;
constexpr auto EAST = 1;
constexpr auto WEST = 2;

struct ImgXY {
public:
	int x;
	int y;

	ImgXY(int xx, int yy) {
		x = xx;
		y = yy;
	}
};

struct CupXY {
public:
	int x;
	int y;
	int EW = 0; // EAST or WEST 1 = E, 2 = W
	int NS = 0; // NORTH or SOUTH 1 = N, 2 = S
	bool moved = false;
	bool settled = false;

	CupXY(int xx, int yy) {
		x = xx;
		y = yy;
	}
};

class Util
{
public:
	//Get working directories
	static TCHAR *getCurrentDirectoryW();
	static char *getCurrentDirectoryA();
	static std::string Util::getCurrentDirectoryS();

	static std::string Util::execCommand(const std::wstring);

	static void Util::CaptureScreen(HWND window, const char* filename);
	static void Util::CaptureHDC(HDC hdc, const char* filename);
	static std::vector<unsigned char> Util::HDCtoMemBmp(HDC hdc);

	static ImgXY Util::getXY(char *result);

	static std::wstring Util::getBluestacksSharedFolderAndroid();
	static std::string Util::getBluestacksSharedFolderWindows();

	static void Util::AppendTextA(const HWND& hwnd, std::string newText, int item);

	static const std::string BotURL;
};

