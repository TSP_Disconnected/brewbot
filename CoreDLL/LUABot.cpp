#include "stdafx.h"
#include "LUABot.h"
#include "sol.hpp"
#include "Misc.h"
#include "OpenCV.h"
#include <sstream>
#include <TlHelp32.h>
#include "ThreadGuard.h"

#define BB_MAX_THREADS 10

int LUABot::my_exception_handler(lua_State* L, sol::optional<const std::exception&> maybe_exception, sol::string_view description) {
	// L is the lua state, which you can wrap in a state_view if necessary
	// maybe_exception will contain exception, if it exists
	// description will either be the what() of the exception or a description saying that we hit the general-case catch(...)
	if (maybe_exception) {
		const std::exception& ex = *maybe_exception;
		std::string msg = "An exception occurred in a function, here's what it says (straight from the exception):\n\t" + std::string(ex.what()) + "\n";
		MessageBoxA(NULL, msg.c_str(), "LUA Error", NULL);
	}
	else {
		std::string msg = "An exception occurred in a function, here's what it says (from the description parameter):\n\t" + std::string(description.data()) + "\n";
		MessageBoxA(NULL, msg.c_str(), "LUA Error", NULL);
	}

	// you must push 1 element onto the stack to be 
	// transported through as the error object in Lua
	// note that Lua -- and 99.5% of all Lua users and libraries -- expects a string
	// so we push a single string (in our case, the description of the error)
	return sol::stack::push(L, description);
}

void LUABot::my_panic(sol::optional<std::string> maybe_msg) {
	//mBot.AppendTextA(mBot.mainWin, "Lua is in a panic state and will now abort() the application", mBot.IDC_CONSOLE);
	if (maybe_msg) {
		const std::string& msg = "LUA Error: " + maybe_msg.value() + "\n\nBot will now exit";
		//mBot.AppendTextA(mBot.mainWin, msg.c_str(), mBot.IDC_CONSOLE);
		MessageBoxA(NULL, msg.c_str(), "LUA Error", NULL);
	}
	//error = true;
	// When this function exits, Lua will exhibit default behavior and abort()
}

void msgbox(const char* str) {
	MessageBoxA(NULL, str, str, NULL);
}

LUABot::LUABot(char* script, HWND mh, char* id)
{
	bStop = false;
	bPlay = false;

	//sol::state lua;

	lua.open_libraries(sol::lib::base, sol::lib::package, sol::lib::math, sol::lib::string, sol::lib::table);

	lua.set_panic(sol::c_call<decltype(&LUABot::my_panic), &LUABot::my_panic >);
	lua.set_exception_handler(&LUABot::my_exception_handler);

	//Set global functions
	lua.set_function("MsgBox", msgbox);

	//Setup platforms table
	sol::table PlatformsTable = lua.create_named_table("Platforms");
	PlatformsTable.set("PLATFORM_BLUESTACKS1", Bot::PLATFORM::PLATFORM_BLUESTACKS1);
	PlatformsTable.set("PLATFORM_BLUESTACKS2", Bot::PLATFORM::PLATFORM_BLUESTACKS2);
	PlatformsTable.set("PLATFORM_BLUESTACKS3", Bot::PLATFORM::PLATFORM_BLUESTACKS3);
	PlatformsTable.set("PLATFORM_NOX", Bot::PLATFORM::PLATFORM_NOX);

	sol::table ControlsTable = lua.create_named_table("Control");
	ControlsTable.set("LABEL", Bot::CONTROLS::LABEL);
	ControlsTable.set("CHECKBOX", Bot::CONTROLS::CHECKBOX);
	ControlsTable.set("BUTTON", Bot::CONTROLS::BUTTON);
	ControlsTable.set("RADIO_BUTTON", Bot::CONTROLS::RADIO_BUTTON);

	lua.new_usertype<Misc>("Misc",

		sol::constructors<Misc(std::string, bool)>(),

		"GET_HDC", &Misc::getWindowHDC,
		"SAVE_HDC", &Misc::saveHDCToBitmap,
		"DELETE_HDC", &Misc::deleteDC,
		"CAPTURE_SCREEN", &Misc::captureScreen,
		"GET_COLOR", &Misc::createColor,
		"REPLACE_COLOR", &Misc::replaceColor,
		"REPLACE_COLOR_WITH_TOL", &Misc::replaceColorWithTol,
		"REPLACE_ALL_COLORS_EXCEPT", &Misc::replaceAllColorsExcept,
		"CROP_HDC", &Misc::cropHDC,
		"READ_IMAGE_TEXT", &Misc::readImageText,
		"READ_HDC_TEXT", &Misc::readHDCText,
		"LOAD_IMAGE", &Misc::LoadImg,
		"COPY_HDC_TO_SCREEN", &Misc::copyHDCToScreen,
		"DISTANCE_BETWEEN_POINTS", &Misc::Distance,
		"REPLACE_PIXEL", &Misc::replacePixel

		);

	lua.new_usertype<OpenCV>("OpenCVScript",

		sol::constructors<OpenCV(std::string, bool, HWND)>(),

		"MULTI_TEMPLATE_MATCH_VIDEO", &OpenCV::multiTemplateMatchVideo,
		"TEMPLATE_MATCH_VIDEO", &OpenCV::templateMatchVideo,
		"TEMPLATE_MATCH", &OpenCV::templateMatch,
		"OPEN_VIDEO", &OpenCV::openVideo,
		"CLOSE_VIDEO", & OpenCV::closeVideo,
		"IS_FRAME_EMPTY", &OpenCV::frameEmpty,
		"IS_VIDEO_OPEN", &OpenCV::isCapOpened,
		"WAIT_KEY", &OpenCV::waitKey,
		"RECT_ARRAY_TO_TABLE", & OpenCV::RectArrayToMap,
		"RECT_TO_TABLE", & OpenCV::RectToMap,
		"NEXT_FRAME_TO_BMP", & OpenCV::ConvertCVMatToBMP,
		"CAPTURE_NEXT_FRAME", & OpenCV::captureNextFrame,
		"RUN_MULTI_TRACKER_VIDEO", &OpenCV::runMultiTrackerVideo

		);

	lua.new_usertype<BotWrapper>("BrewBot",

		//sol::constructors<BotWrapper(Bot &bot)>(),

		"BOOT_PLATFORM", &BotWrapper::bootPlatform,
		"KILL_PLATFORM", & BotWrapper::killPlatform,
		"WAIT_FOR_PLATFORM", &BotWrapper::waitForPlatform,
		"IS_ADB_ALIVE", &BotWrapper::isAlive,
		"RESTART_ADB", &BotWrapper::restartAdb,
		"CONNECT_ADB", &BotWrapper::initADB,
		"START_APP", &BotWrapper::startApp,
		"CLICK_XY", &BotWrapper::clickXY,
		"FIND_CLICK_IMAGE", &BotWrapper::findAndClickImage,
		"FIND_CLICK_IMAGE_WITH_TOL", &BotWrapper::findAndClickImageWithTol,
		"FIND_IMAGE", &BotWrapper::findImage,
		"FIND_IMAGE_WITH_XY", &BotWrapper::findImageWithXY,
		"WAIT_FOR_IMAGE", &BotWrapper::waitForImage,
		"WAIT_CLICK_IMAGE", &BotWrapper::waitClickImage,
		"WAIT_CLICK_IMAGE_WITH_TIMEOUT", &BotWrapper::waitClickImageWithTimeout,
		"WAIT", &BotWrapper::wait,
		"PRINT", &BotWrapper::AppendTextA,
		"GET_TIME", &BotWrapper::getTime,
		"GET_WINDOW", &BotWrapper::getAndroidWindow, //TODO Change to function that detects running emulator
		"FIND_IMAGE_IN_IMAGE", &BotWrapper::searchImageForImage,
		"FIND_IMAGE_IN_BMP", & BotWrapper::searchBMPForImage,
		"FIND_IMAGES_IN_IMAGE", &BotWrapper::searchImageForImages,
		"PROCESS_NEXT_UI_EVENT", &BotWrapper::processNextUIEvent,
		"CREATE_CONTROL", &BotWrapper::createControl,
		"CREATE_CONTROL_FIXED_ID", &BotWrapper::createControlFixedID,
		"TOGGLE_CONTROL", &BotWrapper::toggleControl,
		"UPDATE_GUI", &BotWrapper::updateGUI,
		"GET_GUI_WINDOW", &BotWrapper::getMainWin,
		"SET_CONTROL_TEXT", &BotWrapper::setControlText,
		"START_RECORD", &BotWrapper::startRecord,
		"STOP_RECORD", &BotWrapper::stopRecord,
		"SET_STATUS_MSG", &BotWrapper::setStatusBar,
		"GET_RUN_TIME", &BotWrapper::getRunTimeStr,
		"GET_GLOBAL", &BotWrapper::getGlobal,
		"SET_GLOBAL", &BotWrapper::setGlobal,

		"CREATE_THREAD", &BotWrapper::createThread,

		// gets or set the value using member variable syntax
		"IS_DEBUG", sol::property(&BotWrapper::getDebug, &BotWrapper::setDebug),
		"IS_RUNNING", sol::property(&BotWrapper::getRunning, &BotWrapper::setRunning),
		"IS_PLAYING", sol::property(&BotWrapper::getPlaying, &BotWrapper::setPlaying),

		// read and write variable
		//"speed", &player::speed,
		// can only read from, not write to
		"CONSOLE", sol::readonly(&BotWrapper::ID_CONSOLE),
		"DEFAULT_TOLERANCE", sol::readonly(&BotWrapper::dTol)
		);


	lua.script(R"(
			function handler (message)
				return "LUA Error: " .. message
			end
		)");

	//TODO Automatically handle runtime timer
	std::string check(id);
	std::string find("[L]");

	std::size_t found = check.find(find);

	std::string newScript;

	if (found != std::string::npos) {
		std::string sub = check.substr(3);
		botID = sub.c_str();
		local = true;
		scriptFolder = Util::getCurrentDirectoryS() + std::string("\\scripts\\local\\") + botID + std::string("\\");
	}else{
		botID = id;
		local = false;
		scriptFolder = Util::getCurrentDirectoryS() + std::string("\\scripts\\") + botID + std::string("\\");
	}

	newScript = scriptFolder + "main.lua";
	mBot.init(mh, botID, local);

	lua["Misc"] = Misc(botID, local);
	lua["OpenCV"] = OpenCV(botID, local, mh);
	lua["Bot"] = BotWrapper(std::ref(mBot));

	lua["IS_LOCAL"] = local;

	try {
		lua.script_file(newScript.c_str());
	}
	catch (const sol::error& e) {
		std::string errorStr = "LUA Error: " + std::string(e.what()) + "\n";
		mBot.AppendTextA(mBot.mainWin, errorStr, mBot.IDC_CONSOLE);
	}

	//scriptName = lua["getName"](); //Working

	sol::protected_function start(lua["start"], lua["handler"]);

	sol::protected_function_result result = start();

	if (!result.valid()) {
		// Call failed
		sol::error err = result;
		std::string what = std::string(err.what()) + "\n";
		mBot.AppendTextA(mBot.mainWin, what.c_str(), mBot.IDC_CONSOLE);
	}

	//lua["start"]();
	bPlay = mBot.getPlaying();
	mBot.startHasRun = true;
}

void LUABot::threadFunction(Bot &bot, ThreadArgs args) {
	sol::state luaThread;

	luaThread.open_libraries(sol::lib::base, sol::lib::package, sol::lib::math, sol::lib::string);

	luaThread.set_panic(sol::c_call<decltype(&LUABot::my_panic), &LUABot::my_panic > );
	luaThread.set_exception_handler(&LUABot::my_exception_handler);

	//Set global functions
	luaThread.set_function("MsgBox", msgbox);

	//Setup platforms table
	sol::table PlatformsTable = luaThread.create_named_table("Platforms");
	PlatformsTable.set("PLATFORM_BLUESTACKS1", Bot::PLATFORM::PLATFORM_BLUESTACKS1);
	PlatformsTable.set("PLATFORM_BLUESTACKS2", Bot::PLATFORM::PLATFORM_BLUESTACKS2);
	PlatformsTable.set("PLATFORM_BLUESTACKS3", Bot::PLATFORM::PLATFORM_BLUESTACKS3);
	PlatformsTable.set("PLATFORM_NOX", Bot::PLATFORM::PLATFORM_NOX);

	sol::table ControlsTable = luaThread.create_named_table("Control");
	ControlsTable.set("LABEL", Bot::CONTROLS::LABEL);
	ControlsTable.set("CHECKBOX", Bot::CONTROLS::CHECKBOX);
	ControlsTable.set("BUTTON", Bot::CONTROLS::BUTTON);
	ControlsTable.set("RADIO_BUTTON", Bot::CONTROLS::RADIO_BUTTON);

	luaThread.new_usertype<Misc>("Misc",

		sol::constructors<Misc(std::string, bool)>(),

		"GET_HDC", &Misc::getWindowHDC,
		"SAVE_HDC", &Misc::saveHDCToBitmap,
		"DELETE_HDC", &Misc::deleteDC,
		"CAPTURE_SCREEN", &Misc::captureScreen,
		"GET_COLOR", &Misc::createColor,
		"REPLACE_COLOR", &Misc::replaceColor,
		"REPLACE_COLOR_WITH_TOL", &Misc::replaceColorWithTol,
		"REPLACE_ALL_COLORS_EXCEPT", &Misc::replaceAllColorsExcept,
		"CROP_HDC", &Misc::cropHDC,
		"READ_IMAGE_TEXT", &Misc::readImageText,
		"READ_HDC_TEXT", &Misc::readHDCText,
		"LOAD_IMAGE", &Misc::LoadImg,
		"COPY_HDC_TO_SCREEN", &Misc::copyHDCToScreen,
		"DISTANCE_BETWEEN_POINTS", &Misc::Distance,
		"REPLACE_PIXEL", &Misc::replacePixel

		);

	luaThread.new_usertype<OpenCV>("OpenCVScript",

		sol::constructors<OpenCV(std::string, bool, HWND)>(),

		"MULTI_TEMPLATE_MATCH_VIDEO", & OpenCV::multiTemplateMatchVideo,
		"TEMPLATE_MATCH_VIDEO", & OpenCV::templateMatchVideo,
		"TEMPLATE_MATCH", & OpenCV::templateMatch,
		"OPEN_VIDEO", &OpenCV::openVideo,
		"CLOSE_VIDEO", &OpenCV::closeVideo,
		"IS_FRAME_EMPTY", &OpenCV::frameEmpty,
		"IS_VIDEO_OPEN", &OpenCV::isCapOpened,
		"WAIT_KEY", &OpenCV::waitKey,
		"RECT_ARRAY_TO_TABLE", &OpenCV::RectArrayToMap,
		"RECT_TO_TABLE", &OpenCV::RectToMap,
		"NEXT_FRAME_TO_BMP", &OpenCV::ConvertCVMatToBMP,
		"CAPTURE_NEXT_FRAME", &OpenCV::captureNextFrame,
		"RUN_MULTI_TRACKER_VIDEO", &OpenCV::runMultiTrackerVideo

		);

	luaThread.new_usertype<BotWrapper>("BrewBot",

		//sol::constructors<BotWrapper(Bot &bot)>(),

		"BOOT_PLATFORM", &BotWrapper::bootPlatform,
		"KILL_PLATFORM", &BotWrapper::killPlatform,
		"WAIT_FOR_PLATFORM", &BotWrapper::waitForPlatform,
		"IS_ADB_ALIVE", &BotWrapper::isAlive,
		"RESTART_ADB", &BotWrapper::restartAdb,
		"CONNECT_ADB", &BotWrapper::initADB,
		"START_APP", &BotWrapper::startApp,
		"CLICK_XY", &BotWrapper::clickXY,
		"FIND_CLICK_IMAGE", &BotWrapper::findAndClickImage,
		"FIND_CLICK_IMAGE_WITH_TOL", &BotWrapper::findAndClickImageWithTol,
		"FIND_IMAGE", &BotWrapper::findImage,
		"FIND_IMAGE_WITH_XY", &BotWrapper::findImageWithXY,
		"WAIT_FOR_IMAGE", &BotWrapper::waitForImage,
		"WAIT_CLICK_IMAGE", &BotWrapper::waitClickImage,
		"WAIT_CLICK_IMAGE_WITH_TIMEOUT", &BotWrapper::waitClickImageWithTimeout,
		"WAIT", &BotWrapper::wait,
		"PRINT", &BotWrapper::AppendTextA,
		"GET_TIME", &BotWrapper::getTime,
		"GET_WINDOW", &BotWrapper::getAndroidWindow, //TODO Change to function that detects running emulator
		"FIND_IMAGE_IN_IMAGE", &BotWrapper::searchImageForImage,
		"FIND_IMAGE_IN_BMP", &BotWrapper::searchBMPForImage,
		"FIND_IMAGES_IN_IMAGE", &BotWrapper::searchImageForImages,
		"PROCESS_NEXT_UI_EVENT", &BotWrapper::processNextUIEvent,
		"CREATE_CONTROL", &BotWrapper::createControl,
		"CREATE_CONTROL_FIXED_ID", &BotWrapper::createControlFixedID,
		"TOGGLE_CONTROL", &BotWrapper::toggleControl,
		"UPDATE_GUI", &BotWrapper::updateGUI,
		"GET_GUI_WINDOW", &BotWrapper::getMainWin,
		"SET_CONTROL_TEXT", &BotWrapper::setControlText,
		"START_RECORD", &BotWrapper::startRecord,
		"STOP_RECORD", &BotWrapper::stopRecord,
		"SET_STATUS_MSG", &BotWrapper::setStatusBar,
		"GET_RUN_TIME", &BotWrapper::getRunTimeStr,
		"GET_GLOBAL", &BotWrapper::getGlobal,
		"SET_GLOBAL", &BotWrapper::setGlobal,

		"CREATE_THREAD", &BotWrapper::createThread,

		// gets or set the value using member variable syntax
		"IS_DEBUG", sol::property(&BotWrapper::getDebug, &BotWrapper::setDebug),
		"IS_RUNNING", sol::property(&BotWrapper::getRunning, &BotWrapper::setRunning),
		"IS_PLAYING", sol::property(&BotWrapper::getPlaying, &BotWrapper::setPlaying),

		// read and write variable
		//"speed", &player::speed,
		// can only read from, not write to
		"CONSOLE", sol::readonly(&BotWrapper::ID_CONSOLE),
		"DEFAULT_TOLERANCE", sol::readonly(&BotWrapper::dTol)
		);

	luaThread.script(R"(
			function handler (message)
				return "LUA Error in Thread: " .. message
			end
		)");

	luaThread["Misc"] = Misc(args.botID, args.local);
	luaThread["Bot"] = BotWrapper(bot);
	
	try {
		luaThread.script_file(args.script);
	}
	catch (const sol::error& e) {
		std::string errorStr = "LUA Error in Thread: " + std::string(e.what()) + "\n";
		bot.AppendTextA(bot.mainWin, errorStr, bot.IDC_CONSOLE);
	}

	do {
		if (bot.getPlaying()) {
			sol::protected_function main(luaThread["main"], luaThread["handler"]);

			sol::protected_function_result result = main();

			if (!result.valid()) {
				// Call failed
				sol::error err = result;
				std::string what = std::string(err.what()) + "\n";
				mBot.AppendTextA(mBot.mainWin, what.c_str(), mBot.IDC_CONSOLE);
				args.loop = false;
			}
			Sleep(75);
		}
		else {
			Sleep(500);
		}
	} while (args.loop && bot.getRunning());

}

void LUABot::loop()
{

	std::vector<std::thread> runningThreads;
	std::vector<ThreadInfo> procThreads; //Processed threads
	std::vector<ThreadGuard> threadGuards;

	while (mBot.getRunning() && !error) {

		sol::protected_function loop(lua["loop"], lua["handler"]);

		sol::protected_function_result result = loop();

		if (!result.valid()) {
			// Call failed
			sol::error err = result;
			std::string what = std::string(err.what()) + "\n";
			mBot.AppendTextA(mBot.mainWin, what.c_str(), mBot.IDC_CONSOLE);
			mBot.setRunning(false);
		}

		//Process thread creation
		//TODO Reset thread id counter if no active threads running; store std::thread variable
		if (procThreads.size() < BB_MAX_THREADS) {
			std::vector<ThreadInfo> ThreadQueue = mBot.getThreadQueue();
			for each (ThreadInfo tQ in ThreadQueue)
			{
				bool skipThread = false;
				for each (ThreadInfo pQ in procThreads) {
					if (pQ.threadID == tQ.threadID) {
						skipThread = true;
						break;
					}
				}
				if (skipThread)
					break;
				std::string script = scriptFolder + tQ.functionName + std::string(".lua");
				ThreadArgs ta;
				ta.botID = botID;
				ta.local = local;
				ta.loop = tQ.shouldRun;
				ta.script = script;
				std::thread newThread(&LUABot::threadFunction, this, std::ref(mBot), ta);
				//newThread.detach(); //TODO move non looping threads to new function; add function that queues non loop threads; add second function that runs and joins non looped threads
				runningThreads.push_back(std::move(newThread));
				//threadGuards.push_back(std::move(ThreadGuard(runningThreads.at(runningThreads.size() - 1))));
				procThreads.push_back(tQ);
			}
		}

		Sleep(10);

		if (bStop) {
			mBot.setRunning(false);
			bPlay = false;
			mBot.setPlaying(bPlay);
			Sleep(1000);
		}
		mBot.setPlaying(bPlay);
	}
	
	for (std::thread & th : runningThreads)
	{
		if (th.joinable())
			th.join();
		
		//th.~thread();

	}

	//mBot.deleteUIElements();
	mBot.freeLibraries();
	//PostMessage(mBot.mainWin, BB_CLOSED, NULL, NULL);
}

void LUABot::addSecond()
{
	mBot.addSecond();
}

LUABot::~LUABot()
{
}

bool LUABot::GetPlaying() {
	return bPlay;
}

void LUABot::Stop()
{
	bStop = true;
}

void LUABot::Play()
{
	bPlay = true;
}

void LUABot::clearUIQueue() {
	mBot.clearUIQueue();
}

void LUABot::clearUIElements() {
	mBot.clearUIVector();
}

std::vector<int> LUABot::getUIElements() {
	return mBot.getUIElements();
}

std::vector<UIElementS> LUABot::getUIQueue() {
	return mBot.getUIQueue();
}

void LUABot::addToUIVector(int id) {
	mBot.addToUIVector(id);
}

void LUABot::Pause()
{
	bPlay = false;
}

void LUABot::QueueUIEvent(int ElementID){
	mBot.addUIEvent(ElementID);
}

void LUABot::DeleteUIElements() {
	mBot.deleteUIElements();
}

bool LUABot::HasUIElement(int ElementID) {
	return mBot.hasUIElement(ElementID);
}