#include "stdafx.h"
#include "main.h"
#include "dll.h"
#include "cJSON.h"
#include "Utility.h"
#include "LUABot.h"
#include "miniz.h"
#include <shellapi.h>

#include <Richedit.h>

#include "inc/curl/curl.h"

#pragma comment( lib, "comctl32.lib" )
#pragma comment(lib, "wininet.lib")

#define MAX_LOADSTRING 100

#define IDC_MAIN_LIST 150
#define IDC_MAIN_DESCBOX 151
#define IDC_MAIN_MNBTN 152
#define IDC_MAIN_PLAYBTN 153
#define IDC_MAIN_PAUSEBTN 154
#define IDC_MAIN_STOPBTN 155
#define IDC_AUTH_TIMER 156
#define IDC_STATUSBAR 157
#define IDC_CONSOLE 170
#define IDT_LUA_TIMER 130

//Bot Types
#define BOT_DLL 1
#define BOT_LUA 2

// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name

												// Forward declarations of functions included in this code module:
ATOM                MyRegisterClassCore(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProcCore(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);

//UI elements
HWND hWndLoadButton;
HWND hWndPlayButton;
HWND hWndStopButton;
HWND hWndPauseButton;

//Bot things
HINSTANCE botDLL;
RunPtr rb;
HANDLE runThread;

LUABot* luaBot;

std::string u;
std::string p;
char* botURL;

bool reAuth();

struct LUABotS {
	char file[4096];
	HWND hWnd;
};

struct LocalBot {
	std::string name;
	std::string folder;
};

struct BotInfo {
	char name[1024];
	char url[2048];
	char dll[1024];
	char id[1024];
	int type;
};

BotInfo selBot;
BotInfo runningBot;

std::vector<LocalBot> localBots;

struct string {
	char *ptr;
	size_t len;
};

int silently_remove_directory(const char* dir) // Fully qualified name of the directory being   deleted,   without trailing backslash
{
	int len = strlen(dir) + 2; // required to set 2 nulls at end of argument to SHFileOperation.
	char* tempdir = (char*)malloc(len);
	memset(tempdir, 0, len);
	strcpy(tempdir, dir);

	SHFILEOPSTRUCTA file_op = {
	  NULL,
	  FO_DELETE,
	  tempdir,
	  "",
	  FOF_NOCONFIRMATION |
	  FOF_NOERRORUI |
	  FOF_SILENT,
	  false,
	  0,
	  "" };
	int ret = SHFileOperationA(&file_op);
	free(tempdir);
	return ret; // returns 0 on success, non zero on failure.
}

std::vector<std::string> unzip(std::string const& zipFile, std::string const& path, std::string const& password)
{
	std::vector<std::string> files = {};
	mz_zip_archive zip_archive;
	memset(&zip_archive, 0, sizeof(zip_archive));

	auto status = mz_zip_reader_init_file(&zip_archive, zipFile.c_str(), 0);
	if (!status) return files;
	int fileCount = (int)mz_zip_reader_get_num_files(&zip_archive);
	if (fileCount == 0)
	{
		mz_zip_reader_end(&zip_archive);
		return files;
	}
	mz_zip_archive_file_stat file_stat;
	if (!mz_zip_reader_file_stat(&zip_archive, 0, &file_stat))
	{
		mz_zip_reader_end(&zip_archive);
		return files;
	}
	// Get root folder
	std::string lastDir = "";
	std::string base = file_stat.m_filename; // path delim on end

	// Get and print information about each file in the archive.
	for (int i = 0; i < fileCount; i++)
	{
		if (!mz_zip_reader_file_stat(&zip_archive, i, &file_stat)) continue;
		if (mz_zip_reader_is_file_a_directory(&zip_archive, i)) continue; // skip directories for now
		std::string fileName = file_stat.m_filename; // make path relative
		std::string destFile = path + file_stat.m_filename; // make full dest path

		std::size_t found = fileName.find_last_of("/\\");

		std::string newDir = fileName.substr(0, found); // get the file's path
		if (newDir != lastDir)
		{
			if (!CreateDirectoryA(std::string(path + newDir).c_str(), NULL)) // creates the directory
			{

			}
			lastDir = newDir;
		}

		if (mz_zip_reader_extract_to_file(&zip_archive, i, destFile.c_str(), 0))
		{
			files.emplace_back(destFile);
		}
	}

	files.insert(files.begin(), base);

	// Close the archive, freeing any resources it was using
	mz_zip_reader_end(&zip_archive);
	return files;
}

void init_string(struct string *s) {
	s->len = 0;
	s->ptr = (char *)malloc(s->len + 1);
	if (s->ptr == NULL) {
		fprintf(stderr, "malloc() failed\n");
		exit(EXIT_FAILURE);
	}
	s->ptr[0] = '\0';
}

VOID runTimeFunc(HWND hwnd, UINT message, UINT idTimer, DWORD dwTime)
{
	if (!reAuth()) {
		if (botDLL) {
			DestroyWindowPtr dwp = (DestroyWindowPtr)GetProcAddress(botDLL, "destroyWindow");
			(*dwp)();
			SetRunPtr srp = (SetRunPtr)GetProcAddress(botDLL, "setRunning");
			(*srp)(false);
			WaitForSingleObject(runThread, INFINITE);
		}
		PostMessage(hwnd, WM_CLOSE, 0, 0);
	}
}

size_t writefunc(void *ptr, size_t size, size_t nmemb, struct string *s)
{
	size_t new_len = s->len + size*nmemb;
	s->ptr = (char *)realloc(s->ptr, new_len + 1);
	if (s->ptr == NULL) {
		fprintf(stderr, "realloc() failed\n");
		exit(EXIT_FAILURE);
	}
	memcpy(s->ptr + s->len, ptr, size*nmemb);
	s->ptr[new_len] = '\0';
	s->len = new_len;

	return size*nmemb;
}

size_t curlWriteFile(void *ptr, size_t size, size_t nmemb, FILE *stream) {
	size_t written = fwrite(ptr, size, nmemb, stream);
	return written;
}

bool downloadBotLUA(const char* zipfile) {
	CURL *curl;
	FILE *fp;
	CURLcode res;

	curl = curl_easy_init();
	if (curl) {
		fp = fopen(zipfile, "wb");
		curl_easy_setopt(curl, CURLOPT_URL, selBot.url);
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, curlWriteFile);
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, fp);
		res = curl_easy_perform(curl);
		/* always cleanup */
		curl_easy_cleanup(curl);
		fclose(fp);
		return true;
	}
	else {
		printf("Failed to download update!");
		return false;
	}
}

bool downloadBotDLL(char* dllname) {
#ifdef _DEBUG
	return true;
#endif
	CURL *curl;
	FILE *fp;
	CURLcode res;
	char outfilename[FILENAME_MAX];
	sprintf(outfilename, dllname, Util::getCurrentDirectoryA());
	curl = curl_easy_init();
	if (curl) {
		fp = fopen(outfilename, "wb");
		curl_easy_setopt(curl, CURLOPT_URL, selBot.url);
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, curlWriteFile);
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, fp);
		res = curl_easy_perform(curl);
		/* always cleanup */
		curl_easy_cleanup(curl);
		fclose(fp);
		return true;
	}
	else {
		printf("Failed to download update!");
		return false;
	}
}

bool reAuth() {
	struct string s;
	init_string(&s);
	CURL *curl;
	struct curl_httppost *formpost = NULL;
	struct curl_httppost *lastptr = NULL;
	curl = curl_easy_init();
	if (curl) {
		curl_formadd(&formpost,
			&lastptr,
			CURLFORM_COPYNAME, "apikey",
			CURLFORM_COPYCONTENTS, "YOURAPIKEYHERE",
			CURLFORM_END);
		curl_formadd(&formpost,
			&lastptr,
			CURLFORM_COPYNAME, "username",
			CURLFORM_COPYCONTENTS, u.c_str(),
			CURLFORM_END);
		curl_formadd(&formpost,
			&lastptr,
			CURLFORM_COPYNAME, "password",
			CURLFORM_COPYCONTENTS, p.c_str(),
			CURLFORM_END);
		curl_easy_setopt(curl, CURLOPT_URL, "https://www.freebrew.org/api.php?action=login");
		int code;
		//curl_easy_setopt(curl, CURLOPT_POST, 1);
		//curl_easy_setopt(curl, CURLOPT_POSTFIELDS, "apikey=YOURAPIKEYHERE&username=Mr. Chip53&password=Bobbers851");
		curl_easy_setopt(curl, CURLOPT_HTTPPOST, formpost);
		code = curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, writefunc);
		if (code != CURLE_OK) {
			//fprintf(stderr, "Failed to set writer [%s]\n", errorBuffer);
			return false;
		}
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, &s);
		int res = curl_easy_perform(curl);
		curl_easy_cleanup(curl);
		curl_formfree(formpost);
		/* free slist */
		cJSON * root = cJSON_Parse(s.ptr);
		bool loggedIn = cJSON_GetObjectItem(root, "Success")->valueint;
		bool isPrem = cJSON_GetObjectItem(root, "Premium")->valueint;
#ifdef _DEBUG
		loggedIn = true;
		isPrem = true;
#endif
		if (loggedIn && isPrem) {
			cJSON_Delete(root);
			return true;
		}
		else {
			cJSON_Delete(root);
			return false;
		}
		cJSON_Delete(root);
	}
	else {
		return false;
	}
}

const wchar_t *GetWC(const char *c)
{
	const size_t cSize = strlen(c) + 1;
	wchar_t* wc = new wchar_t[cSize];
	mbstowcs(wc, c, cSize);

	return wc;
}

//LUA Bot Thread
DWORD WINAPI runLUABot(LPVOID lpParameter) {
	Sleep(5000);
	luaBot->loop();
	return 0;
}

//Load bots thread
DWORD WINAPI loadBotsThread(LPVOID lpParameter)
{
	HWND hWnd = (HWND)lpParameter;
	struct string s;
	init_string(&s);
	//Load bots json
	CURL *curl = curl_easy_init();
	if (curl) {
		std::string jsonURL = Util::BotURL + "bots.json";
		curl_easy_setopt(curl, CURLOPT_URL, jsonURL.c_str());
		int code;
		code = curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, writefunc);
		if (code != CURLE_OK) {
			//fprintf(stderr, "Failed to set writer [%s]\n", errorBuffer);
			return -1;
		}
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, &s);
		int res = curl_easy_perform(curl);
		curl_easy_cleanup(curl);
		cJSON * root = cJSON_Parse(s.ptr);
		cJSON * botsobj = cJSON_GetObjectItem(root, "bots");
		int numBots = cJSON_GetObjectItem(root, "numBots")->valueint;
		for (int i = 0; i < numBots; i++) {
			cJSON * botsArrayitem = cJSON_GetArrayItem(botsobj, i);
			char str[88];
			sprintf(str, "%s", cJSON_GetObjectItem(botsArrayitem, "name")->valuestring);
			SendDlgItemMessage(hWnd, IDC_MAIN_LIST, LB_ADDSTRING, 0, (LPARAM)GetWC(str));
		}
		cJSON_Delete(root);
		//return 0;
	}

	//Scan for local bots
	WIN32_FIND_DATAA foundFile;
	HANDLE hFind = INVALID_HANDLE_VALUE;

	std::string path = Util::getCurrentDirectoryA();
	path.append("\\scripts\\local\\*");

	hFind = FindFirstFileA(path.c_str(), &foundFile);
	if (hFind != INVALID_HANDLE_VALUE)
	{
		do
		{
			if ((foundFile.dwFileAttributes | FILE_ATTRIBUTE_DIRECTORY) == FILE_ATTRIBUTE_DIRECTORY
				&& (foundFile.cFileName[0] != '.'))
			{
				std::string folder;
				folder = "local\\" + std::string(foundFile.cFileName);
				LocalBot newBot;
				newBot.folder = folder;
				newBot.name = std::string(foundFile.cFileName);
				localBots.push_back(newBot);
				std::wstring name = L"[L]" + std::wstring(GetWC(foundFile.cFileName));
				SendDlgItemMessage(hWnd, IDC_MAIN_LIST, LB_ADDSTRING, 0, (LPARAM)name.c_str());
			}
		} while (FindNextFileA(hFind, &foundFile) != 0);
	}


	return 0;
}

DWORD WINAPI runBot(LPVOID funcptr) {
	EnableWindow(hWndPlayButton, true);
	EnableWindow(hWndPauseButton, false);
	return (*rb)();
}

void WINAPI initUser(char *username, char *pass) {
	p = pass;
	u = username;
}

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
	_In_opt_ HINSTANCE hPrevInstance,
	_In_ LPWSTR    lpCmdLine,
	_In_ int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	//Check internet first
	if (!InternetCheckConnection(L"http://www.freebrew.org/", FLAG_ICC_FORCE_CONNECTION, NULL)) {
		MessageBoxA(NULL, "Please check your internet connection and try again!", "Could not connect to the internet!", NULL);
		return FALSE;
	}
	//MessageBoxA(NULL, "Hello", "HELLO", NULL);
	// Initialize global strings
	LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	//LoadStringW(hInstance, IDC_FREEBREWBOT_CORE, szWindowClass, MAX_LOADSTRING);
	if (MyRegisterClassCore(hInstance) == 0) {
		MessageBoxA(NULL, "Error creating class", "Error", NULL);
		return FALSE;
	}

	// Perform application initialization:
	if (!InitInstance(hInstance, nCmdShow))
	{
		return FALSE;
	}

	/*HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_FREEBREWBOT));

	MSG msg;

	// Main message loop:
	while (GetMessage(&msg, nullptr, 0, 0))
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return (int)msg.wParam;*/
	return true;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClassCore(HINSTANCE hInstance)
{
	WNDCLASSEXW wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = WndProcCore;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInstance;
	wcex.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_BEER));
	wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszMenuName = MAKEINTRESOURCEW(IDC_FREEBREWBOT);
	wcex.lpszClassName = L"FB_CORE_DLL";
	wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_BEER));

	return RegisterClassExW(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
	hInst = hInstance; // Store instance handle in our global variable

	LoadLibraryA("riched20.dll"); //TODO Unload when stoppedfr

	HWND hWnd = CreateWindowW(L"FB_CORE_DLL", szTitle, WS_OVERLAPPEDWINDOW ^ (WS_MAXIMIZEBOX | WS_THICKFRAME) | WS_CAPTION | WS_SYSMENU,
		CW_USEDEFAULT, CW_USEDEFAULT, 500, 725, nullptr, nullptr, hInstance, nullptr);

	if (!hWnd)
	{
		return FALSE;
	}

	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	//LUABot("C:\\Users\\Chris\\Documents\\Visual Studio 2015\\Projects\\FreeBrewBot\\Debug\\test.lua", hWnd);
	
	//LUABotS* luaBot = (LUABotS *)malloc(sizeof(LUABotS));
	//luaBot->file = "None";
	//luaBot->hWnd = hWnd;

	char str[MAX_PATH];
	sprintf(str, "%s\\%s", Util::getCurrentDirectoryA(), "scripts");
	CreateDirectoryA(str, NULL);

	CreateThread(0, 0, loadBotsThread, (LPVOID)hWnd, 0, NULL);
	UINT_PTR id = SetTimer(hWnd, IDC_AUTH_TIMER, 1000*60*60, (TIMERPROC)runTimeFunc);

	return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//
LRESULT CALLBACK WndProcCore(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case BB_UPDATE_GUI:
	{
		//Delete current ui elements
		std::vector<int> UIElements = luaBot->getUIElements();
		//Clear UI element vector
		luaBot->clearUIElements();
		if (UIElements.size() > 0) {
			for each(int id in UIElements) {
				DestroyWindow(GetDlgItem(hWnd, id));
			}
		}

		//Create UI in element queue
		std::vector<UIElementS> UIQueue = luaBot->getUIQueue();
		//Clear ui element queue
		luaBot->clearUIQueue();
		for each(UIElementS s in UIQueue) {
			switch (s.type) {
			case Bot::LABEL:
				CreateWindowA("STATIC", s.text.c_str(), WS_CHILD | WS_VISIBLE, s.x, s.y, s.width, s.height, hWnd, (HMENU)s.id, GetModuleHandle(NULL), NULL);
				break;
			case Bot::BUTTON:
				CreateWindowA("BUTTON", s.text.c_str(), WS_CHILD | WS_VISIBLE | BS_DEFPUSHBUTTON, s.x, s.y, s.width, s.height, hWnd, (HMENU)s.id, GetModuleHandle(NULL), NULL);
				break;
			case Bot::CHECKBOX:
				CreateWindowA("BUTTON", s.text.c_str(), WS_CHILD | WS_VISIBLE | BS_CHECKBOX, s.x, s.y, s.width, s.height, hWnd, (HMENU)s.id, GetModuleHandle(NULL), NULL);
				break;
			case Bot::RADIO_BUTTON:
				CreateWindowA("BUTTON", s.text.c_str(), WS_CHILD | WS_VISIBLE | BS_RADIOBUTTON, s.x, s.y, s.width, s.height, hWnd, (HMENU)s.id, GetModuleHandle(NULL), NULL);
				break;
			}
			luaBot->addToUIVector(s.id);
		}
	}
	break;
	case BB_CLOSED:
	{
		luaBot->Stop();
		luaBot->DeleteUIElements();
		//delete luaBot; Wait for threads
		EnableWindow(hWndPlayButton, false);
		EnableWindow(hWndStopButton, false);
		EnableWindow(hWndLoadButton, true);
		EnableWindow(hWndPauseButton, false);
		//DestroyWindow(GetDlgItem(hWnd, IDC_CONSOLE));
		WaitForSingleObject(runThread, INFINITE);
		MessageBoxA(NULL, "Bot closed", "Closed", NULL);
	}
	break;
	case WM_CREATE:
	{
		//Create listbox
		HWND hListBox = CreateWindowExW(WS_EX_CLIENTEDGE, L"LISTBOX", NULL, LBS_NOTIFY | WS_CHILD | WS_VISIBLE | ES_AUTOVSCROLL, 10, 10, 230, 200, hWnd, (HMENU)IDC_MAIN_LIST, GetModuleHandle(NULL), NULL);
		//Create description box
		HWND hDescBox = CreateWindowExW(WS_EX_CLIENTEDGE, L"Edit", L"Click a bot to load the description!", WS_CHILD | ES_READONLY | WS_VISIBLE | WS_VSCROLL | ES_LEFT | ES_MULTILINE | ES_AUTOVSCROLL, 250, 10, 230, 196, hWnd, (HMENU)IDC_MAIN_DESCBOX, GetModuleHandle(NULL), NULL);
		//Create main button
		hWndLoadButton = CreateWindowExW(NULL, L"BUTTON", L"Select a bot!", WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON | WS_DISABLED, 10, 210, 470, 24, hWnd, (HMENU)IDC_MAIN_MNBTN, GetModuleHandle(NULL), NULL);
		//Create Pause/Play button
		hWndPlayButton = CreateWindowExW(NULL, L"BUTTON", L"Play", WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON | WS_DISABLED, 10, 239, 230, 24, hWnd, (HMENU)IDC_MAIN_PLAYBTN, GetModuleHandle(NULL), NULL);
		//Create Pause/Play button
		hWndPauseButton = CreateWindowExW(NULL, L"BUTTON", L"Pause", WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON | WS_DISABLED, 250, 239, 230, 24, hWnd, (HMENU)IDC_MAIN_PAUSEBTN, GetModuleHandle(NULL), NULL);
		//Create Pause/Play button
		hWndStopButton = CreateWindowExW(NULL, L"BUTTON", L"Stop Bot", WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON | WS_DISABLED, 10, 268, 470, 24, hWnd, (HMENU)IDC_MAIN_STOPBTN, GetModuleHandle(NULL), NULL);
		//Create status bar
		HWND hWndStatusBar = CreateWindowExW(NULL, STATUSCLASSNAME, NULL, WS_VISIBLE | WS_CHILD, 0, 0, 0, 0, hWnd, (HMENU)IDC_STATUSBAR, GetModuleHandle(NULL), NULL);
		//Console
		CreateWindow(RICHEDIT_CLASS, L"Start the bot whenever!\r\n", WS_CHILD | ES_AUTOVSCROLL | ES_AUTOHSCROLL | WS_HSCROLL | WS_VISIBLE | WS_VSCROLL | ES_MULTILINE | ES_READONLY | WS_BORDER, 20, 497, 450, 125, hWnd, (HMENU)IDC_CONSOLE, GetModuleHandle(NULL), NULL);
	}
	break;
	case WM_COMMAND:
	{
		int wmId = LOWORD(wParam);
		int wmEvent = HIWORD(wParam);
		LPNMHDR	tc = (LPNMHDR)lParam;
		// Parse the menu selections:
		switch (wmId)
		{
		case IDC_MAIN_LIST:
			//Show desc
			if (wmEvent == LBN_SELCHANGE || wmEvent == LBN_DBLCLK) {
				std::string check1;
				std::string find("[L]");
				int SelIndex = SendMessage(GetDlgItem(hWnd, IDC_MAIN_LIST), LB_GETCURSEL, 0, 0L);
				SendMessageA(GetDlgItem(hWnd, IDC_MAIN_LIST), LB_GETTEXT, SelIndex, (LPARAM)check1.c_str());
				std::string check(check1.c_str());
				std::size_t found = check.find(find);
				if (found != std::string::npos) {
					//Local script
					OutputDebugStringA(check.c_str());
					SetDlgItemText(hWnd, IDC_MAIN_DESCBOX, L"Unavailable");
					if (!botDLL) {
						std::string loadTxt = "Load " + check;
						sprintf_s(selBot.name, sizeof(BotInfo::name), "%s", check.c_str());
						sprintf_s(selBot.id, sizeof(BotInfo::id), "%s", check.c_str());
						sprintf_s(selBot.dll, sizeof(BotInfo::dll), "%s", check.c_str());
						selBot.type = BOT_LUA;
						SendMessageA(GetDlgItem(hWnd, IDC_MAIN_MNBTN), WM_SETTEXT, 0, (LPARAM)loadTxt.c_str());
						if (runningBot.type == 0)
							EnableWindow(hWndLoadButton, true);
					}
				}
				else {
					struct string s;
					init_string(&s);
					//Load bots json
					CURL *curl = curl_easy_init();
					if (curl) {
						std::string jsonURL = Util::BotURL + "bots.json";
						curl_easy_setopt(curl, CURLOPT_URL, jsonURL.c_str());
						int code;
						code = curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, writefunc);
						if (code != CURLE_OK) {
							//fprintf(stderr, "Failed to set writer [%s]\n", errorBuffer);
							return -1;
						}
						curl_easy_setopt(curl, CURLOPT_WRITEDATA, &s);
						int res = curl_easy_perform(curl);
						curl_easy_cleanup(curl);
						cJSON * root = cJSON_Parse(s.ptr);
						cJSON * botsobj = cJSON_GetObjectItem(root, "bots");
						cJSON * botsArrayitem = cJSON_GetArrayItem(botsobj, SelIndex);
						char str[1024];
						sprintf(str, "%s", cJSON_GetObjectItem(botsArrayitem, "desc")->valuestring);
						//Fill selected bot stuct
						sprintf_s(selBot.name, sizeof(BotInfo::name), "%s", cJSON_GetObjectItem(botsArrayitem, "name")->valuestring);
						sprintf_s(selBot.url, sizeof(BotInfo::url), "%s", cJSON_GetObjectItem(botsArrayitem, "dll")->valuestring);
						selBot.type = cJSON_GetObjectItem(botsArrayitem, "type")->valueint;
						sprintf_s(selBot.id, sizeof(BotInfo::id), "%s", cJSON_GetObjectItem(botsArrayitem, "id")->valuestring);
						if (selBot.type == BOT_DLL)
							sprintf_s(selBot.dll, sizeof(BotInfo::dll), "%s", cJSON_GetObjectItem(botsArrayitem, "dll")->valuestring);
						else if (selBot.type == BOT_LUA) {
							char buffer[1024];
							sprintf_s(buffer, sizeof(buffer), "%s", cJSON_GetObjectItem(botsArrayitem, "dll")->valuestring);
							sprintf_s(selBot.dll, sizeof(BotInfo::dll), buffer, cJSON_GetObjectItem(botsArrayitem, "id")->valuestring);
						}
						SetDlgItemText(hWnd, IDC_MAIN_DESCBOX, GetWC(str));
						if (!botDLL) {
							char buffer[1024];
							sprintf(buffer, "Load %s", selBot.name);
							SendMessageA(GetDlgItem(hWnd, IDC_MAIN_MNBTN), WM_SETTEXT, 0, (LPARAM)buffer);
							if(runningBot.type == 0)
								EnableWindow(hWndLoadButton, true);
						}
						cJSON_Delete(root);
						SelIndex = SelIndex;
					}
				}
			}
			break;
		case IDC_MAIN_PLAYBTN:
			if (runningBot.type == BOT_DLL) {
				if (botDLL) {
					PausePtr pause = (PausePtr)GetProcAddress(botDLL, "pause");
					(*pause)(false);
					EnableWindow(hWndPlayButton, false);
					EnableWindow(hWndStopButton, true);
					EnableWindow(hWndLoadButton, false);
					EnableWindow(hWndPauseButton, true);
				}
			}
			else if (runningBot.type == BOT_LUA) {
				luaBot->Play();
				EnableWindow(hWndPlayButton, false);
				EnableWindow(hWndStopButton, true);
				EnableWindow(hWndLoadButton, false);
				EnableWindow(hWndPauseButton, true);
			}
			break;
		case IDC_MAIN_PAUSEBTN:
		{
			if (runningBot.type == BOT_DLL) {
				if (botDLL) {
					PausePtr pause = (PausePtr)GetProcAddress(botDLL, "pause");
					(*pause)(true);
					EnableWindow(hWndPlayButton, true);
					EnableWindow(hWndStopButton, true);
					EnableWindow(hWndLoadButton, false);
					EnableWindow(hWndPauseButton, false);
				}
			}
			else if (runningBot.type == BOT_LUA) {
				luaBot->Pause();
				EnableWindow(hWndPlayButton, true);
				EnableWindow(hWndStopButton, true);
				EnableWindow(hWndLoadButton, false);
				EnableWindow(hWndPauseButton, false);
			}
		}
		break;
		case IDC_MAIN_MNBTN:
			//Handle button click
			//Download DLL
			if (selBot.type == BOT_DLL) {
				if (downloadBotDLL(selBot.dll)) {
					//Load and Start DLL
					botDLL = LoadLibraryA(selBot.dll);
					if (botDLL) {
						InitWindowPtr iwp = (InitWindowPtr)GetProcAddress(botDLL, "initWindow");
						(*iwp)(hWnd, true);
						PausePtr pause = (PausePtr)GetProcAddress(botDLL, "pause");
						(*pause)(true);
						rb = (RunPtr)GetProcAddress(botDLL, "run");
						runThread = CreateThread(0, 0, runBot, NULL, 0, NULL);
						EnableWindow(hWndPlayButton, true);
						EnableWindow(hWndStopButton, true);
						EnableWindow(hWndLoadButton, false);
						EnableWindow(hWndPauseButton, false);
						runningBot = selBot;
					}
				}
			}
			else if (selBot.type == BOT_LUA) {
				//Download main script and run
				//char outfilename[FILENAME_MAX];
				std::string find("[L]");
				std::string check(selBot.dll);

				std::size_t found = check.find(find);

				std::string luascript = check;
				if (found == std::string::npos) {
					//Not local

					//std::string scriptDir = Util::getCurrentDirectoryS() + "\\scripts\\" + selBot.id + "\\";
					//CreateDirectoryA(scriptDir.c_str(), NULL);

					std::string zipFile = Util::getCurrentDirectoryS() + "\\scripts\\" + selBot.id + ".zip";

					if (!downloadBotLUA(zipFile.c_str()))
						return 0;
					
					//Extract zip and rename folder
					std::string pathDir = Util::getCurrentDirectoryS() + "\\scripts\\";
					std::string pass = "NULL";

					std::string botPath = pathDir + selBot.id + "\\\0\0";

					silently_remove_directory(botPath.c_str());

					std::vector<std::string> files = unzip(zipFile, pathDir, pass);

					std::string oldPath = pathDir + files.at(0);

					std::string newPath = pathDir + selBot.id;

					MoveFileA(oldPath.c_str(), newPath.c_str());

					DeleteFileA(zipFile.c_str());

					std::string dirPath;
					dirPath = Util::getCurrentDirectoryS() + "\\scripts\\" + selBot.id + "\\Captures";
					CreateDirectoryA(dirPath.c_str(), NULL);
					dirPath = Util::getCurrentDirectoryS() + "\\scripts\\" + selBot.id + "\\imgs";
					CreateDirectoryA(dirPath.c_str(), NULL);
				}
				else {
					std::string idFolder = std::string(selBot.id).substr(found + 3);
					std::string dirPath = Util::getCurrentDirectoryS() + "\\scripts\\local\\" + idFolder.c_str() + "\\Captures";
					CreateDirectoryA(dirPath.c_str(), NULL);
				}
				//sprintf_s(outfilename, sizeof(MAX_PATH), selBot.dll, Util::getCurrentDirectoryA());
				//std::ostringstream oss;
				//oss << "sometext" << somevar << "sometext" << somevar;
				EnableWindow(hWndLoadButton, false);

				luaBot = new LUABot(&luascript[0u], hWnd, selBot.id);
				runThread = CreateThread(0, 0, runLUABot, NULL, 0, NULL);
				EnableWindow(hWndPlayButton, !luaBot->GetPlaying());
				EnableWindow(hWndStopButton, true);
				EnableWindow(hWndPauseButton, luaBot->GetPlaying());
				SetTimer(hWnd,             // handle to main window 
					IDT_LUA_TIMER,            // timer identifier 
					1000,                // five-minute interval 
					(TIMERPROC)NULL);
				runningBot = selBot;
				//Create Console
				
			}
			break;
		case IDC_MAIN_STOPBTN:
		{
			if (runningBot.type == BOT_DLL) {
				if (botDLL) {
					DestroyWindowPtr dwp = (DestroyWindowPtr)GetProcAddress(botDLL, "destroyWindow");
					(*dwp)();
					SetRunPtr srp = (SetRunPtr)GetProcAddress(botDLL, "setRunning");
					(*srp)(false);
					EnableWindow(hWndPlayButton, false);
					EnableWindow(hWndStopButton, false);
					EnableWindow(hWndLoadButton, true);
					EnableWindow(hWndPauseButton, false);
					WaitForSingleObject(runThread, INFINITE);
				}
				FreeLibrary(botDLL);
				botDLL = NULL;
			}
			else if (runningBot.type == BOT_LUA) {
				SendMessage(hWnd, WM_COMMAND, MAKEWPARAM(IDC_CONSOLE, BN_CLICKED), NULL);
				//Sleep(2000);
				KillTimer(hWnd, IDT_LUA_TIMER);
				PostMessage(hWnd, BB_CLOSED, NULL, NULL);
			}
		}
		break;
		case IDM_ABOUT:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		default:
			if (runningBot.type == BOT_DLL) {
				if (botDLL) {
					WndProcPtr wpp = (WndProcPtr)GetProcAddress(botDLL, "WndProc");
					return (*wpp)(hWnd, message, wParam, lParam);
				}
			} else {
				if (runningBot.type == BOT_LUA && wmId >= 5000) {
					if (luaBot->HasUIElement(wmId)) {
						luaBot->QueueUIEvent(wmId);
						break;
					}
				}
				return DefWindowProc(hWnd, message, wParam, lParam);
			}
		}
	}
	break;
	case WM_TIMER:
	{
		switch (wParam)
		{
		case IDT_LUA_TIMER:
			// process the 10-second timer 
			luaBot->addSecond();
			return 0;
		}
	}
	break;
	case WM_PAINT:
	{
		PAINTSTRUCT ps;
		HDC hdc = BeginPaint(hWnd, &ps);

		EndPaint(hWnd, &ps);
	}
	break;
	case WM_DESTROY:
		if (botDLL) {
			DestroyWindowPtr dwp = (DestroyWindowPtr)GetProcAddress(botDLL, "destroyWindow");
			(*dwp)();
			SetRunPtr srp = (SetRunPtr)GetProcAddress(botDLL, "setRunning");
			(*srp)(false);
			if(runThread)
				WaitForSingleObject(runThread, INFINITE);
		}
		PostQuitMessage(0);
		break;
	default:
		if (botDLL) {
			WndProcPtr wpp = (WndProcPtr)GetProcAddress(botDLL, "WndProc");
			return (*wpp)(hWnd, message, wParam, lParam);
		}
		else
			return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}