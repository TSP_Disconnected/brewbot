#pragma once

#include <Windows.h>
#include <CommCtrl.h>
#include "Bot.h"

#ifndef UTILITY_H_
#define UTILITY_H_

//GLOBAL VARIABLES
//Controls from core.dll
#define ID_STATUSBAR 157
//Main Control defines
#define ID_TABCTRL 201
#define ID_MAIN_TIMER 202
#define ID_DEBUGMENU_RELOAD 203
#define ID_DEBUGMENU_DISADBR 204
#define ID_DEBUGMENU_TESTCUP 205
#define ID_DEBUGMENU_TESTUPCASTLE 206
//Tab1 - GENERAL
#define ID_CONSOLE 210
#define ID_SEP1 211
#define ID_SCREEN_BTN 212
#define ID_CLEARED_STATS 213
#define ID_RUN_LBL 214
#define ID_DIAMOND_STATS 215
#define ID_AWIN_BTN 216
#define ID_NWIN_BTN 217
#define ID_CUP_STATS 218
#define ID_CLEARED2_STATS 219
//Tab2 - Config
#define ID_WAVE_CB 230
#define ID_RWAVE_CB 231
#define ID_DRAGONS_CB 232
#define ID_SKIPWAVE_CB 233
#define ID_WATCHADS_CB 234
#define ID_SPAMABIL_CB 235
#define ID_GDRAGON_CB 236
#define ID_BDRAGON_CB 237
#define ID_RDRAGON_CB 238
#define ID_SDRAGON_CB 239
#define ID_LDRAGON_CB 240
#define ID_HELL_CB 241
#define ID_RUNSEASON_CB 242
//Tab3 - Castle
#define ID_CASTLE_START 250
#define ID_CASTLE_1 250
#define ID_CASTLE_2 251
#define ID_CASTLE_3 252
#define ID_CASTLE_4 253
#define ID_SCANCASTLE_BTN 254

#define NUM_XBTN_IMGS 2

#define NUM_DRAGONS 5
enum {
	DRAGON_GREEN,
	DRAGON_BLACK,
	DRAGON_RED,
	DRAGON_LEGENDARY,
	DRAGON_SIN
};

//Castle Defintions
//TODO: Move elsewhere
#define NUM_CASTLE_BASE 4
#define NUM_CASTLE_PARTS 5
enum {
	BASE_FIRE,
	BASE_POISON,
	BASE_LIGHT,
	BASE_FROZEN
};
enum {
	CASTLE_CANNON,
	CASTLE_MINIGUN,
	CASTLE_POISON,
	CASTLE_LIGHT,
	CASTLE_BALLISTA,
	CASTLE_SHIELD,
	CASTLE_GOLD
};

enum {
	DIRECTION_UNDEFINED = -1,
	DIRECTION_UP,
	DIRECTION_DOWN,
	DIRECTION_LEFT,
	DIRECTION_RIGHT
};

struct IMAGELOC {
	int x;
	int y;
};

struct IMAGELOC3 {
	int x;
	int y;
	int xDirection;
	int yDirection;
};

struct IMAGELOC2 {
	int numFound;
	int x[5];
	int y[5];
};

struct string {
	char *ptr;
	size_t len;
};

class Util {
	static size_t writefunc(void * ptr, size_t size, size_t nmemb, string * s);
	static void init_string(string * s);

public:
	//Search image and click functions
	static int ClickImageWait(char* tolerance, char* imagePath, HWND hw, imagesearch funcptr, Bot* bot, ADB* adbCon, int w, int h);
	static int ClickImage(char* tolerance, char* imagePath, HWND hw, imagesearch funcptr, Bot* bot, ADB* adbCon, int w, int h);
	//Get working directories
	static TCHAR *getCurrentDirectoryW();
	static char *getCurrentDirectoryA();
	//Get XY of image location
	static IMAGELOC getXY(char *result);
	static IMAGELOC2 getDiamondXY(char * result);
	//Grab screenshot
	static void CaptureScreen(HWND window, const char* filename);
	static void AppendText(const HWND &hwnd, TCHAR *newText, int item);
	static void AppendTextA(const HWND & hwnd, char * newText, int item);
	static void setRichStyle(int richId, COLORREF color, HWND hWnd);
	static HWND getBluestacks();
	static HWND getMainWindow(Bot* bot);
	//BlueStacks2 Functions
	static bool startBluestacks(HWND hWnd);
	static bool killBluestacks2(HWND hWnd);
	static void removeDirectory(LPCTSTR dir);
	static IMAGELOC getCup(char * folder, imageSearchExM funcptr, imageSearchEx pImageSearchEx);
	//Update images
	static void updateImages(HWND hw);
	static void downloadFile(char * url, char * file);
	//Check hash
	static std::string getHash(char * filename);
	//Curl
	static size_t curlWriteFile(void *ptr, size_t size, size_t nmemb, FILE *stream);
};

#endif