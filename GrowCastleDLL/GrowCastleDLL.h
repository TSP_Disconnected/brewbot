// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the GROWCASTLEDLL_EXPORTS
// symbol defined on the command line. This symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// GROWCASTLEDLL_API functions as being imported from a DLL, whereas this DLL sees symbols
// defined with this macro as being exported.
#ifdef GROWCASTLEDLL_EXPORTS
#define GROWCASTLEDLL_API __declspec(dllexport)
#else
#define GROWCASTLEDLL_API __declspec(dllimport)
#endif
extern "C" {
	int __stdcall run(void);
	bool __stdcall setRunning(bool);
	void __stdcall pause(bool);
	bool __stdcall isPaused(void);
	bool  __stdcall isMyEvent(int);
	void __stdcall parseEvent(int, int);
	void __stdcall initWindow(HWND hWnd);
	void __stdcall destroyWindow(void);
	BOOL __stdcall OnNotify(HWND, HWND, LPARAM);
}