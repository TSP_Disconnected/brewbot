// GrowCastleDLL.cpp : Defines the exported functions for the DLL application.
//
//#pragma comment (lib,"Gdiplus.lib")

#include "stdafx.h"
#include "GrowCastleDLL.h"
#include "ADB.h"
#include "Utility.h"
#include "Bot.h"

#include <windows.h>
#include <stdlib.h>
#include <Richedit.h>
//#include <gdiplus.h>
//#include <objidl.h>
#include <fstream>
#include <vector>
#include <regex>
#include <time.h>
#include <tlhelp32.h>

#pragma comment( lib, "comctl32.lib" )
#pragma comment(lib, "Psapi")

//Tabs
#define TAB_GENERAL 0
#define TAB_CONFIG 1
#define TAB_CASTLE 2
#define TAB_ABOUT 3

//Bot event defines
#define EVENT_NONE 0
#define EVENT_REPLAY 1
#define EVENT_WAVE 2
#define EVENT_DRAGON 3
#define EVENT_HELL 4
#define EVENT_SEASON 5

#define NUM_EVENTS 3

//Bot variables
int dragsCleared = 0;
int wavesCleared = 0;
int wavesReplayed = 0;
int earnedDiamonds = 0;
int lastDiamonds = -1;
int correctCups = 0;
int totalCups = 0;
int lostCount = 30;

//upgradeCastle int for castle tab
// change to bool, make able to upgrade multiple parts at once
int upgradeCastleInt = -1;

bool runDragons = false;
bool runWaves = false;
bool runRWaves = false;
bool skipWaves = false;
bool runHell = false;
bool runSeason = false;
bool watchAds = false;
bool spamAbilities = false;
bool isRunning = true;
bool isPausedB = false;
bool upgradeCastle = true;
bool strangeMonseterSpotted = false;
bool disableADBRestart = false;
DWORD monLastSpot = NULL;
DWORD monFirstSpotted = NULL;
int runningEvent = EVENT_NONE;
DWORD lastEventTime = 0;
DWORD lastBGEventTime = 0;

std::vector<int> activeEvents;

std::vector<int> activeDragons;

int width;
int height;
imagesearch pImageSearch;
imageSearchEx pImageSearchEx;
imageSearchExM pImageSearchExM;
HINSTANCE richDLL;
RECT origSize;

HWND g_HWND = NULL;
HMENU debugMenu = NULL;

Bot bot = NULL;

struct SavedSettings {
	bool replay;
	bool waves;
	bool dragons;
};

SavedSettings sSettings;

void ReplaceStringInPlace(std::string& subject, const std::string& search,
	const std::string& replace) {
	size_t pos = 0;
	while ((pos = subject.find(search, pos)) != std::string::npos) {
		subject.replace(pos, search.length(), replace);
		pos += replace.length();
	}
}

/***********
** Capture messages for bot controls
***********/
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_CTLCOLORSTATIC:
		if ((HWND)lParam == GetDlgItem(hWnd, ID_SPAMABIL_CB) || GetDlgItem(hWnd, ID_CLEARED_STATS) || GetDlgItem(hWnd, ID_DIAMOND_STATS) || GetDlgItem(hWnd, ID_MAIN_TIMER)
			|| GetDlgItem(hWnd, ID_DRAGONS_CB) || GetDlgItem(hWnd, ID_RWAVE_CB) || GetDlgItem(hWnd, ID_SKIPWAVE_CB) || GetDlgItem(hWnd, ID_TABCTRL) || GetDlgItem(hWnd, ID_CASTLE_1)
			|| GetDlgItem(hWnd, ID_CASTLE_2) || GetDlgItem(hWnd, ID_CASTLE_3) || GetDlgItem(hWnd, ID_CASTLE_4) || GetDlgItem(hWnd, TAB_CONFIG)) // lParam is the HWND for the control
			return (LONG)GetStockObject(WHITE_BRUSH);
	case WM_COMMAND:
	{
		int wmId = LOWORD(wParam);
		int wmEvent = HIWORD(wParam);
		BOOL checked;
		switch (wmId)
		{
		case ID_DEBUGMENU_TESTCUP:
		{
			IMAGELOC cup = Util::getCup("fail2", pImageSearchExM, pImageSearchEx);
			char buffer[100];
			snprintf(buffer, sizeof(buffer) - 1, "Cup Test Results: X - %d, Y - %d\r\n", cup.x, cup.y);
			Util::AppendTextA(Util::getMainWindow(&bot), buffer, ID_CONSOLE);
		}
		break;
		case ID_DEBUGMENU_TESTUPCASTLE:
		{
			bot.upgradeCastle(hWnd, width, height, pImageSearch, upgradeCastleInt);
		}
		break;
		case ID_DEBUGMENU_RELOAD: 
		{
			Util::killBluestacks2(Util::getMainWindow(&bot));
			//Start bluestacks here
			Util::startBluestacks(Util::getMainWindow(&bot));
		}
		break;
		case ID_DEBUGMENU_DISADBR:
		{
			disableADBRestart = !disableADBRestart;
		}
		break;
		case ID_SCREEN_BTN:
		{
			char path[MAX_PATH];
			sprintf(path, "%s\\AntiBot\\snap.bmp", Util::getCurrentDirectoryA());
			Util::CaptureScreen(Util::getBluestacks(), path);
			//bot.killApp("com.raongames.growcastle");
			bot.getApp();
		}
		break;
		case ID_SCANCASTLE_BTN:
		{
			upgradeCastleInt = -1;
			bot.scanCastle(Util::getBluestacks(), width, height, pImageSearch);

			// Add to function
			DestroyWindow(GetDlgItem(hWnd, ID_CASTLE_1));
			DestroyWindow(GetDlgItem(hWnd, ID_CASTLE_2));
			DestroyWindow(GetDlgItem(hWnd, ID_CASTLE_3));
			DestroyWindow(GetDlgItem(hWnd, ID_CASTLE_4));
			int basePart = bot.getCastlePartAtIndex(0);
			if (basePart < 0) {
				Util::AppendText(Util::getMainWindow(&bot), L"Scan failed!\r\n", ID_CONSOLE);
				break;
			}
			switch (basePart) {
			case BASE_FIRE:
				CreateWindowExW(NULL, L"BUTTON", L"Upgrade Fire Base", WS_VISIBLE | WS_CHILD | BS_CHECKBOX, 20, 407, 185, 35, hWnd, (HMENU)ID_CASTLE_4, GetModuleHandle(NULL), NULL);
				break;
			case BASE_FROZEN:
				CreateWindowExW(NULL, L"BUTTON", L"Upgrade Frozen Base", WS_VISIBLE | WS_CHILD | BS_CHECKBOX, 20, 407, 185, 35, hWnd, (HMENU)ID_CASTLE_4, GetModuleHandle(NULL), NULL);
				break;
			case BASE_LIGHT:
				CreateWindowExW(NULL, L"BUTTON", L"Upgrade Lightning Base", WS_VISIBLE | WS_CHILD | BS_CHECKBOX, 20, 407, 185, 35, hWnd, (HMENU)ID_CASTLE_4, GetModuleHandle(NULL), NULL);
				break;
			case BASE_POISON:
				CreateWindowExW(NULL, L"BUTTON", L"Upgrade Poison Base", WS_VISIBLE | WS_CHILD | BS_CHECKBOX, 20, 407, 185, 35, hWnd, (HMENU)ID_CASTLE_4, GetModuleHandle(NULL), NULL);
				break;
			}
			for (int i = 1; i < 4; i++) {
				int castlePart = bot.getCastlePartAtIndex(i);
				int y = 332 + (25 * (i - 1));
				switch (castlePart) {
				case CASTLE_BALLISTA:
					CreateWindowExW(NULL, L"BUTTON", L"Upgrade Ballista", WS_VISIBLE | WS_CHILD | BS_CHECKBOX, 20, y, 185, 35, hWnd, (HMENU)(ID_CASTLE_START + (i - 1)), GetModuleHandle(NULL), NULL);
					break;
				case CASTLE_CANNON:
					CreateWindowExW(NULL, L"BUTTON", L"Upgrade Cannon", WS_VISIBLE | WS_CHILD | BS_CHECKBOX, 20, y, 185, 35, hWnd, (HMENU)(ID_CASTLE_START + (i - 1)), GetModuleHandle(NULL), NULL);
					break;
				case CASTLE_LIGHT:
					CreateWindowExW(NULL, L"BUTTON", L"Upgrade Lightning", WS_VISIBLE | WS_CHILD | BS_CHECKBOX, 20, y, 185, 35, hWnd, (HMENU)(ID_CASTLE_START + (i - 1)), GetModuleHandle(NULL), NULL);
					break;
				case CASTLE_MINIGUN:
					CreateWindowExW(NULL, L"BUTTON", L"Upgrade Minigun", WS_VISIBLE | WS_CHILD | BS_CHECKBOX, 20, y, 185, 35, hWnd, (HMENU)(ID_CASTLE_START + (i - 1)), GetModuleHandle(NULL), NULL);
					break;
				case CASTLE_POISON:
					CreateWindowExW(NULL, L"BUTTON", L"Upgrade Poison", WS_VISIBLE | WS_CHILD | BS_CHECKBOX, 20, y, 185, 35, hWnd, (HMENU)(ID_CASTLE_START + (i - 1)), GetModuleHandle(NULL), NULL);
					break;
				}
			}
		}
		break;
		case ID_AWIN_BTN:
		{
			RECT bsSize;
			GetWindowRect(Util::getBluestacks(), &bsSize);
			origSize = bsSize;
			LONG winL = GetWindowLong(Util::getBluestacks(), GWL_STYLE);
			//origBSL = winL;
			SetWindowLong(Util::getBluestacks(), GWL_STYLE, winL | WS_CAPTION | WS_SYSMENU);
			int capSize = GetSystemMetrics(SM_CYCAPTION);
			//SetWindowPos(Util::getBluestacks(), NULL, bsSize.left, bsSize.top, bsSize.right, bsSize.bottom, SWP_FRAMECHANGED);
		}
		break;
		case ID_NWIN_BTN:
		{
			LONG winL = GetWindowLong(Util::getBluestacks(), GWL_STYLE);
			if (winL != NULL) {
				winL &= ~(WS_CAPTION | WS_SYSMENU);
				SetWindowLong(Util::getBluestacks(), GWL_STYLE, winL);
				//SetWindowPos(Util::getBluestacks(), NULL, origSize.left, origSize.top, origSize.right, origSize.bottom, SWP_FRAMECHANGED);
				SendMessage(Util::getBluestacks(), WM_SETREDRAW, TRUE, NULL);
			}
		}
		break;
		case ID_SPAMABIL_CB:
			checked = IsDlgButtonChecked(hWnd, ID_SPAMABIL_CB);
			if (checked) {
				spamAbilities = false;
				CheckDlgButton(hWnd, ID_SPAMABIL_CB, BST_UNCHECKED);
			}
			else {
				spamAbilities = true;
				CheckDlgButton(hWnd, ID_SPAMABIL_CB, BST_CHECKED);
			}
			break;
		case ID_RUNSEASON_CB:
			checked = IsDlgButtonChecked(hWnd, ID_RUNSEASON_CB);
			if (checked) {
				for (int i = 0; i < activeEvents.size(); i++) {
					if (activeEvents.at(i) == EVENT_SEASON) {
						activeEvents.erase(activeEvents.begin() + i);
						break;
					}
				}
				runSeason = false;
				CheckDlgButton(hWnd, ID_RUNSEASON_CB, BST_UNCHECKED);
			}
			else {
				runSeason = true;
				activeEvents.push_back(EVENT_SEASON);
				CheckDlgButton(hWnd, ID_RUNSEASON_CB, BST_CHECKED);
			}
			break;
		case ID_HELL_CB:
			checked = IsDlgButtonChecked(hWnd, ID_HELL_CB);
			if (checked) {
				for (int i = 0; i < activeEvents.size(); i++) {
					if (activeEvents.at(i) == EVENT_HELL) {
						activeEvents.erase(activeEvents.begin() + i);
						break;
					}
				}
				runHell = false;
				CheckDlgButton(hWnd, ID_HELL_CB, BST_UNCHECKED);
			}
			else {
				runHell = true;
				activeEvents.push_back(EVENT_HELL);
				CheckDlgButton(hWnd, ID_HELL_CB, BST_CHECKED);
			}
			break;
		case ID_DRAGONS_CB:
			checked = IsDlgButtonChecked(hWnd, ID_DRAGONS_CB);
			if (checked) {
				for (int i = 0; i < activeEvents.size(); i++) {
					if (activeEvents.at(i) == EVENT_DRAGON) {
						activeEvents.erase(activeEvents.begin() + i);
						break;
					}
				}
				runDragons = false;
				CheckDlgButton(hWnd, ID_DRAGONS_CB, BST_UNCHECKED);
			}
			else {
				runDragons = true;
				activeEvents.push_back(EVENT_DRAGON);
				CheckDlgButton(hWnd, ID_DRAGONS_CB, BST_CHECKED);
			}
			break;
		case ID_GDRAGON_CB:
			checked = IsDlgButtonChecked(hWnd, ID_GDRAGON_CB);
			if (checked) {
				for (int i = 0; i < activeDragons.size(); i++) {
					if (activeDragons.at(i) == DRAGON_GREEN) {
						activeDragons.erase(activeDragons.begin() + i);
						break;
					}
				}
				CheckDlgButton(hWnd, ID_GDRAGON_CB, BST_UNCHECKED);
			}
			else {
				activeDragons.push_back(DRAGON_GREEN);
				CheckDlgButton(hWnd, ID_GDRAGON_CB, BST_CHECKED);
			}
			break;
		case ID_BDRAGON_CB:
			checked = IsDlgButtonChecked(hWnd, ID_BDRAGON_CB);
			if (checked) {
				for (int i = 0; i < activeDragons.size(); i++) {
					if (activeDragons.at(i) == DRAGON_BLACK) {
						activeDragons.erase(activeDragons.begin() + i);
						break;
					}
				}
				CheckDlgButton(hWnd, ID_BDRAGON_CB, BST_UNCHECKED);
			}
			else {
				activeDragons.push_back(DRAGON_BLACK);
				CheckDlgButton(hWnd, ID_BDRAGON_CB, BST_CHECKED);
			}
			break;
		case ID_RDRAGON_CB:
			checked = IsDlgButtonChecked(hWnd, ID_RDRAGON_CB);
			if (checked) {
				for (int i = 0; i < activeDragons.size(); i++) {
					if (activeDragons.at(i) == DRAGON_RED) {
						activeDragons.erase(activeDragons.begin() + i);
						break;
					}
				}
				CheckDlgButton(hWnd, ID_RDRAGON_CB, BST_UNCHECKED);
			}
			else {
				activeDragons.push_back(DRAGON_RED);
				CheckDlgButton(hWnd, ID_RDRAGON_CB, BST_CHECKED);
			}
			break;
		case ID_LDRAGON_CB:
			checked = IsDlgButtonChecked(hWnd, ID_LDRAGON_CB);
			if (checked) {
				for (int i = 0; i < activeDragons.size(); i++) {
					if (activeDragons.at(i) == DRAGON_LEGENDARY) {
						activeDragons.erase(activeDragons.begin() + i);
						break;
					}
				}
				CheckDlgButton(hWnd, ID_LDRAGON_CB, BST_UNCHECKED);
			}
			else {
				activeDragons.push_back(DRAGON_LEGENDARY);
				CheckDlgButton(hWnd, ID_LDRAGON_CB, BST_CHECKED);
			}
			break;
		case ID_SDRAGON_CB:
			checked = IsDlgButtonChecked(hWnd, ID_SDRAGON_CB);
			if (checked) {
				for (int i = 0; i < activeDragons.size(); i++) {
					if (activeDragons.at(i) == DRAGON_SIN) {
						activeDragons.erase(activeDragons.begin() + i);
						break;
					}
				}
				CheckDlgButton(hWnd, ID_SDRAGON_CB, BST_UNCHECKED);
			}
			else {
				activeDragons.push_back(DRAGON_SIN);
				CheckDlgButton(hWnd, ID_SDRAGON_CB, BST_CHECKED);
			}
			break;
		case ID_WAVE_CB:
			checked = IsDlgButtonChecked(hWnd, ID_WAVE_CB);
			if (checked) {
				for (int i = 0; i < activeEvents.size(); i++) {
					if (activeEvents.at(i) == EVENT_WAVE) {
						activeEvents.erase(activeEvents.begin() + i);
						break;
					}
				}
				runWaves = false;
				CheckDlgButton(hWnd, ID_WAVE_CB, BST_UNCHECKED);
			}
			else {
				runWaves = true;
				activeEvents.push_back(EVENT_WAVE);
				CheckDlgButton(hWnd, ID_WAVE_CB, BST_CHECKED);
			}
			break;
		case ID_RWAVE_CB:
			checked = IsDlgButtonChecked(hWnd, ID_RWAVE_CB);
			if (checked) {
				for (int i = 0; i < activeEvents.size(); i++) {
					if (activeEvents.at(i) == EVENT_REPLAY) {
						activeEvents.erase(activeEvents.begin() + i);
						break;
					}
				}
				runRWaves = false;
				CheckDlgButton(hWnd, ID_RWAVE_CB, BST_UNCHECKED);
			}
			else {
				runRWaves = true;
				activeEvents.push_back(EVENT_REPLAY);
				CheckDlgButton(hWnd, ID_RWAVE_CB, BST_CHECKED);
			}
			break;
		case ID_SKIPWAVE_CB:
			checked = IsDlgButtonChecked(hWnd, ID_SKIPWAVE_CB);
			if (checked) {
				skipWaves = false;
				CheckDlgButton(hWnd, ID_SKIPWAVE_CB, BST_UNCHECKED);
			}
			else {
				skipWaves = true;
				CheckDlgButton(hWnd, ID_SKIPWAVE_CB, BST_CHECKED);
			}
			break;
		case ID_WATCHADS_CB:
			checked = IsDlgButtonChecked(hWnd, ID_WATCHADS_CB);
			if (checked) {
				watchAds = false;
				CheckDlgButton(hWnd, ID_WATCHADS_CB, BST_UNCHECKED);
			}
			else {
				watchAds = true;
				CheckDlgButton(hWnd, ID_WATCHADS_CB, BST_CHECKED);
			}
			break;
		case ID_CASTLE_1:
			checked = IsDlgButtonChecked(hWnd, ID_CASTLE_1);
			if (!checked) {
				upgradeCastleInt = ID_CASTLE_1 - ID_CASTLE_START + 1;
				CheckDlgButton(hWnd, ID_CASTLE_1, BST_CHECKED);
				CheckDlgButton(hWnd, ID_CASTLE_2, BST_UNCHECKED);
				CheckDlgButton(hWnd, ID_CASTLE_3, BST_UNCHECKED);
				CheckDlgButton(hWnd, ID_CASTLE_4, BST_UNCHECKED);
			}
			else {
				upgradeCastleInt = -1;
				CheckDlgButton(hWnd, ID_CASTLE_1, BST_UNCHECKED);
				CheckDlgButton(hWnd, ID_CASTLE_2, BST_UNCHECKED);
				CheckDlgButton(hWnd, ID_CASTLE_3, BST_UNCHECKED);
				CheckDlgButton(hWnd, ID_CASTLE_4, BST_UNCHECKED);
			}
			break;
		case ID_CASTLE_2:
			checked = IsDlgButtonChecked(hWnd, ID_CASTLE_2);
			if (!checked) {
				upgradeCastleInt = ID_CASTLE_2 - ID_CASTLE_START + 1;
				CheckDlgButton(hWnd, ID_CASTLE_1, BST_UNCHECKED);
				CheckDlgButton(hWnd, ID_CASTLE_2, BST_CHECKED);
				CheckDlgButton(hWnd, ID_CASTLE_3, BST_UNCHECKED);
				CheckDlgButton(hWnd, ID_CASTLE_4, BST_UNCHECKED);
			}
			else {
				upgradeCastleInt = -1;
				CheckDlgButton(hWnd, ID_CASTLE_1, BST_UNCHECKED);
				CheckDlgButton(hWnd, ID_CASTLE_2, BST_UNCHECKED);
				CheckDlgButton(hWnd, ID_CASTLE_3, BST_UNCHECKED);
				CheckDlgButton(hWnd, ID_CASTLE_4, BST_UNCHECKED);
			}
			break;
		case ID_CASTLE_3:
			checked = IsDlgButtonChecked(hWnd, ID_CASTLE_3);
			if (!checked) {
				upgradeCastleInt = ID_CASTLE_3 - ID_CASTLE_START + 1;
				CheckDlgButton(hWnd, ID_CASTLE_1, BST_UNCHECKED);
				CheckDlgButton(hWnd, ID_CASTLE_2, BST_UNCHECKED);
				CheckDlgButton(hWnd, ID_CASTLE_3, BST_CHECKED);
				CheckDlgButton(hWnd, ID_CASTLE_4, BST_UNCHECKED);
			}
			else {
				upgradeCastleInt = -1;
				CheckDlgButton(hWnd, ID_CASTLE_1, BST_UNCHECKED);
				CheckDlgButton(hWnd, ID_CASTLE_2, BST_UNCHECKED);
				CheckDlgButton(hWnd, ID_CASTLE_3, BST_UNCHECKED);
				CheckDlgButton(hWnd, ID_CASTLE_4, BST_UNCHECKED);
			}
			break;
		case ID_CASTLE_4:
			checked = IsDlgButtonChecked(hWnd, ID_CASTLE_4);
			if (!checked) {
				upgradeCastleInt = 0;
				CheckDlgButton(hWnd, ID_CASTLE_1, BST_UNCHECKED);
				CheckDlgButton(hWnd, ID_CASTLE_2, BST_UNCHECKED);
				CheckDlgButton(hWnd, ID_CASTLE_3, BST_UNCHECKED);
				CheckDlgButton(hWnd, ID_CASTLE_4, BST_CHECKED);
			}
			else {
				upgradeCastleInt = -1;
				CheckDlgButton(hWnd, ID_CASTLE_1, BST_UNCHECKED);
				CheckDlgButton(hWnd, ID_CASTLE_2, BST_UNCHECKED);
				CheckDlgButton(hWnd, ID_CASTLE_3, BST_UNCHECKED);
				CheckDlgButton(hWnd, ID_CASTLE_4, BST_UNCHECKED);
			}
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
	}
	break;
	case WM_NOTIFY:
	{
		LPNMHDR	tc = (LPNMHDR)lParam;
		if (GetDlgItem(hWnd, ID_TABCTRL) && tc != NULL) {
			if ((tc->code == TCN_SELCHANGE) && (tc->hwndFrom == GetDlgItem(hWnd, ID_TABCTRL))) {
				//Load new controls for new tab
				// Then restore values
				int tabIndex = TabCtrl_GetCurSel(tc->hwndFrom);
				switch (tabIndex) {
				case TAB_GENERAL:
					CreateWindow(L"STATIC", L"Waiting...", WS_CHILD | WS_VISIBLE, 20, 401, 450, 20, hWnd, (HMENU)ID_CUP_STATS, GetModuleHandle(NULL), NULL);
					CreateWindow(L"STATIC", L"Loading...", WS_CHILD | WS_VISIBLE, 20, 332, 450, 20, hWnd, (HMENU)ID_CLEARED_STATS, GetModuleHandle(NULL), NULL);
					CreateWindow(L"STATIC", L"Waiting...", WS_CHILD | WS_VISIBLE, 20, 355, 450, 20, hWnd, (HMENU)ID_CLEARED2_STATS, GetModuleHandle(NULL), NULL);
					CreateWindow(L"STATIC", L"Waiting...", WS_CHILD | WS_VISIBLE, 20, 378, 450, 20, hWnd, (HMENU)ID_DIAMOND_STATS, GetModuleHandle(NULL), NULL);
					TCHAR buf[400];
					swprintf(buf, sizeof(buf), L"Waves Cleared: %d - Waves Replayed: %d", wavesCleared, wavesReplayed);
					SetDlgItemText(Util::getMainWindow(&bot), ID_CLEARED_STATS, buf);
					swprintf(buf, sizeof(buf), L"Dragons Cleared: %d", dragsCleared);
					SetDlgItemText(Util::getMainWindow(&bot), ID_CLEARED2_STATS, buf);
					swprintf(buf, sizeof(buf), L"Correct Cups: %d - Total Cups: %d", correctCups, totalCups);
					SetDlgItemText(Util::getMainWindow(&bot), ID_CUP_STATS, buf);
					CreateWindowExW(NULL, L"BUTTON", L"Move Bluestacks", WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON, 20, 436, 220, 24, hWnd, (HMENU)ID_AWIN_BTN, GetModuleHandle(NULL), NULL);
					CreateWindowExW(NULL, L"BUTTON", L"Remove Border", WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON, 250, 436, 220, 24, hWnd, (HMENU)ID_NWIN_BTN, GetModuleHandle(NULL), NULL);
					CreateWindowExW(NULL, L"BUTTON", L"Take Screenshot", WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON, 20, 465, 450, 24, hWnd, (HMENU)ID_SCREEN_BTN, GetModuleHandle(NULL), NULL);
					break;
				case TAB_CONFIG:
					CreateWindowExW(NULL, L"BUTTON", L"Run Dragons", WS_VISIBLE | WS_CHILD | BS_CHECKBOX, 20, 332, 185, 35, hWnd, (HMENU)ID_DRAGONS_CB, GetModuleHandle(NULL), NULL);
					CreateWindowExW(NULL, L"BUTTON", L"G", WS_VISIBLE | WS_CHILD | BS_CHECKBOX, 30, 357, 30, 35, hWnd, (HMENU)ID_GDRAGON_CB, GetModuleHandle(NULL), NULL);
					CreateWindowExW(NULL, L"BUTTON", L"B", WS_VISIBLE | WS_CHILD | BS_CHECKBOX, 60, 357, 30, 35, hWnd, (HMENU)ID_BDRAGON_CB, GetModuleHandle(NULL), NULL);
					CreateWindowExW(NULL, L"BUTTON", L"R", WS_VISIBLE | WS_CHILD | BS_CHECKBOX, 90, 357, 30, 35, hWnd, (HMENU)ID_RDRAGON_CB, GetModuleHandle(NULL), NULL);
					CreateWindowExW(NULL, L"BUTTON", L"S", WS_VISIBLE | WS_CHILD | BS_CHECKBOX, 120, 357, 30, 35, hWnd, (HMENU)ID_SDRAGON_CB, GetModuleHandle(NULL), NULL);
					CreateWindowExW(NULL, L"BUTTON", L"L", WS_VISIBLE | WS_CHILD | BS_CHECKBOX, 150, 357, 30, 35, hWnd, (HMENU)ID_LDRAGON_CB, GetModuleHandle(NULL), NULL);
					CreateWindowExW(NULL, L"BUTTON", L"Run Waves", WS_VISIBLE | WS_CHILD | BS_CHECKBOX, 20, 382, 185, 35, hWnd, (HMENU)ID_WAVE_CB, GetModuleHandle(NULL), NULL);
					CreateWindowExW(NULL, L"BUTTON", L"Skip Waves", WS_VISIBLE | WS_CHILD | BS_CHECKBOX, 30, 407, 175, 35, hWnd, (HMENU)ID_SKIPWAVE_CB, GetModuleHandle(NULL), NULL);
					CreateWindowExW(NULL, L"BUTTON", L"Replay Waves", WS_VISIBLE | WS_CHILD | BS_CHECKBOX, 20, 432, 185, 35, hWnd, (HMENU)ID_RWAVE_CB, GetModuleHandle(NULL), NULL);
					CreateWindowExW(NULL, L"BUTTON", L"Run Season", WS_VISIBLE | WS_CHILD | BS_CHECKBOX, 20, 457, 185, 35, hWnd, (HMENU)ID_RUNSEASON_CB, GetModuleHandle(NULL), NULL);
					
					CreateWindowExW(NULL, L"BUTTON", L"Run Hell", WS_VISIBLE | WS_CHILD | BS_CHECKBOX, 205, 332, 185, 35, hWnd, (HMENU)ID_HELL_CB, GetModuleHandle(NULL), NULL);
					//CreateWindowExW(NULL, L"BUTTON", L"Spam Abilites", WS_VISIBLE | WS_CHILD | BS_CHECKBOX, 205, 357, 185, 35, hWnd, (HMENU)ID_SPAMABIL_CB, GetModuleHandle(NULL), NULL);
					CreateWindowExW(NULL, L"BUTTON", L"Spam Abilites", WS_VISIBLE | WS_CHILD | BS_CHECKBOX, 205, 382, 185, 35, hWnd, (HMENU)ID_SPAMABIL_CB, GetModuleHandle(NULL), NULL);
					CreateWindowExW(NULL, L"BUTTON", L"Watch Ads", WS_VISIBLE | WS_CHILD | BS_CHECKBOX, 205, 407, 185, 35, hWnd, (HMENU)ID_WATCHADS_CB, GetModuleHandle(NULL), NULL);
					
					if (activeDragons.size() > 0) {
						for (int i = 0; i < activeDragons.size(); i++) {
							switch (activeDragons.at(i)) {
							case DRAGON_GREEN:
							{
								CheckDlgButton(hWnd, ID_GDRAGON_CB, true);
							}
							break;
							case DRAGON_BLACK:
							{
								CheckDlgButton(hWnd, ID_BDRAGON_CB, true);
							}
							break;
							case DRAGON_RED:
							{
								CheckDlgButton(hWnd, ID_RDRAGON_CB, true);
							}
							break;
							case DRAGON_LEGENDARY:
							{
								CheckDlgButton(hWnd, ID_LDRAGON_CB, true);
							}
							break;
							case DRAGON_SIN:
							{
								CheckDlgButton(hWnd, ID_SDRAGON_CB, true);
							}
							break;
							}
						}
					}
					
					CheckDlgButton(hWnd, ID_SPAMABIL_CB, spamAbilities);
					CheckDlgButton(hWnd, ID_DRAGONS_CB, runDragons);
					CheckDlgButton(hWnd, ID_WAVE_CB, runWaves);
					CheckDlgButton(hWnd, ID_RWAVE_CB, runRWaves);
					CheckDlgButton(hWnd, ID_WATCHADS_CB, watchAds);
					CheckDlgButton(hWnd, ID_SKIPWAVE_CB, skipWaves);
					CheckDlgButton(hWnd, ID_HELL_CB, runHell);
					CheckDlgButton(hWnd, ID_RUNSEASON_CB, runSeason);
					break;
				case TAB_CASTLE:
				{
					CreateWindowExW(NULL, L"BUTTON", L"Scan Castle Parts", WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON, 20, 465, 450, 24, hWnd, (HMENU)ID_SCANCASTLE_BTN, GetModuleHandle(NULL), NULL);
					int basePart = bot.getCastlePartAtIndex(0);
					if (basePart < 0) {
						Util::AppendText(Util::getMainWindow(&bot), L"Please scan castle!\r\n", ID_CONSOLE);
						break;
					}
					switch (basePart) {
					case BASE_FIRE:
						CreateWindowExW(NULL, L"BUTTON", L"Upgrade Fire Base", WS_VISIBLE | WS_CHILD | BS_CHECKBOX, 20, 407, 185, 35, hWnd, (HMENU)ID_CASTLE_4, GetModuleHandle(NULL), NULL);
						break;
					case BASE_FROZEN:
						CreateWindowExW(NULL, L"BUTTON", L"Upgrade Frozen Base", WS_VISIBLE | WS_CHILD | BS_CHECKBOX, 20, 407, 185, 35, hWnd, (HMENU)ID_CASTLE_4, GetModuleHandle(NULL), NULL);
						break;
					case BASE_LIGHT:
						CreateWindowExW(NULL, L"BUTTON", L"Upgrade Lightning Base", WS_VISIBLE | WS_CHILD | BS_CHECKBOX, 20, 407, 185, 35, hWnd, (HMENU)ID_CASTLE_4, GetModuleHandle(NULL), NULL);
						break;
					case BASE_POISON:
						CreateWindowExW(NULL, L"BUTTON", L"Upgrade Poison Base", WS_VISIBLE | WS_CHILD | BS_CHECKBOX, 20, 407, 185, 35, hWnd, (HMENU)ID_CASTLE_4, GetModuleHandle(NULL), NULL);
						break;
					}
					for (int i = 1; i < 4; i++) {
						int castlePart = bot.getCastlePartAtIndex(i);
						int y = 332 + (25 * (i - 1));
						switch (castlePart) {
						case CASTLE_BALLISTA:
							CreateWindowExW(NULL, L"BUTTON", L"Upgrade Ballista", WS_VISIBLE | WS_CHILD | BS_CHECKBOX, 20, y, 185, 35, hWnd, (HMENU)(ID_CASTLE_START + (i - 1)), GetModuleHandle(NULL), NULL);
							break;
						case CASTLE_CANNON:
							CreateWindowExW(NULL, L"BUTTON", L"Upgrade Cannon", WS_VISIBLE | WS_CHILD | BS_CHECKBOX, 20, y, 185, 35, hWnd, (HMENU)(ID_CASTLE_START + (i - 1)), GetModuleHandle(NULL), NULL);
							break;
						case CASTLE_LIGHT:
							CreateWindowExW(NULL, L"BUTTON", L"Upgrade Lightning", WS_VISIBLE | WS_CHILD | BS_CHECKBOX, 20, y, 185, 35, hWnd, (HMENU)(ID_CASTLE_START + (i - 1)), GetModuleHandle(NULL), NULL);
							break;
						case CASTLE_MINIGUN:
							CreateWindowExW(NULL, L"BUTTON", L"Upgrade Minigun", WS_VISIBLE | WS_CHILD | BS_CHECKBOX, 20, y, 185, 35, hWnd, (HMENU)(ID_CASTLE_START + (i - 1)), GetModuleHandle(NULL), NULL);
							break;
						case CASTLE_POISON:
							CreateWindowExW(NULL, L"BUTTON", L"Upgrade Poison", WS_VISIBLE | WS_CHILD | BS_CHECKBOX, 20, y, 185, 35, hWnd, (HMENU)(ID_CASTLE_START + (i - 1)), GetModuleHandle(NULL), NULL);
							break;
						}
					}
					if (upgradeCastleInt == 0) {
						CheckDlgButton(hWnd, ID_CASTLE_4, BST_CHECKED);
					}
					else if (upgradeCastleInt != -1) {
						CheckDlgButton(hWnd, upgradeCastleInt + 240 - 1, BST_CHECKED);
					}
				}
				break;
				case TAB_ABOUT:

					break;
				}
				return 0;
			}
			else if ((tc->code == TCN_SELCHANGING) && (tc->hwndFrom == GetDlgItem(hWnd, ID_TABCTRL))) {
				//Save values of controls in tab
				// Then delete controls
				int tabIndex = TabCtrl_GetCurSel(tc->hwndFrom);
				switch (tabIndex) {
				case TAB_GENERAL:
					DestroyWindow(GetDlgItem(hWnd, ID_SCREEN_BTN));
					DestroyWindow(GetDlgItem(hWnd, ID_CLEARED_STATS));
					DestroyWindow(GetDlgItem(hWnd, ID_CLEARED2_STATS));
					DestroyWindow(GetDlgItem(hWnd, ID_DIAMOND_STATS));
					DestroyWindow(GetDlgItem(hWnd, ID_CUP_STATS));
					DestroyWindow(GetDlgItem(hWnd, ID_AWIN_BTN));
					DestroyWindow(GetDlgItem(hWnd, ID_NWIN_BTN));
					break;
				case TAB_CONFIG:
					DestroyWindow(GetDlgItem(hWnd, ID_DRAGONS_CB));
					DestroyWindow(GetDlgItem(hWnd, ID_GDRAGON_CB));
					DestroyWindow(GetDlgItem(hWnd, ID_BDRAGON_CB));
					DestroyWindow(GetDlgItem(hWnd, ID_RDRAGON_CB));
					DestroyWindow(GetDlgItem(hWnd, ID_SDRAGON_CB));
					DestroyWindow(GetDlgItem(hWnd, ID_LDRAGON_CB));
					DestroyWindow(GetDlgItem(hWnd, ID_WAVE_CB));
					DestroyWindow(GetDlgItem(hWnd, ID_RWAVE_CB));
					DestroyWindow(GetDlgItem(hWnd, ID_SKIPWAVE_CB));
					DestroyWindow(GetDlgItem(hWnd, ID_WATCHADS_CB));
					DestroyWindow(GetDlgItem(hWnd, ID_SPAMABIL_CB));
					DestroyWindow(GetDlgItem(hWnd, ID_HELL_CB));
					DestroyWindow(GetDlgItem(hWnd, ID_RUNSEASON_CB));
					break;
				case TAB_CASTLE:
					DestroyWindow(GetDlgItem(hWnd, ID_CASTLE_1));
					DestroyWindow(GetDlgItem(hWnd, ID_CASTLE_2));
					DestroyWindow(GetDlgItem(hWnd, ID_CASTLE_3));
					DestroyWindow(GetDlgItem(hWnd, ID_CASTLE_4));
					DestroyWindow(GetDlgItem(hWnd, ID_SCANCASTLE_BTN));
					break;
				case TAB_ABOUT:
					break;
				}
				return 0;
			}
		}
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}
/***********
** Set whether the bot is actually going to run, if set false the bot will
** quit and exit
***********/
bool __stdcall setRunning(bool r)
{
	isRunning = r;
	return isRunning;
}
/***********
** Set whether the bot should be paused
***********/
void __stdcall pause(bool r)
{
	isPausedB = r;
	bot.setPaused(r);
}

/***********
** Get whether the bot is paused or not
***********/
bool __stdcall isPaused(void)
{
	return isPausedB;
}

/***********
** Initializes GUI of bot
***********/
void __stdcall initWindow(HWND hWnd, bool showWhatsNew) {
	//Save main window HWND
	//setMainWindow(hWnd);
	bot = Bot(hWnd);

	richDLL = LoadLibraryA("riched20.dll");

	INITCOMMONCONTROLSEX icex;

	TCITEM tie;

	// Initialize common controls.
	icex.dwSize = sizeof(INITCOMMONCONTROLSEX);
	icex.dwICC = ICC_TAB_CLASSES;
	InitCommonControlsEx(&icex);

	HWND tabC = CreateWindowExW(NULL, L"SysTabControl32", NULL, WS_CHILD | WS_VISIBLE,
		10, 302, 470, 330, hWnd, (HMENU)ID_TABCTRL, NULL, NULL);

	tie.mask = TCIF_TEXT | TCIF_IMAGE;
	tie.iImage = -1;
	tie.pszText = L"General";

	if (TabCtrl_InsertItem(tabC, TAB_GENERAL + 1, &tie) == -1)
	{
		DestroyWindow(tabC);
		return;
	}

	tie.pszText = L"Config";

	if (TabCtrl_InsertItem(tabC, TAB_CONFIG + 1, &tie) == -1)
	{
		DestroyWindow(tabC);
		return;
	}

	tie.pszText = L"Castle";

	if (TabCtrl_InsertItem(tabC, TAB_CASTLE + 1, &tie) == -1)
	{
		DestroyWindow(tabC);
		return;
	}

	tie.pszText = L"About";

	if (TabCtrl_InsertItem(tabC, TAB_ABOUT + 1, &tie) == -1)
	{
		DestroyWindow(tabC);
		return;
	}

	//Create description box
	CreateWindow(L"STATIC", L"Waiting...", WS_CHILD | WS_VISIBLE, 20, 401, 450, 20, hWnd, (HMENU)ID_CUP_STATS, GetModuleHandle(NULL), NULL);
	CreateWindow(L"STATIC", L"Waiting...", WS_CHILD | WS_VISIBLE, 20, 332, 450, 20, hWnd, (HMENU)ID_CLEARED_STATS, GetModuleHandle(NULL), NULL);
	CreateWindow(L"STATIC", L"Waiting...", WS_CHILD | WS_VISIBLE, 20, 355, 450, 20, hWnd, (HMENU)ID_CLEARED2_STATS, GetModuleHandle(NULL), NULL);
	CreateWindow(L"STATIC", L"Waiting...", WS_CHILD | WS_VISIBLE, 20, 378, 450, 20, hWnd, (HMENU)ID_DIAMOND_STATS, GetModuleHandle(NULL), NULL);

	CreateWindowExW(NULL, L"STATIC", NULL, WS_CHILD | WS_VISIBLE | SS_ETCHEDHORZ, 20, 492, 450, 2, hWnd, (HMENU)ID_SEP1, GetModuleHandle(NULL), NULL);
	CreateWindowExW(NULL, L"BUTTON", L"Move Bluestacks", WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON, 20, 436, 220, 24, hWnd, (HMENU)ID_AWIN_BTN, GetModuleHandle(NULL), NULL);
	CreateWindowExW(NULL, L"BUTTON", L"Remove Border", WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON, 250, 436, 220, 24, hWnd, (HMENU)ID_NWIN_BTN, GetModuleHandle(NULL), NULL);
	CreateWindowExW(NULL, L"BUTTON", L"Take Screenshot", WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON, 20, 465, 450, 24, hWnd, (HMENU)ID_SCREEN_BTN, GetModuleHandle(NULL), NULL);
	CreateWindow(RICHEDIT_CLASS, L"Start the bot whenever!\r\n", WS_CHILD | ES_AUTOVSCROLL | ES_AUTOHSCROLL | WS_HSCROLL | WS_VISIBLE | WS_VSCROLL | ES_MULTILINE | ES_READONLY | WS_BORDER, 20, 497, 450, 125, hWnd, (HMENU)ID_CONSOLE, GetModuleHandle(NULL), NULL);


	//Edit menu bar
	HMENU mainMenu = GetMenu(hWnd);
	debugMenu = CreateMenu();

	AppendMenuW(debugMenu, MF_STRING, ID_DEBUGMENU_RELOAD, L"Restart BlueStacks");
#ifdef _DEBUG
	AppendMenuW(debugMenu, MF_STRING, ID_DEBUGMENU_DISADBR, L"Disable ADB Restart");
	AppendMenuW(debugMenu, MF_STRING, ID_DEBUGMENU_TESTCUP, L"Run Cup Test");
	AppendMenuW(debugMenu, MF_STRING, ID_DEBUGMENU_TESTUPCASTLE, L"Run Upgrade Castle Test");
#endif
	AppendMenuW(mainMenu, MF_POPUP, (UINT_PTR)debugMenu, L"&Debug");
	DrawMenuBar(hWnd);

#ifndef _DEBUG
	if (showWhatsNew) {
		MessageBoxA(hWnd, "What's new you ask?\r\n\r\n--Fixed wizard ability usage", "What's new!", NULL);
	}
#endif

}

/***********
** Destroys GUI of bot
***********/
void __stdcall destroyWindow(void) {
	DestroyWindow(GetDlgItem(Util::getMainWindow(&bot), ID_CONSOLE));
	DestroyWindow(GetDlgItem(Util::getMainWindow(&bot), ID_SCREEN_BTN));
	DestroyWindow(GetDlgItem(Util::getMainWindow(&bot), ID_TABCTRL));
	DestroyWindow(GetDlgItem(Util::getMainWindow(&bot), ID_SEP1));
	DestroyWindow(GetDlgItem(Util::getMainWindow(&bot), ID_DRAGONS_CB));
	DestroyWindow(GetDlgItem(Util::getMainWindow(&bot), ID_WAVE_CB));
	DestroyWindow(GetDlgItem(Util::getMainWindow(&bot), ID_RWAVE_CB));
	DestroyWindow(GetDlgItem(Util::getMainWindow(&bot), ID_SKIPWAVE_CB));
	DestroyWindow(GetDlgItem(Util::getMainWindow(&bot), ID_WATCHADS_CB));
	DestroyWindow(GetDlgItem(Util::getMainWindow(&bot), ID_CLEARED2_STATS));
	DestroyWindow(GetDlgItem(Util::getMainWindow(&bot), ID_CLEARED_STATS));
	DestroyWindow(GetDlgItem(Util::getMainWindow(&bot), ID_DIAMOND_STATS));
	DestroyWindow(GetDlgItem(Util::getMainWindow(&bot), ID_SPAMABIL_CB));
	DestroyWindow(GetDlgItem(Util::getMainWindow(&bot), ID_CUP_STATS));
	DestroyWindow(GetDlgItem(Util::getMainWindow(&bot), ID_AWIN_BTN));
	DestroyWindow(GetDlgItem(Util::getMainWindow(&bot), ID_NWIN_BTN));
	DestroyWindow(GetDlgItem(Util::getMainWindow(&bot), ID_CASTLE_1));
	DestroyWindow(GetDlgItem(Util::getMainWindow(&bot), ID_CASTLE_2));
	DestroyWindow(GetDlgItem(Util::getMainWindow(&bot), ID_CASTLE_3));
	DestroyWindow(GetDlgItem(Util::getMainWindow(&bot), ID_CASTLE_4));
	DestroyWindow(GetDlgItem(Util::getMainWindow(&bot), ID_SCANCASTLE_BTN));
	DestroyWindow(GetDlgItem(Util::getMainWindow(&bot), ID_GDRAGON_CB));
	DestroyWindow(GetDlgItem(Util::getMainWindow(&bot), ID_BDRAGON_CB));
	DestroyWindow(GetDlgItem(Util::getMainWindow(&bot), ID_RDRAGON_CB));
	DestroyWindow(GetDlgItem(Util::getMainWindow(&bot), ID_SDRAGON_CB));
	DestroyWindow(GetDlgItem(Util::getMainWindow(&bot), ID_LDRAGON_CB));
	DestroyWindow(GetDlgItem(Util::getMainWindow(&bot), ID_HELL_CB));
	DestroyWindow(GetDlgItem(Util::getMainWindow(&bot), ID_RUNSEASON_CB));

	RemoveMenu(GetMenu(Util::getMainWindow(&bot)), (UINT_PTR)debugMenu, MF_BYCOMMAND);
	DrawMenuBar(Util::getMainWindow(&bot));

	if (richDLL)
		FreeLibrary(richDLL);
}

/***********
** Main bot run function
***********/
int __stdcall run(void) {
	//Update images
#ifndef _DEBUG
	Util::updateImages(bot.getMainWindow());
#endif

	HINSTANCE imageDLL = LoadLibraryA("ImageSearchDLL.dll");
	HINSTANCE richDLL = LoadLibraryA("riched20.dll");

	srand((unsigned)time(NULL));

	if (imageDLL) {
		pImageSearch = (imagesearch)GetProcAddress(imageDLL, "ImageSearch");
		pImageSearchEx = (imageSearchEx)GetProcAddress(imageDLL, "ImageSearchEx");
		pImageSearchExM = (imageSearchExM)GetProcAddress(imageDLL, "ImageSearchExMult");
		HDC hDC = GetDC(NULL);
		if (hDC) {
			width = GetDeviceCaps(hDC, HORZRES);
			height = GetDeviceCaps(hDC, VERTRES);

			runningEvent = EVENT_DRAGON;
			int randEvent = 0;

#ifndef _DEBUG
			Util::killBluestacks2(Util::getMainWindow(&bot));
			//Start bluestacks here
			Util::startBluestacks(Util::getMainWindow(&bot));
#endif

			//HWND hWnd = Util::getBluestacks();

			while (Util::getBluestacks() == NULL) {
				Sleep(1000);
			}

			MessageBoxA(NULL, "Click OK when BlueStacks2 has loaded the launcher!", "Click OK when BlueStacks2 has loaded!", NULL);

			ADB newADB;
			ADB* adbCon = &newADB;
			
			bot.setAdbCon(adbCon);
			adbCon->Clear();
			adbCon->startApp("com.raongames.growcastle");

			bot.startRunTimer();
			while (isRunning) {

				if (!isPausedB) {
					//Disable restart adb for debug
					if (disableADBRestart) {
						lastEventTime = 0;
					}
					if (activeEvents.size() > 0) {
						randEvent = (double)rand() / (RAND_MAX + 1) * (activeEvents.size())
							+ 0;
						runningEvent = activeEvents.at(randEvent);
					}
					else {
						runningEvent = EVENT_NONE;
					}
					if (bot.isMenu(Util::getBluestacks(), 900, 900, pImageSearch)) {

						//Go ahead and update cleared stats
						TCHAR buf2[400];
						swprintf(buf2, 400, L"Waves Cleared: %d - Waves Replayed: %d", wavesCleared, wavesReplayed);
						SetDlgItemText(Util::getMainWindow(&bot), ID_CLEARED_STATS, buf2);
						swprintf(buf2, 400, L"Dragons Cleared: %d", dragsCleared);
						SetDlgItemText(Util::getMainWindow(&bot), ID_CLEARED2_STATS, buf2);
						//Check for max diamonds and upgrade
						int dmnds = 0;
						if (bot.maxDiamonds(Util::getBluestacks(), width, height, pImageSearch)) {
							dmnds = 60;
						}
						else {
							dmnds = bot.getDiamonds(Util::getBluestacks(), pImageSearch);
						}

						if (lastDiamonds == -1) {
							lastDiamonds = dmnds;
							earnedDiamonds = 0;
						}
						else {
							if (dmnds > lastDiamonds)
								earnedDiamonds += (dmnds - lastDiamonds);
							lastDiamonds = dmnds;
						}

						swprintf(buf2, 400, L"Diamonds: %d - Diamonds Earned: %d", dmnds, earnedDiamonds);
						SetDlgItemText(Util::getMainWindow(&bot), ID_DIAMOND_STATS, buf2);

						//Watch ad if any.
						if (watchAds)
							bot.watchAd(Util::getBluestacks(), 900, 900, pImageSearch);

						//Upgrade castle
						if (upgradeCastle && dmnds == 60 && upgradeCastleInt != -1)
							bot.upgradeCastle(Util::getBluestacks(), width, height, pImageSearch, upgradeCastleInt);

						//Run randEvent
						switch (runningEvent) {
						case EVENT_REPLAY:
						{
							if (runRWaves && !strangeMonseterSpotted) {
								if (bot.getTicks() - lastEventTime < 10) {
									//Kill ADB
									adbCon->closeADB();
									Sleep(2000);
									ADB::disconnectADB();
									Util::AppendText(Util::getMainWindow(&bot), L"Restarting ADB connection\r\n", ID_CONSOLE);
								}
								lastEventTime = bot.getTicks();
								Util::setRichStyle(ID_CONSOLE, RGB(0, 0, 255), Util::getMainWindow(&bot));
								Util::AppendText(Util::getMainWindow(&bot), L"Replaying last wave\r\n", ID_CONSOLE);
								bot.clickReplayBtn(Util::getBluestacks(), width, height, pImageSearch);
								wavesReplayed += 1;
							}
						}
						break;
						case EVENT_SEASON:
						{
							if (runSeason && !strangeMonseterSpotted) {
								if (bot.getTicks() - lastEventTime < 10) {
									//Kill ADB
									adbCon->closeADB();
									Sleep(2000);
									ADB::disconnectADB();
									Util::AppendText(Util::getMainWindow(&bot), L"Restarting ADB connection\r\n", ID_CONSOLE);
								}
								lastEventTime = bot.getTicks();
								Util::setRichStyle(ID_CONSOLE, RGB(0, 0, 255), Util::getMainWindow(&bot));
								Util::AppendText(Util::getMainWindow(&bot), L"Starting season battle\r\n", ID_CONSOLE);
								if (dmnds >= 5) {
									bot.runSeason(Util::getBluestacks(), width, height, pImageSearch);
								}
								else {
									Util::AppendText(Util::getMainWindow(&bot), L"Insufficient diamonds!\r\n", ID_CONSOLE);
								}
							}
						}
						break;
						case EVENT_HELL:
						{
							if (runHell && !strangeMonseterSpotted) {
								if (bot.getTicks() - lastEventTime < 10) {
									//Kill ADB
									adbCon->closeADB();
									Sleep(2000);
									ADB::disconnectADB();
									Util::AppendText(Util::getMainWindow(&bot), L"Restarting ADB connection\r\n", ID_CONSOLE);
								}
								lastEventTime = bot.getTicks();
								Util::setRichStyle(ID_CONSOLE, RGB(0, 0, 255), Util::getMainWindow(&bot));
								Util::AppendText(Util::getMainWindow(&bot), L"Entering HELL!\r\n", ID_CONSOLE);
								bot.runHell(Util::getBluestacks(), width, height, pImageSearch);
							}
						}
						break;
						case EVENT_WAVE:
						{
							if (runWaves && !strangeMonseterSpotted) {
								if (bot.getTicks() - lastEventTime < 10) {
									//Kill ADB
									adbCon->closeADB();
									Sleep(2000);
									ADB::disconnectADB();
									Util::AppendText(Util::getMainWindow(&bot), L"Restarting ADB connection\r\n", ID_CONSOLE);
								}
								lastEventTime = bot.getTicks();
								Util::setRichStyle(ID_CONSOLE, RGB(255, 0, 0), Util::getMainWindow(&bot));
								Util::AppendText(Util::getMainWindow(&bot), L"Playing next wave\r\n", ID_CONSOLE);
								bot.clickWaveBtn(Util::getBluestacks(), width, height, pImageSearch, skipWaves, dmnds);
								Sleep(100);
								wavesCleared += 1;
							}
						}
						break;
						case EVENT_DRAGON:
						{
							if (runDragons) {
								if (bot.getTicks() - lastEventTime < 10) {
									//Kill ADB
									adbCon->closeADB();
									Sleep(2000);
									ADB::disconnectADB();
									Util::AppendText(Util::getMainWindow(&bot), L"Restarting ADB connection\r\n", ID_CONSOLE);
								}
								lastEventTime = bot.getTicks();
								Util::setRichStyle(ID_CONSOLE, RGB(0, 255, 0), Util::getMainWindow(&bot));
								Util::AppendText(Util::getMainWindow(&bot), L"Fighting dragon\r\n", ID_CONSOLE);
								int dragon = -1;
								if (activeDragons.size() > 0) {
									int randDragon = randEvent = (double)rand() / (RAND_MAX + 1) * (activeDragons.size())
										+ 0;
									dragon = activeDragons.at(randDragon);
								}
								
								if (bot.runDragonWave(Util::getBluestacks(), width, height, pImageSearch, dragon) == 1) {
									//Sell B Items
									Util::AppendText(Util::getMainWindow(&bot), L"Inventory full; Selling B items\r\n", ID_CONSOLE);
									Sleep(1500);
									//sellBItems(hWnd, width, height, pImageSearch);
								}
								else {
									dragsCleared += 1;
								}
							}
						}
						break;
						default:
							break;
						}
					}
					else {
						if (spamAbilities)
							bot.clickAbility(Util::getBluestacks(), width, height, pImageSearch);
						if (bot.isEvent(Util::getBluestacks(), width, height, pImageSearch)) {
							bot.clickSpeedBtn(Util::getBluestacks(), width, height, pImageSearch);
							bot.clickTongueChest(Util::getBluestacks(), width, height, pImageSearch);
							if (bot.findStrangeMonster(Util::getBluestacks(), width, height, pImageSearch)) {
								if (bot.isWave(Util::getBluestacks(), width, height, pImageSearch)) {
									bot.cancelWave(Util::getBluestacks(), width, height, pImageSearch);
								}
								bot.killApp("com.raongames.growcastle");
								adbCon->startApp("com.raongames.growcastle");
								if (!strangeMonseterSpotted) {
									Util::setRichStyle(ID_CONSOLE, RGB(226, 204, 72), Util::getMainWindow(&bot));
									Util::AppendText(Util::getMainWindow(&bot), L"Strange monster detected. Running only dragons.\r\n", ID_CONSOLE);
									strangeMonseterSpotted = true;
									sSettings.replay = runRWaves;
									sSettings.dragons = runDragons;
									sSettings.waves = runWaves;
									runWaves = false;
									runRWaves = false;
									runDragons = true;
									monFirstSpotted = bot.getTicks();
								}
								monLastSpot = bot.getTicks();
								if ((monLastSpot - monFirstSpotted) / 60 > 60) {
									//Restart app if monster has been around for 1 hour
									bot.killApp("com.raongames.growcastle");
									adbCon->startApp("com.raongames.growcastle");
								}
							}
							else {
								if (monLastSpot > 0) {
									if ((bot.getTicks() - monLastSpot) / 60 > 1) {
										Sleep(1000);
										Util::setRichStyle(ID_CONSOLE, RGB(226, 204, 72), Util::getMainWindow(&bot));
										Util::AppendText(Util::getMainWindow(&bot), L"Strange monster has left.\r\n", ID_CONSOLE);
										monLastSpot = 0;
										monFirstSpotted = 0;
										strangeMonseterSpotted = false;
										runWaves = sSettings.waves;
										runDragons = sSettings.dragons;
										runRWaves = sSettings.replay;
									}
								}
							}
						}else if (bot.getDragonDrop(Util::getBluestacks(), width, height, pImageSearch)) {
							if (bot.getTicks() - lastBGEventTime < 10) {
								//Kill ADB
								adbCon->closeADB();
								Sleep(2000);
								ADB::disconnectADB();
								Util::AppendText(Util::getMainWindow(&bot), L"Restarting ADB connection\r\n", ID_CONSOLE);
							}
							lastBGEventTime = bot.getTicks();
						}
						else {

							int cupRet = bot.solveDiamond(Util::getBluestacks(), width, height, pImageSearch, pImageSearchExM, pImageSearchEx);
							if (cupRet != -1) {
								totalCups++;
								if (cupRet == 1) {
									//Right Cup
									correctCups++;
								}
								TCHAR buf2[400];
								swprintf(buf2, 400, L"Correct Cups: %d - Total Cups: %d", correctCups, totalCups);
								SetDlgItemText(Util::getMainWindow(&bot), ID_CUP_STATS, buf2);
							}
							else {
								if (!bot.clickSeasonOK(Util::getBluestacks(), width, height, pImageSearch)) {
									// add to function in Bot class
									/*char buffer[MAX_PATH];
									sprintf(buffer, "*%d %s\\imgs\\general\\menus\\x.bmp", lostCount, Util::getCurrentDirectoryA());
									char* result = (*pImageSearch)(hWnd, 0, 0, width, height, buffer);

									if (strcmp(result, "0") != 0) {
										struct IMAGELOC resXY = Util::getXY(result);

										adbCon->Click(resXY.x, resXY.y);
										lostCount = 30;
										Sleep(50);
									}
									else {
										lostCount++;
									}*/

									if (bot.clickXBtn(lostCount, Util::getBluestacks(), width, height, pImageSearch)) {
										lostCount = 30;
										Sleep(50);
									}
									else {
										lostCount++;
									}
								}
							}
						}
					}
				}
				Sleep(25); // 250
			}
			KillTimer(Util::getMainWindow(&bot), ID_MAIN_TIMER);
			//adbCon->~ADB();
			SecureZeroMemory(adbCon, sizeof(adbCon));
			if (hDC)
				ReleaseDC(NULL, hDC);
			if (imageDLL)
				FreeLibrary(imageDLL);
			if (richDLL)
				FreeLibrary(richDLL);
		}
		else {
			return -3; //HDC not intialiazed properly
		}
	}
	else {
		return -2; //ImageSearchDLL not intialized properly
	}
	return 0; //Bot stopped
}
