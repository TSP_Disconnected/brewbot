#include "ADB.h"
#include "Utility.h"
#include <fstream>

ADB::ADB()
{
	adbInRead = NULL;
	adbInWrite = NULL;
	adbOutRead = NULL;
	adbOutWrite = NULL;

	while (!initADB()) {
		//connectADB();
		Sleep(2000);
	}
}

ADB::~ADB()
{
	closeADB();
	//disconnectADB();
}

void ADB::Clear()
{
	sendADB("\n", sizeof("\n"));
}

void ADB::killApp(char* package) {
	char buf[100];
	int s = snprintf(buf, 100, "am force-stop %s\n", package);
	sendADB(buf, s);
}

void ADB::Click(int x, int y)
{
	char buf[100];
	int s = snprintf(buf, 100, "input tap %d %d\n", x, y);
	sendADB(buf, s);
}

void ADB::startApp(char* package)
{
	char buf[50];
	int s = sprintf(buf, "monkey -p %s 1\n", package);
	sendADB(buf, s);
}

bool ADB::sendADB(char* cmd, int size)
{
	if (!isAlive()) {
		while (!initADB()) {
			//connectADB();
			Sleep(2000);
		}
	}
	DWORD wrote;
	if (!WriteFile(adbInWrite, cmd, size, &wrote, NULL))
		return false;
	Sleep(100);
	/*CHAR chBuf[4096];
	BOOL bSuccess = FALSE;
	DWORD dwRead;

	while(bSuccess = ReadFile(adbOutRead, chBuf, 4096, &dwRead, NULL))
	{
		char* result;
		if (!bSuccess || dwRead == 0)
		break;
		result = (char*)malloc(dwRead * sizeof(char));
		snprintf(result, dwRead, "%s", chBuf);
		MessageBoxA(NULL, result, result, NULL);
		free(result);
	}*/
	return true;
}

void ADB::closeADB()
{
	//Send exit command
	sendADB("exit\n", sizeof("exit\n"));
	//Close STD handles
	CloseHandle(adbInfo.hProcess);
	CloseHandle(adbInfo.hThread);
	CloseHandle(adbInRead);
	CloseHandle(adbOutRead);
	CloseHandle(adbOutWrite);
	CloseHandle(adbInWrite);
}

void ADB::getApp()
{
	sendADB("dumpsys window windows | grep -E 'mFocusedApp'| cut -d / -f 1 | cut -d " " -f 7", sizeof("dumpsys window windows | grep -E 'mFocusedApp'| cut -d / -f 1 | cut -d " " -f 7"));
}

void ADB::disconnectADB()
{
	//Disconnect ADB
	PROCESS_INFORMATION ProcessInfo;

	STARTUPINFO StartupInfo;

	//CloseHandle(this->adbInfo.hThread);

	ZeroMemory(&StartupInfo, sizeof(StartupInfo));
	StartupInfo.cb = sizeof StartupInfo;
	wchar_t buffer[512];
	int len = swprintf(buffer, 512, L"\"%s\\adb.exe\" kill-server", Util::getCurrentDirectoryW());
	if (CreateProcessW(0, buffer,
		NULL, NULL, FALSE, CREATE_NO_WINDOW, NULL,
		NULL, &StartupInfo, &ProcessInfo))
	{
		WaitForSingleObject(ProcessInfo.hProcess, INFINITE);
		CloseHandle(ProcessInfo.hThread);
		CloseHandle(ProcessInfo.hProcess);
	}
	else
	{
		MessageBox(NULL, L"ERROR: 1001", L"Oops", NULL);
	}
}

bool ADB::connectADB()
{
	PROCESS_INFORMATION ProcessInfo;

	STARTUPINFO StartupInfo;

	ZeroMemory(&StartupInfo, sizeof(StartupInfo));
	StartupInfo.cb = sizeof StartupInfo;
	wchar_t buffer[512];
	int len = swprintf(buffer, 512, L"\"%s\\adb.exe\" connect localhost:5555", Util::getCurrentDirectoryW());
	if (CreateProcessW(0, buffer,
		NULL, NULL, FALSE, CREATE_NO_WINDOW, NULL,
		NULL, &StartupInfo, &ProcessInfo))
	{
		WaitForSingleObject(ProcessInfo.hProcess, INFINITE);
		CloseHandle(ProcessInfo.hThread);
		CloseHandle(ProcessInfo.hProcess);
		return true;
	}
	else
	{
		MessageBox(NULL, L"ERROR: 1001", L"Oops", NULL);
	}
	return false;
}

bool ADB::isAlive() {
	DWORD dwExitCode;
	if (GetExitCodeProcess(adbInfo.hProcess, &dwExitCode)) {
		if (dwExitCode == STILL_ACTIVE) {
			return true;
		}
		else {
			return false;
		}
	}
	else {
		return false;
	}
}

bool ADB::initADB() {
	SECURITY_ATTRIBUTES saAttr;
	saAttr.nLength = sizeof(SECURITY_ATTRIBUTES);
	saAttr.bInheritHandle = TRUE;
	saAttr.lpSecurityDescriptor = NULL;

	if (!CreatePipe(&adbInRead, &adbInWrite, &saAttr, 0))
		return false;

	if (!SetHandleInformation(adbInWrite, HANDLE_FLAG_INHERIT, 0))
		return false;

	if (!CreatePipe(&adbOutRead, &adbOutWrite, &saAttr, 0))
		return false;

	if (!SetHandleInformation(this->adbOutRead, HANDLE_FLAG_INHERIT, 0))
		return false;

	//Start process
	PROCESS_INFORMATION ProcessInfo;

	STARTUPINFO StartupInfo;

	ZeroMemory(&StartupInfo, sizeof(StartupInfo));
	StartupInfo.cb = sizeof(StartupInfo);
	StartupInfo.hStdError = adbOutWrite;
	StartupInfo.hStdOutput = adbOutWrite;
	StartupInfo.hStdInput = adbInRead;
	StartupInfo.dwFlags |= STARTF_USESTDHANDLES;
	wchar_t buffer[512];
	int len = swprintf(buffer, 512, L"\"%s\\adb.exe\" -s emulator-5554 shell", Util::getCurrentDirectoryW());
	//MessageBox(NULL, buffer, buffer, NULL);
	if (CreateProcessW(NULL, buffer,
		NULL, NULL, TRUE, CREATE_NO_WINDOW, NULL,
		NULL, &StartupInfo, &ProcessInfo))
	{
		//CloseHandle(ProcessInfo.hThread);
		//CloseHandle(ProcessInfo.hProcess);
		adbInfo = ProcessInfo;
	}
	else {
		MessageBoxA(NULL, "FAIL", "FAIL", NULL);
		return false;
	}
	//sendADB("wm density 160", sizeof("wm density 160"));
	//sendADB("wm size reset", sizeof("wm size reset"));
	//sendADB("reboot", sizeof("reboot"));
	CHAR chBuf[4096];
	BOOL bSuccess = FALSE;
	DWORD dwRead;
	char *result;
	for (;;)
	{
		bSuccess = ReadFile(adbOutRead, chBuf, 4096, &dwRead, NULL);
		if (!bSuccess || dwRead == 0)
			break;
		//MessageBoxA(NULL, chBuf, chBuf, NULL);
		if ((result = strstr(chBuf, "error: device 'emulator-5554' not found")) != NULL)
			return false;
		else
			break;
	}
	return true;
}