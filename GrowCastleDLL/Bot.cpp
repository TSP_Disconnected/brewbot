#include "Bot.h"
#include "Utility.h"
#include <fstream>

std::map<UINT_PTR, Bot*> Bot::m_Bot;

void Bot::addTick(Bot *bot)
{
	bot->pastTicks++;
}

VOID Bot::runTimeFunc(HWND hwnd, UINT message, UINT idTimer, DWORD dwTime)
{
	Bot *myBot = m_Bot[idTimer];
	if (myBot->getPaused())
		return;
	myBot->addTick(myBot);
	//if (isPaused())
	//	return;
	myBot->runTimeSec += 1;
	if (myBot->runTimeSec == 60) {
		myBot->runTimeSec = 0;
		myBot->runTimeMin += 1;
	}
	if (myBot->runTimeMin == 60) {
		myBot->runTimeMin = 0;
		myBot->runTimeHours += 1;
	}
	TCHAR sec[5];
	if (myBot->runTimeSec < 10)
		swprintf(sec, 5, L"0%d", myBot->runTimeSec);
	else
		swprintf(sec, 5, L"%d", myBot->runTimeSec);

	TCHAR min[5];
	if (myBot->runTimeMin < 10)
		swprintf(min, 5, L"0%d", myBot->runTimeMin);
	else
		swprintf(min, 5, L"%d", myBot->runTimeMin);

	TCHAR buf[100];
	swprintf(buf, 100, L"Running for %d:%s:%s...", myBot->runTimeHours, min, sec);
	SetDlgItemText(Util::getMainWindow(myBot), ID_RUN_LBL, buf);
	HWND hWndStatusBar = GetDlgItem(Util::getMainWindow(myBot), ID_STATUSBAR);
	SendMessage(hWndStatusBar, SB_SETTEXT, 0, (LPARAM)buf);
}

HWND Bot::getMainWindow()
{
	return mainHWND;
}

Bot::Bot(ADB* adb, HWND wnd)
{
	adbCon = adb;
	mainHWND = wnd;
}

Bot::Bot(HWND wnd)
{
	mainHWND = wnd;
}

Bot::~Bot()
{
}

void Bot::setAdbCon(ADB * adb)
{
	adbCon = adb;
}

void Bot::startRunTimer()
{
	runTimeSec = 0;
	runTimeMin = 0;
	runTimeHours = 0;
	UINT_PTR id = SetTimer(Util::getMainWindow(this), ID_MAIN_TIMER, 1000, (TIMERPROC)runTimeFunc);
	m_Bot[id] = this;
}

void Bot::killApp(char* package) {
	adbCon->killApp("com.raongames.growcastle");
}

void Bot::getApp()
{
	adbCon->getApp();
}

DWORD Bot::getTicks()
{
	return pastTicks;
}

bool Bot::maxDiamonds(HWND hw, int w, int h, imagesearch funcptr)
{
	char buffer[1024];
	sprintf(buffer, "*5 %s\\imgs\\max_d.bmp", Util::getCurrentDirectoryA());
	char* result = (*funcptr)(hw, 0, 0, w, h, buffer);
	if (strcmp(result, "0") != 0)
		return true;
	return false;
}

void Bot::clickAbility(HWND hw, int w, int h, imagesearch funcptr) {
	char buffer[1024];
	sprintf(buffer, "*5 %s\\imgs\\ability.bmp", Util::getCurrentDirectoryA());
	char* result = (*funcptr)(hw, 0, 0, w, h, buffer);

	if (strcmp(result, "0") != 0) {
		struct IMAGELOC resXY = Util::getXY(result);

		adbCon->Click(resXY.x, resXY.y);
		Sleep(50);
		adbCon->Click(733, 453);
		//sprintf(buffer, "*20 %s\\imgs\\aoe_ring.bmp", Util::getCurrentDirectoryA());
		/*result = (*funcptr)(hw, 0, 0, w, h, buffer);
		if (strcmp(result, "0") != 0) {
			struct IMAGELOC resXY = Util::getXY(result);
			Sleep(50);
			if (isEvent(hw, w, h, funcptr))
				adbCon->Click(resXY.x, resXY.y);
		}*/
	}
}

int Bot::getDiamonds(HWND hw, imagesearch funcptr)
{
	int diamonds = 0;
	int x = 0;
	for (int i = 0; i <= 9; i++) {
		char buf4[512];
		char *result;
		sprintf(buf4, "*100 %s\\imgs\\d_%d.bmp", Util::getCurrentDirectoryA(), i);
		result = (*funcptr)(hw, 210, 0, 250, 30, buf4);

		if (strcmp(result, "0") != 0) {
			struct IMAGELOC resXY = Util::getXY(result);
			x = resXY.x;
			diamonds = i * 10;
			break;
		}
	}
	for (int i = 0; i <= 9; i++) {
		char buf3[512];
		char *result;
		int thisX = 230;
		sprintf(buf3, "*100 %s\\imgs\\d_%d.bmp", Util::getCurrentDirectoryA(), i);
		if (x > 0)
			thisX = x;
		result = (*funcptr)(hw, thisX, 0, 280, 30, buf3);

		if (strcmp(result, "0") != 0) {
			diamonds = diamonds + i;
			break;
		}
	}
	return diamonds;
}

void Bot::watchAdOld(HWND hw, int w, int h, imagesearch funcptr)
{
	Util::setRichStyle(ID_CONSOLE, RGB(250, 30, 100), getMainWindow());
	Util::AppendText(getMainWindow(), L"Searching for ad...\r\n", ID_CONSOLE);
	Sleep(200);
	char buffer[1024];
	char buffer2[1024];
	sprintf(buffer, "*5 %s\\imgs\\ad_btn.bmp", Util::getCurrentDirectoryA());
	char* result = (*funcptr)(hw, 0, 0, w, h, buffer);

	if (strcmp(result, "0") != 0) {
		struct IMAGELOC resXY = Util::getXY(result);

		adbCon->Click(resXY.x, resXY.y);
		Util::AppendText(getMainWindow(), L"Watching ad...\r\n", ID_CONSOLE);
		Sleep(750);
		sprintf(buffer, "*5 %s\\imgs\\ad_x.bmp", Util::getCurrentDirectoryA());
		result = (*funcptr)(hw, 0, 0, w, h, buffer);
		sprintf(buffer2, "*5 %s\\imgs\\general\\ads\\x_1.bmp", Util::getCurrentDirectoryA());
		char* result2 = (*funcptr)(hw, 0, 0, w, h, buffer2);
		if (strcmp(result, "0") == 0) {
			DWORD lastTick = getTicks();
			while (strcmp(result, "0") == 0) {
				if (getTicks() - lastTick >= 70) {
					Util::setRichStyle(ID_CONSOLE, RGB(250, 0, 0), getMainWindow());
					Util::AppendText(getMainWindow(), L"Ad failed...?\r\n", ID_CONSOLE);
					break;
				}
				result = (*funcptr)(hw, 0, 0, w, h, buffer);
				if (strcmp(result, "0") != 0) {
					struct IMAGELOC resXY = Util::getXY(result);

					adbCon->Click(resXY.x, resXY.y);
				}
				Sleep(1000);
			}
			if (strcmp(result2, "0") == 0) {
					result2 = (*funcptr)(hw, 0, 0, w, h, buffer2);
					if (strcmp(result2, "0") != 0) {
						struct IMAGELOC resXY = Util::getXY(result2);

						adbCon->Click(resXY.x, resXY.y);
					}
			}
			else {
				struct IMAGELOC resXY = Util::getXY(result2);

				adbCon->Click(resXY.x, resXY.y);
			}
		}
		else {
			struct IMAGELOC resXY = Util::getXY(result);

			adbCon->Click(resXY.x, resXY.y);
		}
	}
}

void Bot::watchAd(HWND hw, int w, int h, imagesearch funcptr)
{
	Util::setRichStyle(ID_CONSOLE, RGB(250, 30, 100), getMainWindow());
	Util::AppendText(getMainWindow(), L"Searching for ad...\r\n", ID_CONSOLE);
	Sleep(200);

	if (Util::ClickImageWait("*5", "imgs\\ad_btn.bmp", hw, funcptr, this, adbCon, w, h)) {
		Util::AppendText(getMainWindow(), L"Watching ad...\r\n", ID_CONSOLE);
		Sleep(750);
		DWORD lastTick = getTicks();
		while (getTicks() - lastTick < 70) {
			Sleep(1000);
		}
		adbCon->Click(809, 65);
	}
}

void Bot::upgradeCastle(HWND hWnd, int w, int h, imagesearch funcptr, int index)
{
	Util::setRichStyle(ID_CONSOLE, RGB(250, 30, 100), getMainWindow());
	Util::AppendText(getMainWindow(), L"Upgrading castle base\r\n", ID_CONSOLE);
	adbCon->Click(20, 410);
	Sleep(100);
	
	char buffer[1024];
	sprintf(buffer, "*55 %s\\imgs\\castle\\b_%d.bmp", Util::getCurrentDirectoryA(), Castle[0]);
	char* result = (*funcptr)(hWnd, 0, 0, w, h, buffer);

	if (strcmp(result, "0") != 0) {
		struct IMAGELOC resXY = Util::getXY(result);

		adbCon->Click(resXY.x, resXY.y);
		Sleep(150);
		if(index == 0)
			sprintf(buffer, "*5 %s\\imgs\\castle\\b_%d.bmp", Util::getCurrentDirectoryA(), index);
		else
			sprintf(buffer, "*5 %s\\imgs\\castle\\c_%d.bmp", Util::getCurrentDirectoryA(), index);
		char* result = (*funcptr)(hWnd, 0, 0, w, h, buffer);

		if (strcmp(result, "0") != 0) {
			struct IMAGELOC resXY = Util::getXY(result);
			adbCon->Click(resXY.x, resXY.y);
			Sleep(500);
			if (index == 0)
				sprintf(buffer, "*5 %s\\imgs\\castle\\menus\\b_%d.bmp", Util::getCurrentDirectoryA(), index);
			else
				sprintf(buffer, "*5 %s\\imgs\\castle\\menus\\c_%d.bmp", Util::getCurrentDirectoryA(), index);
			result = (*funcptr)(hWnd, 0, 0, w, h, buffer);

			if (strcmp(result, "0") != 0) {
				struct IMAGELOC resXY = Util::getXY(result);

				adbCon->Click(resXY.x, resXY.y);
				Sleep(500);
				sprintf(buffer, "*5 %s\\imgs\\castle\\menus\\lvlup.bmp", Util::getCurrentDirectoryA());
				result = (*funcptr)(hWnd, 0, 0, w, h, buffer);
				if (strcmp(result, "0") != 0) {
					struct IMAGELOC resXY = Util::getXY(result);
					for (int i = 0; i < 30; i++) {
						Sleep(17);
						adbCon->Click(resXY.x, resXY.y);
					}
				}
			}
		}
	}
}

bool Bot::isMenu(HWND hw, int w, int h, imagesearch funcptr)
{
	char buffer[1024];
	sprintf(buffer, "*5 %s\\imgs\\replay.bmp", Util::getCurrentDirectoryA());
	//MessageBoxA(NULL, buffer, buffer, NULL);
	char* result = (*funcptr)(hw, 0, 0, w, h, buffer);

	if (strcmp(result, "0") != 0)
		return true;
	return false;
}

int Bot::runDragonWave(HWND hw, int w, int h, imagesearch funcptr, int dragon)
{
	int stage = 0;
	char buffer[1024];
	if (Util::ClickImageWait("*5", "imgs\\dragon_shrine.bmp", hw, funcptr, this, adbCon, w, h)) {
		char* dragonimg;
		switch (dragon) {
			case DRAGON_GREEN:
				{
					dragonimg = "green_dragon";
				}
				break;
			case DRAGON_BLACK:
				{
					dragonimg = "black_dragon";
				}
				break;
			case DRAGON_RED:
				{
					dragonimg = "red_dragon";
				}
				break;
			case DRAGON_LEGENDARY:
				{
					dragonimg = "legend_dragon";
				}
				break;
			case DRAGON_SIN:
				{
					dragonimg = "sin_dragon";
				}
				break;
			default:
				{
					dragonimg = "black_dragon";
				}
				break;
		}
		sprintf(buffer, "imgs\\%s.bmp", dragonimg);
		if (Util::ClickImageWait("*5", buffer, hw, funcptr, this, adbCon, w, h)) {
			if (Util::ClickImageWait("*155", "imgs\\dragon_battle.bmp", hw, funcptr, this, adbCon, w, h)) {

			}
		}
	}
	
		/*result = (*funcptr)(hw, 0, 0, w, h, "full_inv.bmp");

		if (strcmp(result, "0") != 0) {
		result = (*funcptr)(hw, 0, 0, w, h, "x_btn.bmp");

		if (strcmp(result, "0") != 0) {
		struct imageLoc resXY = Util::getXY(result);

		click(resXY.x, resXY.y);
		return 1; //Inventory full. Sell B items
		}
		return -4; //Could not find x button
		}*/

	return 0;
}

void Bot::clickReplayBtn(HWND hw, int w, int h, imagesearch funcptr)
{
	if (Util::ClickImageWait("*5", "imgs\\replay.bmp", hw, funcptr, this, adbCon, w, h)) {
		if (Util::ClickImageWait("*5", "imgs\\100_replay.bmp", hw, funcptr, this, adbCon, w, h)) {
		}
	}
}

void Bot::runHell(HWND hw, int w, int h, imagesearch funcptr)
{
	if (Util::ClickImageWait("*5", "imgs\\hell.bmp", hw, funcptr, this, adbCon, w, h)) {
		if (Util::ClickImageWait("*5",  "imgs\\hell_battle.bmp", hw, funcptr, this, adbCon, w, h)) {
			if (Util::ClickImageWait("*5",  "imgs\\hell_x.bmp", hw, funcptr, this, adbCon, w, h)) {

			}
		}
	}
}

void Bot::runSeason(HWND hw, int w, int h, imagesearch funcptr)
{
	if (Util::ClickImageWait("*5", "imgs\\season_map.bmp", hw, funcptr, this, adbCon, w, h)) {
		Util::ClickImageWait("*30", "imgs\\general\\menus\\x_0.bmp", hw, funcptr, this, adbCon, w, h);
		if (Util::ClickImageWait("*5", "imgs\\season_banner.bmp", hw, funcptr, this, adbCon, w, h)) {
			if (Util::ClickImageWait("*5", "imgs\\season_battle.bmp", hw, funcptr, this, adbCon, w, h)) {
				if (Util::ClickImageWait("*5", "imgs\\season_diamonds.bmp", hw, funcptr, this, adbCon, w, h)) {

				}
			}
		}
	}
}


void Bot::clickWaveBtn(HWND hw, int w, int h, imagesearch funcptr, bool skipWaves, int diamonds)
{
	if (Util::ClickImageWait("*5", "imgs\\battle_btn.bmp", hw, funcptr, this, adbCon, w, h)) {
		if (skipWaves && diamonds >= 7) {
			Sleep(1200);
			if (diamonds >= 20) {
				if (Util::ClickImageWait("*5", "imgs\\skip_20.bmp", hw, funcptr, this, adbCon, w, h)) {
				}
			}
			else if (diamonds >= 12) {
				if (Util::ClickImageWait("*5", "imgs\\skip_12.bmp", hw, funcptr, this, adbCon, w, h)) {
				}
			}
			else if (diamonds >= 7) {
				if (Util::ClickImageWait("*5", "imgs\\skip_7.bmp", hw, funcptr, this, adbCon, w, h)) {
				}
			}
		}
	}
}

void Bot::clickSpeedBtn(HWND hw, int w, int h, imagesearch funcptr)
{
	if (Util::ClickImage("*5", "imgs\\1xspeed.bmp", hw, funcptr, this, adbCon, w, h)) {
		Util::setRichStyle(ID_CONSOLE, RGB(75, 23, 152), Util::getMainWindow(this));
		Util::AppendText(Util::getMainWindow(this), L"Set speed to x2\r\n", ID_CONSOLE);
	}
}

int Bot::clickXBtn(int tolerance, HWND hw, int w, int h, imagesearch funcptr)
{
	char tol[6];
	snprintf(tol, 5, "*%d", tolerance);

	for (int i = 0; i < NUM_XBTN_IMGS; i++) { //i = x_#.bmp
		char buffer[MAX_PATH];
		snprintf(buffer, MAX_PATH - 1, "imgs\\general\\menus\\x_%d.bmp", i);
		if (Util::ClickImage(tol, buffer, hw, funcptr, this, adbCon, w, h)) {
			return TRUE;
		}
	}
	return FALSE;
}

int Bot::clickSeasonOK(HWND hw, int w, int h, imagesearch funcptr)
{
	return Util::ClickImage("*5", "imgs\\season_ok.bmp", hw, funcptr, this, adbCon, w, h);
}

bool Bot::getDragonDrop(HWND hw, int w, int h, imagesearch funcptr)
{
	char buffer[1024];
	sprintf(buffer, "*5 %s\\imgs\\mat2_btn.bmp", Util::getCurrentDirectoryA());
	char* result = (*funcptr)(hw, 0, 0, w, h, buffer);

	if (strcmp(result, "0") != 0) {
		struct IMAGELOC resXY = Util::getXY(result);

		adbCon->Click(resXY.x, resXY.y);
		adbCon->Click(resXY.x, resXY.y);
		Util::AppendText(Util::getMainWindow(this), L"Collected the material\r\n", ID_CONSOLE);
		Sleep(750);
		return true;
	}
	else {
		sprintf(buffer, "*5 %s\\imgs\\mat_btn.bmp", Util::getCurrentDirectoryA());
		result = (*funcptr)(hw, 0, 0, w, h, buffer);

		if (strcmp(result, "0") != 0) {
			struct IMAGELOC resXY = Util::getXY(result);

			adbCon->Click(resXY.x, resXY.y);
			adbCon->Click(resXY.x, resXY.y);
			Util::AppendText(Util::getMainWindow(this), L"Collected the material\r\n", ID_CONSOLE);
			return true;
		}
		else
			return false; //Couldnt find image
	}
}

void Bot::clickTongueChest(HWND hw, int w, int h, imagesearch funcptr)
{
	if (Util::ClickImage("*5", "imgs\\tongue_chest.bmp", hw, funcptr, this, adbCon, w, h)) {
		Util::AppendText(Util::getMainWindow(this), L"Collected mimic chest!\r\n", ID_CONSOLE);
	}
}

bool Bot::findStrangeMonster(HWND hw, int w, int h, imagesearch funcptr) {
	char buffer[1024];
	sprintf(buffer, "*5 %s\\imgs\\starnge_monster.bmp", Util::getCurrentDirectoryA());
	char *result = (*funcptr)(hw, 0, 0, w, h, buffer);

	if (strcmp(result, "0") != 0) {
		return true;
	}
	return false;
}

int Bot::solveDiamond(HWND hw, int w, int h, imagesearch funcptr, imageSearchExM f2, imageSearchEx f3)
{
	char buffer[1024];
	sprintf(buffer, "*5 %s\\imgs\\ab_d\\where.bmp", Util::getCurrentDirectoryA());
	char *result = (*funcptr)(hw, 0, 0, w, h, buffer);

	sprintf(buffer, "*5 %s\\imgs\\ab_d\\ab_hero.bmp", Util::getCurrentDirectoryA());
	char *result2 = (*funcptr)(hw, 0, 0, w, h, buffer);

	sprintf(buffer, "*5 %s\\imgs\\ab_d\\ab_left.bmp", Util::getCurrentDirectoryA());
	char *result4 = (*funcptr)(hw, 0, 0, w, h, buffer);

	sprintf(buffer, "*5 %s\\imgs\\ab_d\\start.bmp", Util::getCurrentDirectoryA());
	char *result3 = (*funcptr)(hw, 0, 0, w, h, buffer);

	if ((strcmp(result, "0") != 0 || strcmp(result2, "0") != 0) && strcmp(result3, "0") != 0 && strcmp(result4, "0") == 0) {
		Util::AppendText(Util::getMainWindow(this), L"Diamond antibot detected!\r\n", ID_CONSOLE);
	
			struct IMAGELOC resXY = Util::getXY(result3);

			int j = 0;

			sprintf(buffer, "%s\\AntiBot\\ab%d", Util::getCurrentDirectoryA(), j);
			CreateDirectoryA(buffer, NULL);

			/*while (true) {
				sprintf(buffer, "%s\\AntiBot\\ab%i", Util::getCurrentDirectoryA(), j);
				if (CreateDirectoryA(buffer, NULL))
					break;
				j++;
			}*/
			//sprintf(buffer, "%s\\AntiBot\\ab%d", Util::getCurrentDirectoryA(), j);
			//CreateDirectoryA(buffer, NULL);

			adbCon->Click(resXY.x, resXY.y);
			Sleep(136);
			for (int i = 0; i < 150; i++) {
				sprintf(buffer, "%s\\AntiBot\\ab%d\\snap%d.bmp", Util::getCurrentDirectoryA(), j, i);
				Util::CaptureScreen(hw, buffer);
				Sleep(5);
			}
			sprintf(buffer, "ab%d", j);
			IMAGELOC cup = Util::getCup(buffer, f2, f3);
			adbCon->Click(cup.x, cup.y);
			// Fix detect
			Sleep(450);
			sprintf(buffer, "*5 %s\\imgs\\ab_d\\wrong.bmp", Util::getCurrentDirectoryA());
			result = (*funcptr)(hw, 0, 0, w, h, buffer);

			if (strcmp(result, "0") != 0) {
#ifdef _DEBUG
				sprintf(buffer, "%s\\AntiBot\\ab0", Util::getCurrentDirectoryA());
				char buf2[MAX_PATH];
				sprintf(buf2, "%s\\AntiBot\\fail", Util::getCurrentDirectoryA());
				MoveFileA(buffer, buf2);
#endif
				return 0;
			}
			//Delete pic files
			TCHAR buffer2[MAX_PATH];
			_swprintf(buffer2, L"%s\\AntiBot\\ab%d", Util::getCurrentDirectoryW(), j);
			//Util::removeDirectory(buffer2);
			return 1;
	}else
		return -1;
}

bool Bot::isWave(HWND hw, int w, int h, imagesearch funcptr) {
	char buffer[1024];
	sprintf(buffer, "*5 %s\\imgs\\wave.bmp", Util::getCurrentDirectoryA());
	char *result = (*funcptr)(hw, 0, 0, w, h, buffer);

	if (strcmp(result, "0") != 0) {
		return true;
	}
	return false;
}

bool Bot::isEvent(HWND hw, int w, int h, imagesearch funcptr) {
	char buffer[1024];
	sprintf(buffer, "*5 %s\\imgs\\event_back.bmp", Util::getCurrentDirectoryA());
	char *result = (*funcptr)(hw, 0, 0, w, h, buffer);

	if (strcmp(result, "0") != 0) {
		return true;
	}
	return false;
}

void Bot::cancelWave(HWND hw, int w, int h, imagesearch funcptr) {
	char buffer[1024];
	sprintf(buffer, "*5 %s\\imgs\\event_back.bmp", Util::getCurrentDirectoryA());
	char *result = (*funcptr)(hw, 0, 0, w, h, buffer);

	if (strcmp(result, "0") != 0) {
		struct IMAGELOC resXY = Util::getXY(result);

		adbCon->Click(resXY.x, resXY.y);
		Sleep(1000);
		sprintf(buffer, "*5 %s\\imgs\\event_exit.bmp", Util::getCurrentDirectoryA());
		result = (*funcptr)(hw, 0, 0, w, h, buffer);

		if (strcmp(result, "0") != 0) {
			struct IMAGELOC resXY = Util::getXY(result);
			adbCon->Click(resXY.x, resXY.y);
		}
	}
}

void Bot::scanCastle(HWND hw, int w, int h, imagesearch funcptr) {
	//Castle structure, [0] = bottom
	//int castle[4];
	adbCon->Click(20, 410);
	Sleep(1000);
	int cLoc = 0;
	char buffer[1024];
	char *result;
	for (int i = 0; i < NUM_CASTLE_BASE; i++) {
		sprintf(buffer, "*5 %s\\imgs\\castle\\b_%d.bmp", Util::getCurrentDirectoryA(), i);
		result = (*funcptr)(hw, 0, 0, w, h, buffer);

		if (strcmp(result, "0") != 0) {
			Castle[cLoc] = i;
			cLoc++;
			struct IMAGELOC resXY = Util::getXY(result);

			adbCon->Click(resXY.x, resXY.y);
			Sleep(500);
			for (int i2 = 0; i2 < NUM_CASTLE_PARTS; i2++) {
				sprintf(buffer, "*5 %s\\imgs\\castle\\c_%d.bmp", Util::getCurrentDirectoryA(), i2);
				result = (*funcptr)(hw, 0, 0, w, h, buffer);

				if (strcmp(result, "0") != 0) {
					Castle[cLoc] = i2;
					cLoc++;
					if (cLoc > 3) {
						sprintf(buffer, "Found %d castle parts.\n", cLoc);
						Util::AppendTextA(Util::getMainWindow(this), buffer, ID_CONSOLE);
						adbCon->Click(20, 410);
						Sleep(1000);
						return;
					}
				}
			}
		}
	}
	sprintf(buffer, "Found %d castle parts.\n", cLoc);
	Util::AppendTextA(Util::getMainWindow(this), buffer, ID_CONSOLE);
	adbCon->Click(20, 410);
	Sleep(1000);
}

int Bot::getCastlePartAtIndex(int index) {
	if (index < 0 || index > 3)
		return 0;
	return Castle[index];
}

void Bot::setPaused(bool p) {
	isPaused = p;
}

bool Bot::getPaused() {
	return isPaused;
}