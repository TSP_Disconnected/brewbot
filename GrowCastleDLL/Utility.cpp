#include "Utility.h"
#include <fstream>
#include <atlstr.h>
#include <Richedit.h>
#include <vector>
#include <TlHelp32.h>
#include <Wincrypt.h>

#include "cJSON.h"
#include "inc/curl/curl.h"

#define BUFSIZE 1024
#define MD5LEN  16

size_t Util::writefunc(void *ptr, size_t size, size_t nmemb, struct string *s)
{
	size_t new_len = s->len + size*nmemb;
	s->ptr = (char *)realloc(s->ptr, new_len + 1);
	if (s->ptr == NULL) {
		fprintf(stderr, "realloc() failed\n");
		exit(EXIT_FAILURE);
	}
	memcpy(s->ptr + s->len, ptr, size*nmemb);
	s->ptr[new_len] = '\0';
	s->len = new_len;

	return size*nmemb;
}

void Util::init_string(struct string *s) {
	s->len = 0;
	s->ptr = (char *)malloc(s->len + 1);
	if (s->ptr == NULL) {
		fprintf(stderr, "malloc() failed\n");
		exit(EXIT_FAILURE);
	}
	s->ptr[0] = '\0';
}

TCHAR * Util::getCurrentDirectoryW()
{
	TCHAR currentDirectory[4096];
	GetCurrentDirectory(4096, currentDirectory);
	return (TCHAR *)currentDirectory;
}

char * Util::getCurrentDirectoryA()
{
	char currentDirectory[4096];
	GetCurrentDirectoryA(4096, currentDirectory);
	return (char *)currentDirectory;
}

IMAGELOC2 Util::getDiamondXY(char *result) {
	char ** res = NULL;
	char *  p = strtok(result, "|");
	int n_spaces = 0;


	/* split string and append tokens to 'res' */

	while (p) {
		res = (char**)realloc(res, sizeof(char*) * ++n_spaces);

		if (res == NULL)
			exit(-1); /* memory allocation failed */

		res[n_spaces - 1] = p;

		p = strtok(NULL, "|");
	}

	/* realloc one extra element for the last NULL */

	res = (char**)realloc(res, sizeof(char*) * (n_spaces + 1));
	res[n_spaces] = 0;
	int mod = 0;
	struct IMAGELOC2 ret;
	int numFound = 0;
	for (int i = 0; i < (n_spaces)/2; i++) {
		if (numFound == 5)
			break;
		int x = atoi(res[i * 2]);
		int y = atoi(res[i *2 +1]);

		if (x >= ret.x[i + mod - 1] && x - ret.x[i + mod - 1] < 5) {
			mod -= 1;
			continue;
		}

		ret.x[i + mod] = x;
		ret.y[i + mod] = y;
		numFound++;
		if (numFound > 5)
			MessageBoxA(NULL, result, result, NULL);
	}
	
	ret.numFound = numFound;
	free(res);
	return ret;
}

int Util::ClickImageWait(char* tolerance, char* imagePath, HWND hw, imagesearch funcptr, Bot* bot, ADB* adbCon, int w = 860, int h = 732) {
	char buffer[MAX_PATH];
	sprintf(buffer, "%s %s\\%s", tolerance, Util::getCurrentDirectoryA(), imagePath);
	char* result = (*funcptr)(hw, 0, 0, w, h, buffer);
	if (strcmp(result, "0") != 0) {
		struct IMAGELOC resXY = Util::getXY(result);
		adbCon->Click(resXY.x, resXY.y);
		return true;
	}
	else {
		DWORD lastTick = bot->getTicks();
		while (strcmp(result, "0") == 0) {
			if (bot->getTicks() - lastTick >= 5)
				break;
			result = (*funcptr)(hw, 0, 0, w, h, buffer);

			if (strcmp(result, "0") != 0) {
				struct IMAGELOC resXY = Util::getXY(result);
				adbCon->Click(resXY.x, resXY.y);
				return true;
			}
			Sleep(10);
		}
	}
	return false;
}

int Util::ClickImage(char* tolerance, char* imagePath, HWND hw, imagesearch funcptr, Bot* bot, ADB* adbCon, int w, int h) {
	char buffer[MAX_PATH];
	sprintf(buffer, "%s %s\\%s", tolerance, Util::getCurrentDirectoryA(), imagePath);
	char* result = (*funcptr)(hw, 0, 0, w, h, buffer);
	if (strcmp(result, "0") != 0) {
		struct IMAGELOC resXY = Util::getXY(result);
		adbCon->Click(resXY.x, resXY.y);
		return true;
	}
	return false;
}

IMAGELOC Util::getXY(char *result) {
	struct IMAGELOC ret;

	char ** res = NULL;
	char *  p = strtok(result, "|");
	int n_spaces = 0;


	/* split string and append tokens to 'res' */

	while (p) {
		res = (char**)realloc(res, sizeof(char*) * ++n_spaces);

		if (res == NULL)
			exit(-1); /* memory allocation failed */

		res[n_spaces - 1] = p;

		p = strtok(NULL, "|");
	}

	/* realloc one extra element for the last NULL */

	res = (char**)realloc(res, sizeof(char*) * (n_spaces + 1));
	res[n_spaces] = 0;
	//TODO: Get random loc in square
	int x = atoi(res[1]);
	int y = atoi(res[2]);
	int width = atoi(res[3]);
	int height = atoi(res[4]);

	int centerX = ((x + width) - x) / 2;
	int centerY = ((y + height) - y) / 2;

	ret.x = x + centerX;
	ret.y = y + centerY;

	free(res);

	return ret;
}

void Util::CaptureScreen(HWND window, const char * filename)
{
	// get screen rectangle
	RECT windowRect;
	GetWindowRect(window, &windowRect);

	// bitmap dimensions
	int bitmap_dx = windowRect.right - windowRect.left;
	int bitmap_dy = windowRect.bottom - windowRect.top;

	// create file
	std::ofstream file(filename, std::ios::binary);
	if (!file) return;

	// save bitmap file headers
	BITMAPFILEHEADER fileHeader;
	BITMAPINFOHEADER infoHeader;

	fileHeader.bfType = 0x4d42;
	fileHeader.bfSize = 0;
	fileHeader.bfReserved1 = 0;
	fileHeader.bfReserved2 = 0;
	fileHeader.bfOffBits = sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER);

	infoHeader.biSize = sizeof(infoHeader);
	infoHeader.biWidth = bitmap_dx;
	infoHeader.biHeight = bitmap_dy;
	infoHeader.biPlanes = 1;
	infoHeader.biBitCount = 24;
	infoHeader.biCompression = BI_RGB;
	infoHeader.biSizeImage = 0;
	infoHeader.biXPelsPerMeter = 0;
	infoHeader.biYPelsPerMeter = 0;
	infoHeader.biClrUsed = 0;
	infoHeader.biClrImportant = 0;

	file.write((char*)&fileHeader, sizeof(fileHeader));
	file.write((char*)&infoHeader, sizeof(infoHeader));

	// dibsection information
	BITMAPINFO info;
	info.bmiHeader = infoHeader;

	// ------------------
	// THE IMPORTANT CODE
	// ------------------
	// create a dibsection and blit the window contents to the bitmap
	HDC winDC = GetDC(window);
	if (winDC == NULL)
		MessageBoxA(NULL, "ERROR1", "ERROR1", NULL);
	//HDC winDC = CreateCompatibleDC(GetWindowDC(window));
	//PrintWindow(window, winDC, NULL);
	HDC memDC = CreateCompatibleDC(winDC);
	if (memDC == NULL)
		MessageBoxA(NULL, "ERROR2", "ERROR1", NULL);
	BYTE* memory = 0;
	HBITMAP bitmap = CreateDIBSection(winDC, &info, DIB_RGB_COLORS, (void**)&memory, 0, 0);
	SelectObject(memDC, bitmap);
	BitBlt(memDC, 0, 0, bitmap_dx, bitmap_dy, winDC, 0, 0, SRCCOPY);
	DeleteDC(memDC);
	ReleaseDC(window, winDC);

	// save dibsection data
	int bytes = (((24 * bitmap_dx + 31) & (~31)) / 8)*bitmap_dy;
	file.write((char *)memory, bytes);

	// HA HA, forgot paste in the DeleteObject lol, happy now ;)?
	DeleteObject(bitmap);
}

void Util::AppendText(const HWND & hwnd, TCHAR * newText, int item)
{
	// get edit control from dialog
	HWND hwndOutput = GetDlgItem(hwnd, item);

	// get the current selection
	DWORD StartPos, EndPos;
	SendMessage(hwndOutput, EM_GETSEL, reinterpret_cast<WPARAM>(&StartPos), reinterpret_cast<WPARAM>(&EndPos));

	// move the caret to the end of the text
	int outLength = GetWindowTextLength(hwndOutput);
	SendMessage(hwndOutput, EM_SETSEL, outLength, outLength);

	// insert the text at the new caret position
	SendMessage(hwndOutput, EM_REPLACESEL, TRUE, reinterpret_cast<LPARAM>(newText));

	// restore the previous selection
	//SendMessage(hwndOutput, EM_SETSEL, StartPos, EndPos);

	int line = SendMessage(hwndOutput, EM_LINEFROMCHAR, -1, NULL);
	if (line > 60) {
		int lLen = SendMessage(hwndOutput, EM_LINELENGTH, 0, NULL);
		SendMessage(hwndOutput, EM_SETSEL, 0, lLen + 2);
		SendMessage(hwndOutput, EM_REPLACESEL, TRUE, reinterpret_cast<LPARAM>(L""));
		outLength = GetWindowTextLength(hwndOutput);
		SendMessage(hwndOutput, EM_SETSEL, outLength, outLength);
		SendMessage(hwndOutput, EM_SCROLLCARET, NULL, NULL);
	}
}

void Util::AppendTextA(const HWND & hwnd, char * newText, int item)
{
	// get edit control from dialog
	HWND hwndOutput = GetDlgItem(hwnd, item);

	// get the current selection
	DWORD StartPos, EndPos;
	SendMessage(hwndOutput, EM_GETSEL, reinterpret_cast<WPARAM>(&StartPos), reinterpret_cast<WPARAM>(&EndPos));

	// move the caret to the end of the text
	int outLength = GetWindowTextLength(hwndOutput);
	SendMessage(hwndOutput, EM_SETSEL, outLength, outLength);

	// insert the text at the new caret position
	TCHAR txt[512];
	USES_CONVERSION;
	_tcscpy(txt, A2T(newText));
	SendMessage(hwndOutput, EM_REPLACESEL, TRUE, reinterpret_cast<LPARAM>(txt));

	// restore the previous selection
	//SendMessage(hwndOutput, EM_SETSEL, StartPos, EndPos);

	int line = SendMessage(hwndOutput, EM_LINEFROMCHAR, -1, NULL);
	if (line > 60) {
		int lLen = SendMessage(hwndOutput, EM_LINELENGTH, 0, NULL);
		SendMessage(hwndOutput, EM_SETSEL, 0, lLen + 2);
		SendMessage(hwndOutput, EM_REPLACESEL, TRUE, reinterpret_cast<LPARAM>(L""));
		outLength = GetWindowTextLength(hwndOutput);
		SendMessage(hwndOutput, EM_SETSEL, outLength, outLength);
		SendMessage(hwndOutput, EM_SCROLLCARET, NULL, NULL);
	}
}

void Util::setRichStyle(int richId, COLORREF color, HWND hWnd)
{
	CHARFORMAT cf;
	HWND richC = GetDlgItem(hWnd, richId);
	SendMessage(richC, EM_GETCHARFORMAT, SCF_SELECTION, ((LPARAM)&cf));
	cf.cbSize = sizeof(cf);
	cf.dwMask = CFM_COLOR;
	cf.dwEffects = 0;
	cf.crTextColor = color;
	SendMessage(richC, EM_SETCHARFORMAT, SCF_SELECTION, ((LPARAM)&cf));
}

HWND Util::getBluestacks()
{
	HWND hWnd = FindWindow(NULL, TEXT("BlueStacks Android PluginAndroid"));
	return hWnd;
}

HWND Util::getMainWindow(Bot* bot)
{
	return bot->getMainWindow();
}

bool Util::startBluestacks(HWND hWnd)
{
	AppendText(hWnd, L"Starting BlueStacks!\r\n", ID_CONSOLE);
	HKEY hKey, fKey;
	long lKey = RegOpenKeyExW(HKEY_LOCAL_MACHINE, L"SOFTWARE\\BlueStacks", NULL, KEY_READ, &hKey);
	if (lKey == ERROR_FILE_NOT_FOUND) {
		AppendText(hWnd, L"Please install BlueStacks!\r\n", ID_CONSOLE);
		return false;
	}
	long lKey2 = RegOpenKeyExW(HKEY_LOCAL_MACHINE, L"SOFTWARE\\BlueStacks\\Guests\\Android\\FrameBuffer\\0", NULL, KEY_ALL_ACCESS, &fKey);
	if (lKey2 == ERROR_FILE_NOT_FOUND) {
		AppendText(hWnd, L"Please install BlueStacks!\r\n", ID_CONSOLE);
		return false;
	}
	DWORD gH(0), gW(0), wH(0), wW(0);
	DWORD dwBufferSz(sizeof(DWORD));
	ULONG lPath = RegQueryValueExW(fKey, L"GuestWidth", NULL, NULL, reinterpret_cast<LPBYTE>(&gW), &dwBufferSz);
	if (lPath == ERROR_SUCCESS) {
		if (gW != 860) {
			DWORD val = 860;
			LSTATUS ls = RegSetValueExW(fKey, L"GuestWidth", NULL, REG_DWORD, (const BYTE*)&val, sizeof(DWORD));
			if (ls != ERROR_SUCCESS) {
				AppendText(hWnd, L"Error setting BlueStacks width and height!\r\n", ID_CONSOLE);
			}
		}
	}
	lPath = RegQueryValueExW(fKey, L"GuestHeight", NULL, NULL, reinterpret_cast<LPBYTE>(&gH), &dwBufferSz);
	if (lPath == ERROR_SUCCESS) {
		if (gH != 732) {
			DWORD val = 732;
			LSTATUS ls = RegSetValueExW(fKey, L"GuestHeight", NULL, REG_DWORD, (const BYTE*)&val, sizeof(DWORD));
			if (ls != ERROR_SUCCESS) {
				AppendText(hWnd, L"Error setting BlueStacks width and height!\r\n", ID_CONSOLE);
			}
		}
	}
	lPath = RegQueryValueExW(fKey, L"WindowHeight", NULL, NULL, reinterpret_cast<LPBYTE>(&wH), &dwBufferSz);
	if (lPath == ERROR_SUCCESS) {
		if (wH != 732) {
			DWORD val = 732;
			LSTATUS ls = RegSetValueExW(fKey, L"WindowHeight", NULL, REG_DWORD, (const BYTE*)&val, sizeof(DWORD));
			if (ls != ERROR_SUCCESS) {
				AppendText(hWnd, L"Error setting BlueStacks width and height!\r\n", ID_CONSOLE);
			}
		}
	}
	lPath = RegQueryValueExW(fKey, L"WindowWidth", NULL, NULL, reinterpret_cast<LPBYTE>(&wW), &dwBufferSz);
	if (lPath == ERROR_SUCCESS) {
		if (wW != 860) {
			DWORD val = 860;
			LSTATUS ls = RegSetValueExW(fKey, L"WindowWidth", NULL, REG_DWORD, (const BYTE*)&val, sizeof(DWORD));
			if (ls != ERROR_SUCCESS) {
				AppendText(hWnd, L"Error setting BlueStacks width and height!\r\n", ID_CONSOLE);
			}
		}
	}
	RegCloseKey(fKey);
	WCHAR szBuffer[512];
	DWORD dwBufferSize = sizeof(szBuffer);
	lPath = RegQueryValueExW(hKey, L"InstallDir", NULL, NULL, (LPBYTE)szBuffer, &dwBufferSize);
	if (lPath == ERROR_SUCCESS) {
		PROCESS_INFORMATION ProcessInfo;

		STARTUPINFO StartupInfo;

		ZeroMemory(&StartupInfo, sizeof(StartupInfo));
		StartupInfo.cb = sizeof(StartupInfo);
		wchar_t buffer[512];
		//HD-StartLauncher = bluestacks 1
		int len = swprintf(buffer, 512, L"\"%sHD-Frontend.exe\" Android", szBuffer);
		if (CreateProcessW(NULL, buffer,
			NULL, NULL, FALSE, NULL, NULL,
			NULL, &StartupInfo, &ProcessInfo))
		{
			CloseHandle(ProcessInfo.hThread);
			CloseHandle(ProcessInfo.hProcess);
			while (!getBluestacks())
				Sleep(100);
			Sleep(5000);
			/*RECT bsSize;
			GetWindowRect(getBluestacks(), &bsSize);
			LONG winL = GetWindowLong(getBluestacks(), GWL_STYLE);
			SetWindowLong(getBluestacks(), GWL_STYLE, winL | WS_CAPTION | WS_SYSMENU);
			int capSize = GetSystemMetrics(SM_CYCAPTION);
			SetWindowPos(getBluestacks(), NULL, bsSize.left, bsSize.top, bsSize.right, bsSize.bottom + capSize, SWP_ASYNCWINDOWPOS);
			/*while (!initADB()) {
			connectADB();
			Sleep(100);
			}
			sendADB("wm density 160", sizeof("wm density 160"));
			Sleep(1000);
			sendADB("wm size reset", sizeof("wm size reset"));
			Sleep(1000);
			sendADB("reboot", sizeof("reboot"));
			Sleep(1000);
			disconnectADB();*/
			
		}
		else {
			RegCloseKey(hKey);
			return false;
		}
	}
	RegCloseKey(hKey);
	return true;
}

bool Util::killBluestacks2(HWND hWnd)
{
	PROCESSENTRY32 pe32;
	HANDLE hProcessSnap = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, NULL);
	if (hProcessSnap == INVALID_HANDLE_VALUE) {
		AppendText(hWnd, L"Failed to start BlueStacks2. Couldn't retrieve process list![1]\r\n", ID_CONSOLE);
		return false;
	}
	pe32.dwSize = sizeof(PROCESSENTRY32);
	if (!Process32First(hProcessSnap, &pe32)) {
		CloseHandle(hProcessSnap);
		AppendText(hWnd, L"Failed to start BlueStacks2. Couldn't retrieve process list![2]\r\n", ID_CONSOLE);
		return false;
	}
	do {
		if ((lstrcmpW(pe32.szExeFile, L"HD-Agent.exe") == 0) || (lstrcmpW(pe32.szExeFile, L"Bluestacks.exe") == 0)
			|| (lstrcmpW(pe32.szExeFile, L"HD-LogRotatorService.exe") == 0) || (lstrcmpW(pe32.szExeFile, L"HD-Plus-Service.exe") == 0)
			|| (lstrcmpW(pe32.szExeFile, L"HD-Frontend.exe") == 0)) {
			AppendText(hWnd, pe32.szExeFile, ID_CONSOLE);
			AppendText(hWnd, L" -- Closing BlueStacks2...\r\n", ID_CONSOLE);
			HANDLE hProc = OpenProcess(PROCESS_TERMINATE, FALSE, pe32.th32ProcessID);
			if (hProc == NULL)
				AppendText(hWnd, L"Failed to close process![1]\r\n", ID_CONSOLE);
			else {
				if (!TerminateProcess(hProc, 1))
					AppendText(hWnd, L"Failed to close process![2]\r\n", ID_CONSOLE);
				CloseHandle(hProc);
				Sleep(500);
			}
		}
	} while (Process32Next(hProcessSnap, &pe32));
	CloseHandle(hProcessSnap);
	return true;
}

void Util::removeDirectory(LPCTSTR dir) // Fully qualified name of the directory being deleted, without trailing backslash
{
	SHFILEOPSTRUCT file_op = {
		NULL,
		FO_DELETE,
		dir,
		L"",
		FOF_NOCONFIRMATION |
		FOF_NOERRORUI |
		FOF_SILENT,
		false,
		0,
		L"" };
	SHFileOperation(&file_op);
}

IMAGELOC Util::getCup(char* folder, imageSearchExM funcptr, imageSearchEx pImageSearchEx) {
	char buffer[1024];
	char buffer2[1024];
	char dest[1024];
	//char* result;
	sprintf(buffer, "*10 %s\\imgs\\d_container.bmp", Util::getCurrentDirectoryA());
	IMAGELOC2 lastLoc;
	IMAGELOC3 con1;
	IMAGELOC3 con2;
	IMAGELOC3 con3;
	IMAGELOC3 con4;
	IMAGELOC3 con5;
	std::vector<IMAGELOC3> container;
	int dContainer = 0;
	bool settled = false;
	lastLoc.x[0] = -1;
	bool moveStarted = false;
	bool conSettled[5] = { false, false, false, false, false };
	for (int i = 0; i < 100; i++) {
		sprintf(dest, "%s\\AntiBot\\%s\\snap%d.bmp", Util::getCurrentDirectoryA(), folder, i);
		sprintf(buffer, "*7 %s\\imgs\\ab_d\\ab_start_btn.bmp", Util::getCurrentDirectoryA());
		char* result2 = (*pImageSearchEx)(0, 0, 900, 900, buffer, dest);
		if (strlen(result2) > 3)
			continue;
		sprintf(buffer, "*7 %s\\imgs\\d_container.bmp", Util::getCurrentDirectoryA());
		std::string result = (*funcptr)(0, 0, 900, 900, buffer, dest);
		int var = 8;
		int numFound = 0;
		if (result.length() > 4) {
			IMAGELOC2 ds = Util::getDiamondXY(&result[0u]);
			numFound = ds.numFound;
		}
		result.clear();
		while (numFound < 5) { //###|###|###|###|###|###|###|###|
			sprintf(buffer, "*%d %s\\imgs\\d_container.bmp", var, Util::getCurrentDirectoryA());
			result = (*funcptr)(0, 0, 900, 900, buffer, dest);
			if (result.length() > 35) {
				IMAGELOC2 ds = Util::getDiamondXY(&result[0u]);
				numFound = ds.numFound;
			}
			var++;
		}
		result = (*funcptr)(0, 0, 900, 900, buffer, dest);
		if (result.length() > 4) {
			IMAGELOC2 ds = Util::getDiamondXY(&result[0u]);
			if (lastLoc.x[0] == -1) {
				//Find the diamond
				sprintf(buffer2, "*7 %s\\imgs\\ab_d.bmp", Util::getCurrentDirectoryA());
				char* result2 = (*pImageSearchEx)(0, 0, 900, 900, buffer2, dest);
				IMAGELOC dloc = Util::getXY(result2);
				if (dloc.x > 175 && dloc.x < 220 && dloc.y > 440 && dloc.y < 480) {
					dContainer = 1;
				}else if (dloc.x > 280 && dloc.x < 330 && dloc.y > 440 && dloc.y < 480) {
					dContainer = 2;
				}
				else if (dloc.x > 375 && dloc.x < 425 && dloc.y > 440 && dloc.y < 480) {
					dContainer = 3;
				}
				else if (dloc.x > 475 && dloc.x < 525 && dloc.y > 440 && dloc.y < 480) {
					dContainer = 4;
				}
				else if (dloc.x > 575 && dloc.x < 625 && dloc.y > 440 && dloc.y < 480) {
					dContainer = 5;
				}
				else {
					MessageBoxA(NULL, "Could not find the diamond!", "Oops!", MB_ICONERROR);
				}
			}
			if (settled) {
				bool conused[5] = { false, false, false, false, false };
				for (int i2 = 0; i2 < 5; i2++) {
					double dist = -1;
					int con = -1;
					bool thisSettled = false;
					for (int i4 = 0; i4 < 5; i4++) {
						if (conSettled[i4]) {
							if ((ds.x[i2] == container.at(i4).x && ds.y[i2] == container.at(i4).y) || (abs(ds.x[i2] - container.at(i4).x) < 5 && abs(ds.y[i2] - container.at(i4).y) < 5)) {
								thisSettled = true;
								conused[i4] = true;
							}
						}
					}
					if (thisSettled)
						continue;
					for (int i3 = 0; i3 < container.size(); i3++) {
						if (conused[i3] || ds.x < 0 || ds.y < 0 || conSettled[i3]) {
							/*if (conSettled[i3]) {
								if (ds.x[i2] == container.at(i3).x && ds.y[i2] == container.at(i3).y) {
									thisSettled = true;
									con = i3;
									break;
								}
							}*/
							continue;
						}
						const double dx = container.at(i3).x - ds.x[i2];
						const double dy = container.at(i3).y - ds.y[i2];
						const double distance = sqrt(dx * dx + dy * dy);
						if (dist == -1) {
							dist = distance;
							con = i3;
						}
						else if (distance == 0.0000) {
							dist = distance;
							con = i3;
							break;
						}
						else if (distance < dist){/*  && 
							(((container.at(i3).x > ds.x[i2]) && container.at(i3).xDirection == DIRECTION_RIGHT) || 
								((container.at(i3).x < ds.x[i2]) && container.at(i3).xDirection == DIRECTION_LEFT) || (container.at(i3).xDirection == DIRECTION_UNDEFINED)) &&
							(((container.at(i3).y > ds.y[i2]) && container.at(i3).yDirection == DIRECTION_DOWN) ||
								((container.at(i3).y < ds.y[i2]) && container.at(i3).yDirection == DIRECTION_UP) || (container.at(i3).yDirection == DIRECTION_UNDEFINED))) {*/
							dist = distance;
							con = i3;
						}
						
					}
					if (ds.x[i2] < 0 || ds.y[i2] < 0)
						continue;
					if (con == -1)
						continue;
					if (thisSettled) {
						conused[con] = true;
						continue;
					}
					const double dx = container.at(con).x - ds.x[i2];
					const double dy = container.at(con).y - ds.y[i2];
					const double distance = sqrt(dx * dx + dy * dy);
					if ((container.at(con).y > 420) || (container.at(con).y < 416)) {
						moveStarted = true;
					}
					if (moveStarted && ds.y[i2] == 417)
						conSettled[con] = true;
					if (con == 0 && i > 93) {
						if (true == false)
							MessageBoxA(NULL, dest, dest, NULL);
						container.at(con).x = ds.x[i2];
						container.at(con).y = ds.y[i2];
					}
					else {
						if (ds.y[i2] > 425 || ds.y[i2] < 410) {
							if ((ds.x[i2] < container.at(con).x) && container.at(con).xDirection == -1)
								container.at(con).xDirection = DIRECTION_LEFT;
							else if(container.at(con).xDirection == -1)
								container.at(con).xDirection = DIRECTION_RIGHT;
							if ((ds.y[i2] < 410) && container.at(con).yDirection == -1)
								container.at(con).yDirection = DIRECTION_UP;
							else if (container.at(con).yDirection == -1)
								container.at(con).yDirection = DIRECTION_DOWN;
						}
						container.at(con).x = ds.x[i2];
						container.at(con).y = ds.y[i2];
					}
					conused[con] = true;
				}
			}
			else {
				settled = true;
				for (int i2 = 0; i2 < 5; i2++) {
					if (ds.y[i2] < 416 || ds.y[i2] > 420) {
						settled = false;
						break;
					}
				}
				if (settled == true) {
					con1.x = ds.x[0];
					con1.y = ds.y[0];
					con1.xDirection = -1;
					con1.yDirection = -1;
					con2.x = ds.x[1];
					con2.y = ds.y[1];
					con2.xDirection = -1;
					con2.yDirection = -1;
					con3.x = ds.x[2];
					con3.y = ds.y[2];
					con3.xDirection = -1;
					con3.yDirection = -1;
					con4.x = ds.x[3];
					con4.y = ds.y[3];
					con4.xDirection = -1;
					con4.yDirection = -1;
					con5.x = ds.x[4];
					con5.y = ds.y[4];
					con5.xDirection = -1;
					con5.yDirection = -1;
					container.push_back(con1);
					container.push_back(con2);
					container.push_back(con3);
					container.push_back(con4);
					container.push_back(con5);
				}
			}
			lastLoc = ds;
		}

	}
	IMAGELOC resss;
	resss.x = container.at(dContainer-1).x;
	resss.y = container.at(dContainer-1).y;
	return resss;
}

void Util::updateImages(HWND hw)
{
	AppendText(hw, L"Checking image directories...\r\n", ID_CONSOLE);
	struct string s;
	init_string(&s);
	//Load bots json
	CURL *curl = curl_easy_init();
	if (curl) {
		curl_easy_setopt(curl, CURLOPT_URL, "https://freebrew.org/bots/gcbot.imgs.json.php");
		int code;
		code = curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, writefunc);
		if (code != CURLE_OK) {
			//fprintf(stderr, "Failed to set writer [%s]\n", errorBuffer);
		}
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, &s);
		int res = curl_easy_perform(curl);
		curl_easy_cleanup(curl);
		cJSON * root = cJSON_Parse(s.ptr);
		cJSON * folderobj = cJSON_GetObjectItem(root, "folders");
		int numFolders = cJSON_GetArraySize(folderobj);
		for (int i = 0; i < numFolders; i++) {
			cJSON * folderArrayitem = cJSON_GetArrayItem(folderobj, i);
			char str[MAX_PATH];
			sprintf(str, "%s\\%s", Util::getCurrentDirectoryA(), cJSON_GetObjectItem(folderArrayitem, "path")->valuestring);
			if (CreateDirectoryA(str, NULL)) {
				TCHAR buf[4096];
				wsprintf(buf, L"Checking folder %d of %d\r\n", i + 1, numFolders);
				AppendText(hw, buf, ID_CONSOLE);
			}
			//create folder
		}
		AppendText(hw, L"Checking image files...\r\n", ID_CONSOLE);
		cJSON * imgobj = cJSON_GetObjectItem(root, "images");
		int numImgs = cJSON_GetArraySize(imgobj);
		for (int i = 0; i < numImgs; i++) {
			cJSON * imgsArrayItem = cJSON_GetArrayItem(imgobj, i);
			char buffer[MAX_PATH];
			sprintf_s(buffer, MAX_PATH - 1, "%s\\%s", Util::getCurrentDirectoryA(), cJSON_GetObjectItem(imgsArrayItem, "path")->valuestring);
			if (PathFileExistsA(buffer)) {
				//Check checksum, then download
				std::string hash = getHash(buffer);
				char* checksum = cJSON_GetObjectItem(imgsArrayItem, "checksum")->valuestring;
				if (strcmp(hash.c_str(), checksum) != 0) {
					//Download file
					TCHAR buf[4096];
					wsprintf(buf, L"Downloading image %d of %d\r\n", i + 1, numImgs);
					AppendText(hw, buf, ID_CONSOLE);
					char urlbuf[MAX_PATH];
					sprintf(urlbuf, "https://freebrew.org/bots/download.php?item=gcbotimg&f=%s", cJSON_GetObjectItem(imgsArrayItem, "path")->valuestring);
					downloadFile(urlbuf, buffer);
				}
			}
			else {
				//Download File
				TCHAR buf[4096];
				wsprintf(buf, L"Downloading image %d of %d\r\n", i + 1, numImgs);
				AppendText(hw, buf, ID_CONSOLE);
				char urlbuf[MAX_PATH];
				sprintf(urlbuf, "https://freebrew.org/bots/download.php?item=gcbotimg&f=%s", cJSON_GetObjectItem(imgsArrayItem, "path")->valuestring);
				downloadFile(urlbuf, buffer);
			}
		}
		cJSON_Delete(root);
	}
}

size_t Util::curlWriteFile(void *ptr, size_t size, size_t nmemb, FILE *stream) {
	size_t written = fwrite(ptr, size, nmemb, stream);
	return written;
}

void Util::downloadFile(char* url, char* file) {
	CURL *curl;
	FILE *fp;
	CURLcode res;
	curl = curl_easy_init();
	if (curl) {
		fp = fopen(file, "wb");
		curl_easy_setopt(curl, CURLOPT_URL, url);
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, curlWriteFile);
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, fp);
		res = curl_easy_perform(curl);
		curl_easy_cleanup(curl);
		fclose(fp);
	}
	else {
		printf("Failed to download update!");
	}
}

std::string Util::getHash(char * filename)
{
	DWORD dwStatus = 0;
	BOOL bResult = FALSE;
	HCRYPTPROV hProv = 0;
	HCRYPTHASH hHash = 0;
	HANDLE hFile = NULL;
	BYTE rgbFile[BUFSIZE];
	DWORD cbRead = 0;
	BYTE rgbHash[MD5LEN];
	DWORD cbHash = 0;
	CHAR rgbDigits[] = "0123456789abcdef";
	// Logic to check usage goes here.

	std::string error = "error";

	hFile = CreateFileA(filename,
		GENERIC_READ,
		FILE_SHARE_READ,
		NULL,
		OPEN_EXISTING,
		FILE_FLAG_SEQUENTIAL_SCAN,
		NULL);

	if (INVALID_HANDLE_VALUE == hFile)
	{
		dwStatus = GetLastError();
		//printf("Error opening file %s\nError: %d\n", filename, dwStatus);
		//printf("ERROR");
		return error;
	}

	// Get handle to the crypto provider
	if (!CryptAcquireContext(&hProv,
		NULL,
		NULL,
		PROV_RSA_FULL,
		CRYPT_VERIFYCONTEXT))
	{
		dwStatus = GetLastError();
		//printf("CryptAcquireContext failed: %d\n", dwStatus);
		//printf("ERROR");
		CloseHandle(hFile);
		return error;
	}

	if (!CryptCreateHash(hProv, CALG_MD5, 0, 0, &hHash))
	{
		dwStatus = GetLastError();
		//printf("CryptAcquireContext failed: %d\n", dwStatus);
		//printf("ERROR");
		CloseHandle(hFile);
		CryptReleaseContext(hProv, 0);
		return error;
	}

	while (bResult = ReadFile(hFile, rgbFile, BUFSIZE,
		&cbRead, NULL))
	{
		if (0 == cbRead)
		{
			break;
		}

		if (!CryptHashData(hHash, rgbFile, cbRead, 0))
		{
			dwStatus = GetLastError();
			//printf("CryptHashData failed: %d\n", dwStatus);
			//printf("ERROR");
			CryptReleaseContext(hProv, 0);
			CryptDestroyHash(hHash);
			CloseHandle(hFile);
			return error;
		}
	}

	if (!bResult)
	{
		dwStatus = GetLastError();
		//printf("ERROR");
		//printf("ERROR");
		CryptReleaseContext(hProv, 0);
		CryptDestroyHash(hHash);
		CloseHandle(hFile);
		return error;
	}

	cbHash = MD5LEN;
	char buffer[MD5LEN + 1];
	std::string hash;
	if (CryptGetHashParam(hHash, HP_HASHVAL, rgbHash, &cbHash, 0))
	{
		//wprintf(L"MD5 hash of file %s is: ", filename);
		//printf("\n");
		for (DWORD i = 0; i < cbHash; i++)
		{
			sprintf(buffer, "%c%c", rgbDigits[rgbHash[i] >> 4],
				rgbDigits[rgbHash[i] & 0xf]);
			hash.append(buffer);
		}
		//printf(hash.c_str());
		//printf("\n");
	}
	else
	{
		dwStatus = GetLastError();
		//printf("CryptGetHashParam failed: %d\n", dwStatus);
		//printf("ERROR");
		return error;
	}

	CryptDestroyHash(hHash);
	CryptReleaseContext(hProv, 0);
	CloseHandle(hFile);
	return hash;
}
