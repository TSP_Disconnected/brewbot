#pragma once

#include <Windows.h>

#ifndef ADB_H_
#define ADB_H_

class ADB {

	HANDLE adbInRead;
	HANDLE adbInWrite;
	HANDLE adbOutRead; 
	HANDLE adbOutWrite;
	PROCESS_INFORMATION adbInfo;

	bool sendADB(char* cmd, int size);
	bool connectADB();
	bool initADB();

public:
	ADB();
	~ADB();
	void Clear();
	void Click(int x, int y);
	void startApp(char* package);
	void killApp(char* package);
	bool isAlive();
	void closeADB();
	void getApp();
	static void disconnectADB();
};

#endif