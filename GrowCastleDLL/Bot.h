#pragma once

#include "ADB.h"

#include <Windows.h>
#include <map>
#include <string.h>

#ifndef BOT_H_
#define BOT_H_

typedef char* (__stdcall *imagesearch)(HWND, int, int, int, int, char *);
typedef char* (__stdcall *imageSearchEx)(int x, int y, int w, int h, char *searchImg, char *destImage);
typedef std::string (__stdcall *imageSearchExM)(int x, int y, int w, int h, char *searchImg, char *destImage);

class Bot {
	ADB* adbCon;
	//Timer values
	int runTimeSec;
	int runTimeMin;
	int runTimeHours;
	DWORD pastTicks = 0;

	HWND mainHWND;

	//Castle structure
	int Castle[4];

	//Bot variables
	bool isPaused = true;

	static std::map<UINT_PTR, Bot*> m_Bot;

	static void addTick(Bot *bot);
	//Timer Callback function
	static VOID CALLBACK runTimeFunc(HWND hwnd, UINT message, UINT idTimer, DWORD dwTime);

public:
	//For timer

	Bot(ADB* adb, HWND wnd);
	Bot(HWND wnd);
	~Bot();

	void setAdbCon(ADB* adb);
	HWND getMainWindow();
	//ADB functions
	void killApp(char* package);
	void getApp();
	//Start run timer
	void startRunTimer();
	DWORD getTicks();
	//Diamonds
	bool maxDiamonds(HWND hw, int w, int h, imagesearch funcptr);
	void clickAbility(HWND hw, int w, int h, imagesearch funcptr);
	int getDiamonds(HWND hw, imagesearch funcptr);
	void watchAdOld(HWND hw, int w, int h, imagesearch funcptr);
	//Watch Ads
	void watchAd(HWND hw, int w, int h, imagesearch funcptr);
	//Upgrade castle functions
	void upgradeCastle(HWND hWnd, int w, int h, imagesearch funcptr, int index);
	//Check for menu
	bool isMenu(HWND hw, int w, int h, imagesearch funcptr);
	//Event functions
	int runDragonWave(HWND hw, int w, int h, imagesearch funcptr, int dragon);
	void clickReplayBtn(HWND hw, int w, int h, imagesearch funcptr);
	void runHell(HWND hw, int w, int h, imagesearch funcptr);
	void runSeason(HWND hw, int w, int h, imagesearch funcptr);
	void clickWaveBtn(HWND hw, int w, int h, imagesearch funcptr, bool skipWaves, int diamonds);
	//functions ran if not on menu
	void clickSpeedBtn(HWND hw, int w, int h, imagesearch funcptr);
	int clickXBtn(int tolerance, HWND hw, int w, int h, imagesearch funcptr);
	int clickSeasonOK(HWND hw, int w, int h, imagesearch funcptr);
	bool getDragonDrop(HWND hw, int w, int h, imagesearch funcptr);
	void clickTongueChest(HWND hw, int w, int h, imagesearch funcptr);
	bool findStrangeMonster(HWND hw, int w, int h, imagesearch funcptr);
	void cancelWave(HWND hw, int w, int h, imagesearch funcptr);
	void scanCastle(HWND hw, int w, int h, imagesearch funcptr);
	int getCastlePartAtIndex(int index);
	void setPaused(bool p);
	bool getPaused();
	//AntiBot
	int solveDiamond(HWND hw, int w, int h, imagesearch funcptr, imageSearchExM f2, imageSearchEx f3);
	bool isWave(HWND hw, int w, int h, imagesearch funcptr);
	bool isEvent(HWND hw, int w, int h, imagesearch funcptr);
};

#endif