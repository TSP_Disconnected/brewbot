// TesseractDLL.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"

#include <tesseract/baseapi.h>
#include <leptonica/allheaders.h>
#include <string>

std::string __stdcall GetTextFromPicture(std::string imgPath, std::string dataPath, std::string dataFile) {
	char *outText;

	tesseract::TessBaseAPI *api = new tesseract::TessBaseAPI();
	// Initialize tesseract-ocr with English, without specifying tessdata path
	if (api->Init(NULL, dataFile.c_str())) {
		fprintf(stderr, "Could not initialize tesseract.\n");
		exit(1);
	}

	// Open input image with leptonica library
	Pix *image = pixRead(imgPath.c_str());
	api->SetImage(image);
	// Get OCR result
	outText = api->GetUTF8Text();
	OutputDebugStringA(outText);

	std::string ret = std::string(outText);

	// Destroy used object and release memory
	api->End();
	//delete[] outText; //TODO Mermory leak due to incompatible tesseract DLL; recompile tesseract for vs2017?
	pixDestroy(&image);

	return ret;
}