// OpenCVTest.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <opencv2/opencv.hpp>
#include <opencv2/tracking.hpp>
#include <opencv2/tracking/tracker.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/core.hpp>
#include <Windows.h>

using namespace cv;
using namespace std;

vector<string> trackerTypes = { "BOOSTING", "MIL", "KCF", "TLD", "MEDIANFLOW", "GOTURN", "MOSSE", "CSRT" };

std::vector<Rect> matchingMethod(Mat& ref)
{
	std::vector<Rect> boxes;

	cv::Mat tpl = cv::imread("cup.bmp");

	cv::Mat gref, gtpl;
	cv::cvtColor(ref, gref, CV_BGR2GRAY);
	cv::cvtColor(tpl, gtpl, CV_BGR2GRAY);

	cv::Mat res(ref.rows - tpl.rows + 1, ref.cols - tpl.cols + 1, CV_32FC1);
	cv::matchTemplate(gref, gtpl, res, CV_TM_CCOEFF_NORMED);
	cv::threshold(res, res, 0.8, 1., CV_THRESH_TOZERO);

	int count = 0;

	while (true)
	{
		double minval, maxval, threshold = 0.8;
		cv::Point minloc, maxloc;
		cv::minMaxLoc(res, &minval, &maxval, &minloc, &maxloc);

		for (int i = 0; i < boxes.size(); i++) {
			if (boxes[i].x == maxloc.x && boxes[i].y == maxloc.y) {
				maxval = 0.0;
				break;
			}
		}

		if (maxval >= threshold)
		{
			count++;
			cv::rectangle(
				ref,
				maxloc,
				cv::Point(maxloc.x + tpl.cols, maxloc.y + tpl.rows),
				CV_RGB(0, 255, 0), 2
			);
			cv::floodFill(res, maxloc, cv::Scalar(0), 0, cv::Scalar(.1), cv::Scalar(1.));
			Rect rect(maxloc.x, maxloc.y, tpl.cols, tpl.rows);
			boxes.push_back(rect);
			if (count == 5)
				break;
		}
		else
			break;
	}

	cv::imshow("reference", ref);

	return boxes;
}

Point MatchingMethod(Mat img, Mat templ, int match_method)
{
	Mat img_display, imgd, tempd, result;
	const char* image_window = "Source Image";
	const char* result_window = "Result window";

	namedWindow(image_window, WINDOW_AUTOSIZE);

	img.copyTo(imgd);
	img.copyTo(img_display);
	templ.copyTo(tempd);

	int result_cols = imgd.cols - tempd.cols + 1;
	int result_rows = imgd.rows - tempd.rows + 1;
	result.create(result_rows, result_cols, CV_32FC1);

	cv::Mat gref, gtpl;
	cv::cvtColor(imgd, gref, CV_BGR2GRAY);
	cv::cvtColor(tempd, gtpl, CV_BGR2GRAY);

	matchTemplate(gref, gtpl, result, match_method);
	//normalize(result, result, 0, 1, NORM_MINMAX, -1, Mat());
	cv::threshold(result, result, 0.8, 1., CV_THRESH_TOZERO);
	double minVal; double maxVal; Point minLoc; Point maxLoc;
	Point matchLoc;
	minMaxLoc(result, &minVal, &maxVal, &minLoc, &maxLoc, Mat());
	if (match_method == TM_SQDIFF || match_method == TM_SQDIFF_NORMED)
	{
		matchLoc = minLoc;
		if (minVal != 0.00)
			return Point(-1, -1);
	}
	else
	{
		matchLoc = maxLoc;
		if (maxVal != 1.00)
			return Point(-1, -1);
	}
	rectangle(img_display, matchLoc, Point(matchLoc.x + tempd.cols, matchLoc.y + tempd.rows), Scalar::all(0), 2, 8, 0);
	imshow(image_window, img_display);

	return matchLoc;
}

void fastMatchTemplate(cv::Mat& srca,  // The reference image
	cv::Mat& srcb,  // The template image
	cv::Mat& dst,   // Template matching result
	int maxlevel)   // Number of levels
{
	std::vector<cv::Mat> refs, tpls, results;

	// Build Gaussian pyramid
	cv::buildPyramid(srca, refs, maxlevel);
	cv::buildPyramid(srcb, tpls, maxlevel);

	cv::Mat ref, tpl, res;

	// Process each level
	for (int level = maxlevel; level >= 0; level--)
	{
		ref = refs[level];
		tpl = tpls[level];
		res = cv::Mat::zeros(ref.size() + cv::Size(1, 1) - tpl.size(), CV_32FC1);

		if (level == maxlevel)
		{
			// On the smallest level, just perform regular template matching
			cv::matchTemplate(ref, tpl, res, CV_TM_CCORR_NORMED);
		}
		else
		{
			// On the next layers, template matching is performed on pre-defined 
			// ROI areas.  We define the ROI using the template matching result 
			// from the previous layer.

			cv::Mat mask;
			cv::pyrUp(results.back(), mask);

			cv::Mat mask8u;
			mask.convertTo(mask8u, CV_8U);

			// Find matches from previous layer
			std::vector<std::vector<cv::Point> > contours;
			cv::findContours(mask8u, contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_NONE);

			// Use the contours to define region of interest and 
			// perform template matching on the areas
			for (int i = 0; i < contours.size(); i++)
			{
				cv::Rect r = cv::boundingRect(contours[i]);
				cv::matchTemplate(
					ref(r + (tpl.size() - cv::Size(1, 1))),
					tpl,
					res(r),
					CV_TM_CCORR_NORMED
				);
			}
		}

		// Only keep good matches
		cv::threshold(res, res, 0.80, 1., CV_THRESH_TOZERO);
		results.push_back(res);
	}

	res.copyTo(dst);
}

// create tracker by name
Ptr<Tracker> createTrackerByName(string trackerType)
{
	Ptr<Tracker> tracker;
	if (trackerType == trackerTypes[0])
		tracker = TrackerBoosting::create();
	else if (trackerType == trackerTypes[1])
		tracker = TrackerMIL::create();
	else if (trackerType == trackerTypes[2])
		tracker = TrackerKCF::create();
	else if (trackerType == trackerTypes[3])
		tracker = TrackerTLD::create();
	else if (trackerType == trackerTypes[4])
		tracker = TrackerMedianFlow::create();
	else if (trackerType == trackerTypes[5])
		tracker = TrackerGOTURN::create();
	else if (trackerType == trackerTypes[6])
		tracker = TrackerMOSSE::create();
	else if (trackerType == trackerTypes[7])
		tracker = TrackerCSRT::create();
	else {
		cout << "Incorrect tracker name" << endl;
		cout << "Available trackers are: " << endl;
		for (vector<string>::iterator it = trackerTypes.begin(); it != trackerTypes.end(); ++it)
			std::cout << " " << *it << endl;
	}
	return tracker;
}

// Fill the vector with random colors
void getRandomColors(vector<Scalar>& colors, int numColors)
{
	RNG rng(0);
	for (int i = 0; i < numColors; i++)
		colors.push_back(Scalar(rng.uniform(0, 255), rng.uniform(0, 255), rng.uniform(0, 255)));
}

Mat hwnd2mat(HWND hwnd) {

	HDC hwindowDC, hwindowCompatibleDC;

	int height, width, srcheight, srcwidth;
	HBITMAP hbwindow;
	Mat src;
	BITMAPINFOHEADER  bi;

	hwindowDC = GetDC(hwnd);
	hwindowCompatibleDC = CreateCompatibleDC(hwindowDC);
	SetStretchBltMode(hwindowCompatibleDC, COLORONCOLOR);

	RECT windowsize;    // get the height and width of the screen
	GetClientRect(hwnd, &windowsize);

	srcheight = windowsize.bottom;
	srcwidth = windowsize.right;
	height = windowsize.bottom;  //change this to whatever size you want to resize to
	width = windowsize.right;

	src.create(height, width, CV_8UC4);

	// create a bitmap
	hbwindow = CreateCompatibleBitmap(hwindowDC, width, height);
	bi.biSize = sizeof(BITMAPINFOHEADER);    //http://msdn.microsoft.com/en-us/library/windows/window/dd183402%28v=vs.85%29.aspx
	bi.biWidth = width;
	bi.biHeight = -height;  //this is the line that makes it draw upside down or not
	bi.biPlanes = 1;
	bi.biBitCount = 32;
	bi.biCompression = BI_RGB;
	bi.biSizeImage = 0;
	bi.biXPelsPerMeter = 0;
	bi.biYPelsPerMeter = 0;
	bi.biClrUsed = 0;
	bi.biClrImportant = 0;

	// use the previously created device context with the bitmap
	SelectObject(hwindowCompatibleDC, hbwindow);
	// copy from the window device context to the bitmap device context
	StretchBlt(hwindowCompatibleDC, 0, 0, width, height, hwindowDC, 0, 0, srcwidth, srcheight, SRCCOPY); //change SRCCOPY to NOTSRCCOPY for wacky colors !
	GetDIBits(hwindowCompatibleDC, hbwindow, 0, height, src.data, (BITMAPINFO*)& bi, DIB_RGB_COLORS);  //copy from hwindowCompatibleDC to hbwindow

	// avoid memory leak
	DeleteObject(hbwindow); DeleteDC(hwindowCompatibleDC); ReleaseDC(hwnd, hwindowDC);

	return src;
}

bool NMultipleTemplateMatching(Mat mInput, Mat mTemplate, float Threshold, float Closeness, vector<Point2f>& List_Matches)
{
	Mat mResult;
	Size szTemplate = mTemplate.size();
	Size szTemplateCloseRadius((szTemplate.width / 2) * Closeness, (szTemplate.height / 2) * Closeness);

	matchTemplate(mInput, mTemplate, mResult, TM_CCOEFF_NORMED);
	threshold(mResult, mResult, Threshold, 1.0, THRESH_TOZERO);
	while (true)
	{
		double minval, maxval;
		Point minloc, maxloc;
		minMaxLoc(mResult, &minval, &maxval, &minloc, &maxloc);

		if (maxval >= Threshold)
		{
			List_Matches.push_back(maxloc);
			rectangle(mResult, Point2f(maxloc.x - szTemplateCloseRadius.width, maxloc.y - szTemplateCloseRadius.height), Point2f(maxloc.x + szTemplateCloseRadius.width, maxloc.y + szTemplateCloseRadius.height), Scalar(0), -1);
		}
		else
			break;
	}
	//imshow("reference", mDebug_Bgr);
	return true;
}


int main(int argc, char** argv) {
	Mat mTemplate_Bgr, mTemplate_Gray;
	mTemplate_Bgr = imread("C:\\Users\\Chris\\Documents\\Visual Studio 2015\\Projects\\FreeBrewBot\\Debug\\scripts\\local\\gcbotlua\\imgs\\castle_base_3.bmp", 1);
	imshow("mTemplate_Bgr", mTemplate_Bgr);

	HWND hDesktopWnd;
	hDesktopWnd = FindWindow(NULL, TEXT("BlueStacks Android PluginAndroid"));
	Mat mScreenShot = hwnd2mat(hDesktopWnd);
	Mat mSource_Gray, mResult_Bgr = mScreenShot.clone();


	float Threshold = 0.9;
	float Closeness = 0.9;
	vector<Point2f> List_Matches;

	cvtColor(mScreenShot, mSource_Gray, COLOR_BGR2GRAY);
	cvtColor(mTemplate_Bgr, mTemplate_Gray, COLOR_BGR2GRAY);

	namedWindow("Screen Shot", WINDOW_AUTOSIZE);
	imshow("Screen Shot", mSource_Gray);

	NMultipleTemplateMatching(mSource_Gray, mTemplate_Gray, Threshold, Closeness, List_Matches);

	for (int i = 0; i < List_Matches.size(); i++)
	{
		rectangle(mResult_Bgr, List_Matches[i], Point(List_Matches[i].x + mTemplate_Bgr.cols, List_Matches[i].y + mTemplate_Bgr.rows), Scalar(0, 255, 0), 2);
	}

	imshow("Final Results", mResult_Bgr);

	waitKey(0);

	return 0;

}