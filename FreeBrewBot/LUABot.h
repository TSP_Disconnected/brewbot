#pragma once

#include "stdafx.h"

extern "C" {
#include "lua.h"
#include "lauxlib.h"
#include "lualib.h"
}

class LUABot
{
public:
	LUABot(char* script, HWND mh);
	~LUABot();
	HWND getMainWindow();

private:
	std::vector<int> uiControls;
	HWND mainHwnd;


};

