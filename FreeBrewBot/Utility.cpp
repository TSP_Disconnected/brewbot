#include "stdafx.h"
#include "Utility.h"
#include <Wincrypt.h>

#define BUFSIZE 1024
#define MD5LEN  16


TCHAR * Util::getCurrentDirectoryW()
{
	TCHAR currentDirectory[4096];
	GetCurrentDirectory(4096, currentDirectory);
	return (TCHAR *)currentDirectory;
}

char * Util::getCurrentDirectoryA()
{
	char currentDirectory[4096];
	GetCurrentDirectoryA(4096, currentDirectory);
	return (char*)currentDirectory;
}

COORD Util::GetConsoleCursorPosition(HANDLE hConsoleOutput)
{
	CONSOLE_SCREEN_BUFFER_INFO cbsi;
	if (GetConsoleScreenBufferInfo(hConsoleOutput, &cbsi))
	{
		return cbsi.dwCursorPosition;
	}
	else
	{
		// The function failed. Call GetLastError() for details.
		COORD invalid = { 0, 0 };
		return invalid;
	}
}

std::string Util::getHash(TCHAR * filename)
{
	DWORD dwStatus = 0;
	BOOL bResult = FALSE;
	HCRYPTPROV hProv = 0;
	HCRYPTHASH hHash = 0;
	HANDLE hFile = NULL;
	BYTE rgbFile[BUFSIZE];
	DWORD cbRead = 0;
	BYTE rgbHash[MD5LEN];
	DWORD cbHash = 0;
	CHAR rgbDigits[] = "0123456789abcdef";
	// Logic to check usage goes here.

	std::string error = "error";

	hFile = CreateFile(filename,
		GENERIC_READ,
		FILE_SHARE_READ,
		NULL,
		OPEN_EXISTING,
		FILE_FLAG_SEQUENTIAL_SCAN,
		NULL);

	if (INVALID_HANDLE_VALUE == hFile)
	{
		dwStatus = GetLastError();
		//printf("Error opening file %s\nError: %d\n", filename, dwStatus);
		//printf("ERROR");
		return error;
	}

	// Get handle to the crypto provider
	if (!CryptAcquireContext(&hProv,
		NULL,
		NULL,
		PROV_RSA_FULL,
		CRYPT_VERIFYCONTEXT))
	{
		dwStatus = GetLastError();
		//printf("CryptAcquireContext failed: %d\n", dwStatus);
		//printf("ERROR");
		CloseHandle(hFile);
		return error;
	}

	if (!CryptCreateHash(hProv, CALG_MD5, 0, 0, &hHash))
	{
		dwStatus = GetLastError();
		//printf("CryptAcquireContext failed: %d\n", dwStatus);
		//printf("ERROR");
		CloseHandle(hFile);
		CryptReleaseContext(hProv, 0);
		return error;
	}

	while (bResult = ReadFile(hFile, rgbFile, BUFSIZE,
		&cbRead, NULL))
	{
		if (0 == cbRead)
		{
			break;
		}

		if (!CryptHashData(hHash, rgbFile, cbRead, 0))
		{
			dwStatus = GetLastError();
			//printf("CryptHashData failed: %d\n", dwStatus);
			//printf("ERROR");
			CryptReleaseContext(hProv, 0);
			CryptDestroyHash(hHash);
			CloseHandle(hFile);
			return error;
		}
	}

	if (!bResult)
	{
		dwStatus = GetLastError();
		//printf("ERROR");
		//printf("ERROR");
		CryptReleaseContext(hProv, 0);
		CryptDestroyHash(hHash);
		CloseHandle(hFile);
		return error;
	}

	cbHash = MD5LEN;
	char buffer[MD5LEN + 1];
	std::string hash;
	if (CryptGetHashParam(hHash, HP_HASHVAL, rgbHash, &cbHash, 0))
	{
		//wprintf(L"MD5 hash of file %s is: ", filename);
		//printf("\n");
		for (DWORD i = 0; i < cbHash; i++)
		{
			sprintf(buffer, "%c%c", rgbDigits[rgbHash[i] >> 4],
				rgbDigits[rgbHash[i] & 0xf]);
			hash.append(buffer);
		}
		//printf(hash.c_str());
		//printf("\n");
	}
	else
	{
		dwStatus = GetLastError();
		//printf("CryptGetHashParam failed: %d\n", dwStatus);
		//printf("ERROR");
		return error;
	}

	CryptDestroyHash(hHash);
	CryptReleaseContext(hProv, 0);
	CloseHandle(hFile);
	return hash;
}
