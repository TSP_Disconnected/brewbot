// FreeBrewBot.cpp : Defines the entry point for the application.
//

#include <atlimage.h>

#include "stdafx.h"
#include "FreeBrewBot.h"
#include "dll.h"
#include "Utility.h"
#include "cJSON.h"

#include "inc/curl/curl.h"

#pragma comment(linker,"\"/manifestdependency:type='win32' \
name='Microsoft.Windows.Common-Controls' version='6.0.0.0' \
processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")

#pragma comment(lib, "wininet.lib")


#define MAX_LOADSTRING 100

#define IDC_LOGIN_BTN 1
#define IDC_USER_BOX 2
#define IDC_PASS_BOX 3

// Global Variables:
HINSTANCE hInst;  
HINSTANCE hPInst;
LPWSTR lpCmd;
int nCmd;// current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name

// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);

//Bot things
HANDLE runThread;


struct string {
	char *ptr;
	size_t len;
};

void init_string(struct string *s) {
	s->len = 0;
	s->ptr = (char *)malloc(s->len + 1);
	if (s->ptr == NULL) {
		fprintf(stderr, "malloc() failed\n");
		exit(EXIT_FAILURE);
	}
	s->ptr[0] = '\0';
}

size_t writefunc(void *ptr, size_t size, size_t nmemb, struct string *s)
{
	size_t new_len = s->len + size*nmemb;
	s->ptr = (char *)realloc(s->ptr, new_len + 1);
	if (s->ptr == NULL) {
		fprintf(stderr, "realloc() failed\n");
		exit(EXIT_FAILURE);
	}
	memcpy(s->ptr + s->len, ptr, size*nmemb);
	s->ptr[new_len] = '\0';
	s->len = new_len;

	return size*nmemb;
}


size_t curlWriteFile(void *ptr, size_t size, size_t nmemb, FILE *stream) {
	size_t written = fwrite(ptr, size, nmemb, stream);
	return written;
}

const wchar_t *GetWC(const char *c)
{
	const size_t cSize = strlen(c) + 1;
	wchar_t* wc = new wchar_t[cSize];
	mbstowcs(wc, c, cSize);

	return wc;
}

BOOL WINAPI checkUpdate()
{
	std::string localEXEHash = "NULL";
	std::string coreDLLHash = "NULL";
	std::string lua51Dll = "NULL";
	std::string lua51DLL = "NULL";
	std::string opencvHash = "NULL";
	std::string leptHash = "NULL";
	std::string imgsearchHash = "NULL";
	std::string tiffDll = "NULL";
	std::string jpegDLL = "NULL";
	std::string pngHash = "NULL";
	std::string webpHash = "NULL";
	std::string lzmaHash = "NULL";
	std::string zlibHash = "NULL";

	printf("Checking for updates..");
	if (PathFileExists(L"MainGUI.exe"))
		localEXEHash = Util::getHash(L"MainGUI.exe");
	printf(".");
	if(PathFileExists(L"CoreDLL.dll"))
		coreDLLHash = Util::getHash(L"CoreDLL.dll");
	printf(".");
	if(PathFileExists(L"lua51.dll"))
		lua51Dll = Util::getHash(L"lua51.dll");
	printf(".");
	if(PathFileExists(L"lua5.1.dll"))
		lua51DLL = Util::getHash(L"lua5.1.dll");
	printf(".");
	if(PathFileExists(L"opencv_world412.dll"))
		opencvHash = Util::getHash(L"opencv_world412.dll");
	printf(".");
	if (PathFileExists(L"ImageSearchDLL.dll"))
		imgsearchHash = Util::getHash(L"ImageSearchDLL.dll");
	if (PathFileExists(L"tiff.dll"))
		tiffDll = Util::getHash(L"tiff.dll");
	printf(".");
	if (PathFileExists(L"lzma.dll"))
		lzmaHash = Util::getHash(L"lzma.dll");
	printf(".");
	if (PathFileExists(L"webp.dll"))
		webpHash = Util::getHash(L"webp.dll");
	printf(".");
	if (PathFileExists(L"zlib1.dll"))
		zlibHash = Util::getHash(L"zlib1.dll");
	printf(".");
	if (PathFileExists(L"libpng16.dll"))
		pngHash = Util::getHash(L"libpng16.dll");
	printf(".");
	if (PathFileExists(L"jpeg62.dll"))
		jpegDLL = Util::getHash(L"jpeg62.dll");
	printf(".");
	if (PathFileExists(L"leptonica-1.78.0.dll"))
		leptHash = Util::getHash(L"leptonica-1.78.0.dll");
	printf(".\n");

	struct string s;
	init_string(&s);
	//Load bots json
	CURL *curl = curl_easy_init();
	if (curl) {
		char buffer[4096];
		sprintf(buffer, "https://freebrew.org/bots/ver.json.php?coreV=%s&launchV=%s&lua1=%s&lua2=%s&ocv=%s&lept=%s&imgdll=%s&jpeg=%s&png=%s&lzma=%s&tiff=%s&webp=%s&zlib=%s&t=1", coreDLLHash.c_str(), localEXEHash.c_str(), lua51Dll.c_str(), 
			lua51DLL.c_str(), opencvHash.c_str(), leptHash.c_str(), imgsearchHash.c_str(), jpegDLL.c_str(), pngHash.c_str(), lzmaHash.c_str(), tiffDll.c_str(), webpHash.c_str(), zlibHash.c_str());
		curl_easy_setopt(curl, CURLOPT_URL, buffer);
		int code;
		code = curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, writefunc);
		if (code != CURLE_OK) {
			//fprintf(stderr, "Failed to set writer [%s]\n", errorBuffer);
			return -1;
		}
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, &s);
		int res = curl_easy_perform(curl);
		curl_easy_cleanup(curl);
		cJSON * root = cJSON_Parse(s.ptr);
		bool needsUpdateEXE = cJSON_GetObjectItem(root, "launchEXE")->valueint;
		bool needsUpdateCoreDLL = cJSON_GetObjectItem(root, "coreDLL")->valueint;
		bool needsUpdatelua1DLL = cJSON_GetObjectItem(root, "lua1DLL")->valueint;
		bool needsUpdatelua2DLL = cJSON_GetObjectItem(root, "lua2DLL")->valueint;
		bool needsUpdateocvDLL = cJSON_GetObjectItem(root, "ocvDLL")->valueint;
		bool needsUpdateimgDLL = cJSON_GetObjectItem(root, "imgDLL")->valueint;
		bool needsUpdateleptDLL = cJSON_GetObjectItem(root, "leptDLL")->valueint;
		bool needsUpdatejpegDLL = cJSON_GetObjectItem(root, "jpegDLL")->valueint;
		bool needsUpdatepngDLL = cJSON_GetObjectItem(root, "pngDLL")->valueint;
		bool needsUpdatelzmaDLL = cJSON_GetObjectItem(root, "lzmaDLL")->valueint;
		bool needsUpdatetiffDLL = cJSON_GetObjectItem(root, "tiffDLL")->valueint;
		bool needsUpdatewebpDLL = cJSON_GetObjectItem(root, "webpDLL")->valueint;
		bool needsUpdatezlibDLL = cJSON_GetObjectItem(root, "zlibDLL")->valueint;
		if (needsUpdateEXE) {
			//Open browser to thread
			printf("Downloading new launcher...\n");
			char * exeurl = cJSON_GetObjectItem(root, "launchURL")->valuestring;
			CURL *curl;
			FILE *fp;
			CURLcode res;
			char outfilename[FILENAME_MAX];
			sprintf(outfilename, "MainGUI.new.exe", Util::getCurrentDirectoryA());
			curl = curl_easy_init();
			if (curl) {
				fp = fopen(outfilename, "wb");
				curl_easy_setopt(curl, CURLOPT_URL, exeurl);
				curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, curlWriteFile);
				curl_easy_setopt(curl, CURLOPT_WRITEDATA, fp);
				res = curl_easy_perform(curl);
				// always cleanup
				curl_easy_cleanup(curl);
				fclose(fp);
			}
			else {
				printf("Failed to download update!");
			}
			return -2;
		}
		if (needsUpdateCoreDLL) {
			//Download new core DLL
			printf("Downloading CoreDLL.dll...\n");
			char * dllurl = cJSON_GetObjectItem(root, "coreURL")->valuestring;
			CURL *curl;
			FILE *fp;
			CURLcode res;
			char outfilename[FILENAME_MAX];
			sprintf(outfilename, "CoreDLL.dll", Util::getCurrentDirectoryA());
			curl = curl_easy_init();
			if (curl) {
				fp = fopen(outfilename, "wb");
				curl_easy_setopt(curl, CURLOPT_URL, dllurl);
				curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, curlWriteFile);
				curl_easy_setopt(curl, CURLOPT_WRITEDATA, fp);
				res = curl_easy_perform(curl);
				/* always cleanup */
				curl_easy_cleanup(curl);
				fclose(fp);
			}
			else {
				printf("Failed to download update!");
			}
		}
		if (needsUpdatelua1DLL) {
			//Download new core DLL
			printf("Downloading lua51.dll...\n");
			char* dllurl = cJSON_GetObjectItem(root, "lua1URL")->valuestring;
			CURL* curl;
			FILE* fp;
			CURLcode res;
			char outfilename[FILENAME_MAX];
			sprintf(outfilename, "lua51.dll", Util::getCurrentDirectoryA());
			curl = curl_easy_init();
			if (curl) {
				fp = fopen(outfilename, "wb");
				curl_easy_setopt(curl, CURLOPT_URL, dllurl);
				curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, curlWriteFile);
				curl_easy_setopt(curl, CURLOPT_WRITEDATA, fp);
				res = curl_easy_perform(curl);
				/* always cleanup */
				curl_easy_cleanup(curl);
				fclose(fp);
			}
			else {
				printf("Failed to download update!");
			}
		}
		if (needsUpdatelua2DLL) {
			//Download new core DLL
			printf("Downloading lua5.1.dll...\n");
			char* dllurl = cJSON_GetObjectItem(root, "lua2URL")->valuestring;
			CURL* curl;
			FILE* fp;
			CURLcode res;
			char outfilename[FILENAME_MAX];
			sprintf(outfilename, "lua5.1.dll", Util::getCurrentDirectoryA());
			curl = curl_easy_init();
			if (curl) {
				fp = fopen(outfilename, "wb");
				curl_easy_setopt(curl, CURLOPT_URL, dllurl);
				curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, curlWriteFile);
				curl_easy_setopt(curl, CURLOPT_WRITEDATA, fp);
				res = curl_easy_perform(curl);
				/* always cleanup */
				curl_easy_cleanup(curl);
				fclose(fp);
			}
			else {
				printf("Failed to download update!");
			}
		}
		if (needsUpdateocvDLL) {
			//Download new core DLL
			printf("Downloading opencv_world412.dll...\n");
			char* dllurl = cJSON_GetObjectItem(root, "ocvURL")->valuestring;
			CURL* curl;
			FILE* fp;
			CURLcode res;
			char outfilename[FILENAME_MAX];
			sprintf(outfilename, "opencv_world412.dll", Util::getCurrentDirectoryA());
			curl = curl_easy_init();
			if (curl) {
				fp = fopen(outfilename, "wb");
				curl_easy_setopt(curl, CURLOPT_URL, dllurl);
				curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, curlWriteFile);
				curl_easy_setopt(curl, CURLOPT_WRITEDATA, fp);
				res = curl_easy_perform(curl);
				/* always cleanup */
				curl_easy_cleanup(curl);
				fclose(fp);
			}
			else {
				printf("Failed to download update!");
			}
		}
		if (needsUpdateleptDLL) {
			//Download new core DLL
			printf("Downloading leptonica-1.78.0.dll...\n");
			char* dllurl = cJSON_GetObjectItem(root, "leptURL")->valuestring;
			CURL* curl;
			FILE* fp;
			CURLcode res;
			char outfilename[FILENAME_MAX];
			sprintf(outfilename, "leptonica-1.78.0.dll", Util::getCurrentDirectoryA());
			curl = curl_easy_init();
			if (curl) {
				fp = fopen(outfilename, "wb");
				curl_easy_setopt(curl, CURLOPT_URL, dllurl);
				curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, curlWriteFile);
				curl_easy_setopt(curl, CURLOPT_WRITEDATA, fp);
				res = curl_easy_perform(curl);
				/* always cleanup */
				curl_easy_cleanup(curl);
				fclose(fp);
			}
			else {
				printf("Failed to download update!");
			}
		}
		if (needsUpdateimgDLL) {
			//Download new core DLL
			printf("Downloading ImageSearchDLL.dll...\n");
			char* dllurl = cJSON_GetObjectItem(root, "imgURL")->valuestring;
			CURL* curl;
			FILE* fp;
			CURLcode res;
			char outfilename[FILENAME_MAX];
			sprintf(outfilename, "ImageSearchDLL.dll", Util::getCurrentDirectoryA());
			curl = curl_easy_init();
			if (curl) {
				fp = fopen(outfilename, "wb");
				curl_easy_setopt(curl, CURLOPT_URL, dllurl);
				curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, curlWriteFile);
				curl_easy_setopt(curl, CURLOPT_WRITEDATA, fp);
				res = curl_easy_perform(curl);
				/* always cleanup */
				curl_easy_cleanup(curl);
				fclose(fp);
			}
			else {
				printf("Failed to download update!");
			}
		}
		if (needsUpdatejpegDLL) {
			//Download new core DLL
			printf("Downloading jpeg62.dll...\n");
			char* dllurl = cJSON_GetObjectItem(root, "jpegURL")->valuestring;
			CURL* curl;
			FILE* fp;
			CURLcode res;
			char outfilename[FILENAME_MAX];
			sprintf(outfilename, "jpeg62.dll", Util::getCurrentDirectoryA());
			curl = curl_easy_init();
			if (curl) {
				fp = fopen(outfilename, "wb");
				curl_easy_setopt(curl, CURLOPT_URL, dllurl);
				curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, curlWriteFile);
				curl_easy_setopt(curl, CURLOPT_WRITEDATA, fp);
				res = curl_easy_perform(curl);
				/* always cleanup */
				curl_easy_cleanup(curl);
				fclose(fp);
			}
			else {
				printf("Failed to download update!");
			}
		}
		if (needsUpdatepngDLL) {
			//Download new core DLL
			printf("Downloading libpng16.dll...\n");
			char* dllurl = cJSON_GetObjectItem(root, "pngURL")->valuestring;
			CURL* curl;
			FILE* fp;
			CURLcode res;
			char outfilename[FILENAME_MAX];
			sprintf(outfilename, "libpng16.dll", Util::getCurrentDirectoryA());
			curl = curl_easy_init();
			if (curl) {
				fp = fopen(outfilename, "wb");
				curl_easy_setopt(curl, CURLOPT_URL, dllurl);
				curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, curlWriteFile);
				curl_easy_setopt(curl, CURLOPT_WRITEDATA, fp);
				res = curl_easy_perform(curl);
				/* always cleanup */
				curl_easy_cleanup(curl);
				fclose(fp);
			}
			else {
				printf("Failed to download update!");
			}
		}
		if (needsUpdatelzmaDLL) {
			//Download new core DLL
			printf("Downloading lzma.dll...\n");
			char* dllurl = cJSON_GetObjectItem(root, "lzmaURL")->valuestring;
			CURL* curl;
			FILE* fp;
			CURLcode res;
			char outfilename[FILENAME_MAX];
			sprintf(outfilename, "lzma.dll", Util::getCurrentDirectoryA());
			curl = curl_easy_init();
			if (curl) {
				fp = fopen(outfilename, "wb");
				curl_easy_setopt(curl, CURLOPT_URL, dllurl);
				curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, curlWriteFile);
				curl_easy_setopt(curl, CURLOPT_WRITEDATA, fp);
				res = curl_easy_perform(curl);
				/* always cleanup */
				curl_easy_cleanup(curl);
				fclose(fp);
			}
			else {
				printf("Failed to download update!");
			}
		}
		if (needsUpdatetiffDLL) {
			//Download new core DLL
			printf("Downloading tiff.dll...\n");
			char* dllurl = cJSON_GetObjectItem(root, "tiffURL")->valuestring;
			CURL* curl;
			FILE* fp;
			CURLcode res;
			char outfilename[FILENAME_MAX];
			sprintf(outfilename, "tiff.dll", Util::getCurrentDirectoryA());
			curl = curl_easy_init();
			if (curl) {
				fp = fopen(outfilename, "wb");
				curl_easy_setopt(curl, CURLOPT_URL, dllurl);
				curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, curlWriteFile);
				curl_easy_setopt(curl, CURLOPT_WRITEDATA, fp);
				res = curl_easy_perform(curl);
				/* always cleanup */
				curl_easy_cleanup(curl);
				fclose(fp);
			}
			else {
				printf("Failed to download update!");
			}
		}
		if (needsUpdatewebpDLL) {
			//Download new core DLL
			printf("Downloading webp.dll...\n");
			char* dllurl = cJSON_GetObjectItem(root, "webpURL")->valuestring;
			CURL* curl;
			FILE* fp;
			CURLcode res;
			char outfilename[FILENAME_MAX];
			sprintf(outfilename, "webp.dll", Util::getCurrentDirectoryA());
			curl = curl_easy_init();
			if (curl) {
				fp = fopen(outfilename, "wb");
				curl_easy_setopt(curl, CURLOPT_URL, dllurl);
				curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, curlWriteFile);
				curl_easy_setopt(curl, CURLOPT_WRITEDATA, fp);
				res = curl_easy_perform(curl);
				/* always cleanup */
				curl_easy_cleanup(curl);
				fclose(fp);
			}
			else {
				printf("Failed to download update!");
			}
		}
		if (needsUpdatezlibDLL) {
			//Download new core DLL
			printf("Downloading zlib1.dll...\n");
			char* dllurl = cJSON_GetObjectItem(root, "zlibURL")->valuestring;
			CURL* curl;
			FILE* fp;
			CURLcode res;
			char outfilename[FILENAME_MAX];
			sprintf(outfilename, "zlib1.dll", Util::getCurrentDirectoryA());
			curl = curl_easy_init();
			if (curl) {
				fp = fopen(outfilename, "wb");
				curl_easy_setopt(curl, CURLOPT_URL, dllurl);
				curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, curlWriteFile);
				curl_easy_setopt(curl, CURLOPT_WRITEDATA, fp);
				res = curl_easy_perform(curl);
				/* always cleanup */
				curl_easy_cleanup(curl);
				fclose(fp);
			}
			else {
				printf("Failed to download update!");
			}
		}
		cJSON_Delete(root);
		return 0;
	}
	return -1;
}

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
	_In_opt_ HINSTANCE hPrevInstance,
	_In_ LPWSTR    lpCmdLine,
	_In_ int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	if (!InternetCheckConnection(L"https://freebrew.org/", FLAG_ICC_FORCE_CONNECTION, NULL)) {
		MessageBoxA(NULL, "Please check your internet connection and try again!", "Could not connect to the internet!", NULL);
		return FALSE;
	}

	//Check command line for rename old, rename new, delete old
	if (lstrcmpW(lpCmdLine, L"RO") == 0) {
		//New version is running, rename MainGUI.exe to MainGUI.old.exe
		if (MoveFile(L"MainGUI.exe", L"MainGUI.old.exe")) {
			//Load old version with RN tag
			PROCESS_INFORMATION ProcessInfo;

			STARTUPINFO StartupInfo;

			//CloseHandle(this->adbInfo.hThread);

			ZeroMemory(&StartupInfo, sizeof(StartupInfo));
			StartupInfo.cb = sizeof StartupInfo;
			wchar_t buffer[512];
			int len = swprintf(buffer, 512, L"\"%s\\MainGUI.old.exe\" RN", Util::getCurrentDirectoryW());
			Sleep(1000);
			if (CreateProcessW(0, buffer,
				NULL, NULL, FALSE, CREATE_NO_WINDOW, NULL,
				NULL, &StartupInfo, &ProcessInfo))
			{
				//WaitForSingleObject(ProcessInfo.hProcess, INFINITE);
				CloseHandle(ProcessInfo.hThread);
				CloseHandle(ProcessInfo.hProcess);
			}
			else
			{
				MessageBoxA(NULL, "Update process failed!", "Update Failed", NULL);
			}
			return 0;
		}
		else {
			MessageBoxA(NULL, "Update process failed!", "Update Failed", NULL);
		}
	}
	else if (lstrcmpW(lpCmdLine, L"RN") == 0) {
		//Old version is running, rename MainGUI.new.exe to MainGUI.exe
		if (MoveFile(L"MainGUI.new.exe", L"MainGUI.exe")) {
			//Load new version with DO tag
			PROCESS_INFORMATION ProcessInfo;

			STARTUPINFO StartupInfo;

			//CloseHandle(this->adbInfo.hThread);

			ZeroMemory(&StartupInfo, sizeof(StartupInfo));
			StartupInfo.cb = sizeof StartupInfo;
			wchar_t buffer[512];
			int len = swprintf(buffer, 512, L"\"%s\\MainGUI.exe\" DO", Util::getCurrentDirectoryW());
			Sleep(1000);
			if (CreateProcessW(0, buffer,
				NULL, NULL, FALSE, CREATE_NO_WINDOW, NULL,
				NULL, &StartupInfo, &ProcessInfo))
			{
				//WaitForSingleObject(ProcessInfo.hProcess, INFINITE);
				CloseHandle(ProcessInfo.hThread);
				CloseHandle(ProcessInfo.hProcess);
			}
			else
			{
				MessageBoxA(NULL, "Update process failed!", "Update Failed", NULL);
			}
			return 0;
		}
		else {
			MessageBoxA(NULL, "Update process failed!", "Update Failed", NULL);
		}
	}
	else if (lstrcmpW(lpCmdLine, L"DO") == 0) {
		//New version is running, Delete MainGUI.old.exe
		int retval = ::_tsystem(_T("taskkill /F /T /IM MainGUI.old.exe"));
		Sleep(1000);
		if (!DeleteFile(L"MainGUI.old.exe")) {
			MessageBoxA(NULL, "Could not delete old version!", "Update Error", NULL);
		}
	}
	//Update code
	curl_global_init(CURL_GLOBAL_ALL);
	AllocConsole();
	freopen("CONIN$", "r", stdin);
	freopen("CONOUT$", "w", stdout);
	freopen("CONOUT$", "w", stderr);

	
	BOOL needsUpdate;
	//BOOL needsUpdate = false;
#ifdef _DEBUG
	needsUpdate = false;
#else
	needsUpdate = checkUpdate();
#endif
	if (needsUpdate == -2) {
		//Download EXE from freebrew.org; Exit this program
		printf("Closing...\n");
		curl_global_cleanup();
		Sleep(3000);
		HWND hwnd = GetConsoleWindow();
		FreeConsole();
		PostMessage(hwnd, WM_CLOSE, 0, 0);
		PROCESS_INFORMATION ProcessInfo;

		STARTUPINFO StartupInfo;

		//CloseHandle(this->adbInfo.hThread);

		ZeroMemory(&StartupInfo, sizeof(StartupInfo));
		StartupInfo.cb = sizeof StartupInfo;
		wchar_t buffer[512];
		int len = swprintf(buffer, 512, L"\"%s\\MainGUI.new.exe\" RO", Util::getCurrentDirectoryW());
		Sleep(1000);
		if (CreateProcessW(0, buffer,
			NULL, NULL, FALSE, CREATE_NO_WINDOW, NULL,
			NULL, &StartupInfo, &ProcessInfo))
		{
			//WaitForSingleObject(ProcessInfo.hProcess, INFINITE);
			CloseHandle(ProcessInfo.hThread);
			CloseHandle(ProcessInfo.hProcess);
		}
		else
		{
			MessageBoxA(NULL, "Update process failed!", "Update Failed", NULL);
		}
		return 0;
	}
	HWND hwnd = GetConsoleWindow();
	FreeConsole();
	PostMessage(hwnd, WM_CLOSE, 0, 0);
	curl_global_cleanup();


	// Initialize global strings
	LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadStringW(hInstance, IDC_FREEBREWBOT, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// Perform application initialization:
	if (!InitInstance(hInstance, nCmdShow))
	{
		return FALSE;
	}

	HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_FREEBREWBOT));

	MSG msg;

	hInst = hInstance;
	hPInst = hPrevInstance;
	lpCmd = lpCmdLine;
	nCmd = nCmdShow;

	// Main message loop:
	while (GetMessage(&msg, nullptr, 0, 0))
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return (int)msg.wParam;
}
/*
	//Check internet first
	

	curl_global_init(CURL_GLOBAL_ALL);
	AllocConsole();
	freopen("CONIN$", "r", stdin);
	freopen("CONOUT$", "w", stdout);
	freopen("CONOUT$", "w", stderr);

	printf("Checking for updates..");
	std::string localEXEHash = Util::getHash(L"MainGUI.exe");
	printf(".");
	std::string coreDLLHash = Util::getHash(L"CoreDLL.dll");
	printf(".\n");
	BOOL needsUpdate = checkUpdate(coreDLLHash.c_str(), localEXEHash.c_str());
	//BOOL needsUpdate = false;
#ifdef _DEBUG
	needsUpdate = false;
#endif
	if (needsUpdate == -2) {
		//Download EXE from freebrew.org; Exit this program
		printf("Closing...\n");
		curl_global_cleanup();
		Sleep(3000);
		HWND hwnd = GetConsoleWindow();
		FreeConsole();
		PostMessage(hwnd, WM_CLOSE, 0, 0);
		PROCESS_INFORMATION ProcessInfo;

		STARTUPINFO StartupInfo;

		//CloseHandle(this->adbInfo.hThread);

		ZeroMemory(&StartupInfo, sizeof(StartupInfo));
		StartupInfo.cb = sizeof StartupInfo;
		wchar_t buffer[512];
		int len = swprintf(buffer, 512, L"\"%s\\MainGUI.new.exe\" RO", Util::getCurrentDirectoryW());
		Sleep(1000);
		if (CreateProcessW(0, buffer,
			NULL, NULL, FALSE, CREATE_NO_WINDOW, NULL,
			NULL, &StartupInfo, &ProcessInfo))
		{
			//WaitForSingleObject(ProcessInfo.hProcess, INFINITE);
			CloseHandle(ProcessInfo.hThread);
			CloseHandle(ProcessInfo.hProcess);
		}
		else
		{
			MessageBoxA(NULL, "Update process failed!", "Update Failed", NULL);
		}
		return 0;
	}
	else {
		//Login
	login:
		printf("Enter username: ");
		static HANDLE stdinHandle;
		// Get the IO handles
		stdinHandle = GetStdHandle(STD_INPUT_HANDLE);
		int pos = 0;
		char username[31];
		bool run = true;
		while (run)
		{
			switch (WaitForSingleObject(stdinHandle, INFINITE))
			{
			case(WAIT_TIMEOUT) :
				//cerr << "timeout" << endl;
				break; // return from this function to allow thread to terminate
			case(WAIT_OBJECT_0) :
				if (_kbhit()) // _kbhit() always returns immediately
				{
					int i = _getch();
					//cerr << "key: " << i << endl;
					//printf("%d", i); //To character
					if (i == 13) {
						run = false;
						break;
					}
					else if (i == 8) {
						pos -= 1;
						if (pos < 0) {
							pos = 0;
						}
						else {
							username[pos] = '\0';
							COORD cursor = Util::GetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE));
							cursor.X -= pos + 1;
							SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), cursor);
							for (int i = 0; i < pos + 1; i++)
								printf(" ");
							SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), cursor);
							printf("%s", username);
						}
					}
					else if (pos < 30) {
						username[pos] = (char)i;
						username[pos + 1] = '\0';
						pos++;
						COORD cursor = Util::GetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE));
						cursor.X -= pos - 1;
						SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), cursor);
						printf("%s", username);
					}
				}
				else // some sort of other events , we need to clear it from the queue
				{
					// clear events
					INPUT_RECORD r[512];
					DWORD read;
					ReadConsoleInput(stdinHandle, r, 512, &read); //Not needed
					//cerr << "mouse event" << endl;
				}
								break;
			case(WAIT_FAILED) :
				//cerr << "WAIT_FAILED" << endl;
				break;
			case(WAIT_ABANDONED) :
				//cerr << "WAIT_ABANDONED" << endl;
				break;
				//default:
					//cerr << "Someting's unexpected was returned.";
			}
		}

		//PAssword
		printf("\nEnter password: ");
		stdinHandle = GetStdHandle(STD_INPUT_HANDLE);
		pos = 0;
		char pass[31];
		run = true;
		while (run)
		{
			switch (WaitForSingleObject(stdinHandle, INFINITE))
			{
			case(WAIT_TIMEOUT) :
				//cerr << "timeout" << endl;
				break; // return from this function to allow thread to terminate
			case(WAIT_OBJECT_0) :
				if (_kbhit()) // _kbhit() always returns immediately
				{
					int i = _getch();
					//cerr << "key: " << i << endl;
					//printf("%d", i); //To character
					if (i == 13) {
						run = false;
						break;
					}
					else if (i == 8) {
						pos -= 1;
						if (pos < 0) {
							pos = 0;
						}
						else {
							pass[pos] = '\0';
							COORD cursor = Util::GetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE));
							cursor.X -= pos + 1;
							SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), cursor);
							for (int i = 0; i < pos + 1; i++)
								printf(" ");
							SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), cursor);
							for (int i = 0; i < pos; i++)
								printf("*");
						}
					}
					else if (pos < 30) {
						pass[pos] = (char)i;
						pass[pos + 1] = '\0';
						pos++;
						COORD cursor = Util::GetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE));
						cursor.X -= pos - 1;
						SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), cursor);
						for (int i = 0; i < pos; i++)
							printf("*");
					}
				}
				else // some sort of other events , we need to clear it from the queue
				{
					// clear events
					INPUT_RECORD r[512];
					DWORD read;
					ReadConsoleInput(stdinHandle, r, 512, &read); //Not needed
																  //cerr << "mouse event" << endl;
				}
								break;
			case(WAIT_FAILED) :
				//cerr << "WAIT_FAILED" << endl;
				break;
			case(WAIT_ABANDONED) :
				//cerr << "WAIT_ABANDONED" << endl;
				break;
				//default:
				//cerr << "Someting's unexpected was returned.";
			}
		}

		printf("\nLogging in...\n");
		struct string s;
		init_string(&s);
		CURL *curl;
		struct curl_httppost *formpost = NULL;
		struct curl_httppost *lastptr = NULL;
		CURLcode res;
		curl = curl_easy_init();
		if (curl) {
			curl_formadd(&formpost,
				&lastptr,
				CURLFORM_COPYNAME, "apikey",
				CURLFORM_COPYCONTENTS, "YOURAPIKEYHERE",
				CURLFORM_END);
			curl_formadd(&formpost,
				&lastptr,
				CURLFORM_COPYNAME, "username",
				CURLFORM_COPYCONTENTS, username,
				CURLFORM_END);
			curl_formadd(&formpost,
				&lastptr,
				CURLFORM_COPYNAME, "password",
				CURLFORM_COPYCONTENTS, pass,
				CURLFORM_END);
			curl_easy_setopt(curl, CURLOPT_URL, "https://freebrew.org/api.php?action=login");
			int code;
			//curl_easy_setopt(curl, CURLOPT_POST, 1);
			//curl_easy_setopt(curl, CURLOPT_POSTFIELDS, "apikey=YOURAPIKEYHERE&username=Mr. Chip53&password=Bobbers851");
			curl_easy_setopt(curl, CURLOPT_HTTPPOST, formpost);
			code = curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, writefunc);
			if (code != CURLE_OK) {
				//fprintf(stderr, "Failed to set writer [%s]\n", errorBuffer);
				return -1;
			}
			curl_easy_setopt(curl, CURLOPT_WRITEDATA, &s);
			int res = curl_easy_perform(curl);
			curl_easy_cleanup(curl);
			curl_formfree(formpost);
			/* free slist 
			cJSON * root = cJSON_Parse(s.ptr);
			bool loggedIn = cJSON_GetObjectItem(root, "Success")->valueint;
			bool isPrem = cJSON_GetObjectItem(root, "Premium")->valueint;
#ifdef _DEBUG
			isPrem = true;
			loggedIn = true;
#endif
			if (loggedIn) {
				if (isPrem)
					printf("Login success!");
				else {
					printf("You are not a BrewBot subscriber!");
					Sleep(3000);
					HWND hwnd = GetConsoleWindow();
					FreeConsole();
					PostMessage(hwnd, WM_CLOSE, 0, 0);
					return 0;
				}
			}
			else {
				printf("Login failed.\n");
				goto login;
			}
			cJSON_Delete(root);
		}
		else {
			printf("Failed to login!");
		}
		//Load bot
		printf("\nLoading bot...");
		Sleep(1000);
		HWND hwnd = GetConsoleWindow();
		FreeConsole();
		PostMessage(hwnd, WM_CLOSE, 0, 0);
		curl_global_cleanup();
		HMODULE coreDLL = LoadLibraryA("CoreDLL.dll");
		if (coreDLL) {
			initUser iu = (initUser)GetProcAddress(coreDLL, "initUser");
			(*iu)(username, pass);
			winMain iwp = (winMain)GetProcAddress(coreDLL, "wWinMain");
			int ret = (*iwp)(hInstance, hPrevInstance, lpCmdLine, nCmdShow);
			FreeLibrary(coreDLL);
			return ret;
		}
		return 0;
	}
}*/



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEXW wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = WndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInstance;
	wcex.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_BEER));
	wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszMenuName = NULL;
	wcex.lpszClassName = szWindowClass;
	wcex.hIconSm = NULL;

	return RegisterClassExW(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
	hInst = hInstance; // Store instance handle in our global variable

	HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW ^ (WS_MAXIMIZEBOX | WS_THICKFRAME) | WS_CAPTION | WS_SYSMENU,
		CW_USEDEFAULT, CW_USEDEFAULT, 400, 145, nullptr, nullptr, hInstance, nullptr);

	//HWND hWnd = CreateWindowExW(WS_EX_LAYERED, szWindowClass, szTitle, WS_POPUP | WS_SYSMENU,
	//	CW_USEDEFAULT, CW_USEDEFAULT, 400, 145, nullptr, nullptr, hInstance, nullptr);

	if (!hWnd)
	{
		return FALSE;
	}

	/*CImage splashImage;
	//splashImage.LoadFromResource(hInstance, IDB_SPLASHPNG);
	CreateStream
	//splashImage.Load("Splash.png");
	// Get dimensions
	int iWidth = splashImage.GetWidth();
	int iHeight = splashImage.GetHeight();
	// Make mem DC + mem  bitmap
	HDC hdcScreen = GetDC(NULL);
	HDC hDC = CreateCompatibleDC(hdcScreen);
	HBITMAP hBmp = CreateCompatibleBitmap(hdcScreen, iWidth, iHeight);
	HBITMAP hBmpOld = (HBITMAP)SelectObject(hDC, hBmp);
	// Draw image to memory DC
	splashImage.Draw(hDC, 0, 0, iWidth, iHeight, 0, 0, iWidth, iHeight);

	// Call UpdateLayeredWindow
	BLENDFUNCTION blend = { 0 };
	blend.BlendOp = AC_SRC_OVER;
	blend.SourceConstantAlpha = 255;
	blend.AlphaFormat = AC_SRC_ALPHA;
	POINT ptPos = { 0, 0 };
	SIZE sizeWnd = { iWidth, iHeight };
	POINT ptSrc = { 0, 0 };
	UpdateLayeredWindow(hWnd, hdcScreen, &ptPos, &sizeWnd, hDC, &ptSrc, 0, &blend, ULW_ALPHA);

	SelectObject(hDC, hBmpOld);
	DeleteObject(hBmp);
	DeleteDC(hDC);
	ReleaseDC(NULL, hdcScreen);*/

	RECT rc;

	GetWindowRect(hWnd, &rc);

	int xPos = (GetSystemMetrics(SM_CXSCREEN) - rc.right) / 2;
	int yPos = (GetSystemMetrics(SM_CYSCREEN) - rc.bottom) / 2;

	SetWindowPos(hWnd, 0, xPos, yPos, 0, 0, SWP_NOZORDER | SWP_NOSIZE);

	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	//CreateThread(0, 0, loadBotsThread, (LPVOID)hWnd, 0, NULL);

	return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_CREATE:
	{
		//Create username text
		HWND userBox = CreateWindowExW(WS_EX_CLIENTEDGE, L"Edit", L"Username", WS_TABSTOP | WS_CHILD | WS_VISIBLE | ES_LEFT, 10, 10, 365, 25, hWnd, (HMENU)IDC_USER_BOX, GetModuleHandle(NULL), NULL);
		HWND passBox = CreateWindowExW(WS_EX_CLIENTEDGE, L"Edit", L"Password", WS_TABSTOP | WS_CHILD | WS_VISIBLE | ES_PASSWORD | ES_LEFT, 10, 45, 365, 25, hWnd, (HMENU)IDC_PASS_BOX, GetModuleHandle(NULL), NULL);
		HWND hWndLoginButton = CreateWindowExW(NULL, L"BUTTON", L"Login", WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON, 10, 75, 365, 25, hWnd, (HMENU)IDC_LOGIN_BTN, GetModuleHandle(NULL), NULL);
	}
	break;
	case WM_COMMAND:
	{
		int wmId = LOWORD(wParam);
		int wmEvent = HIWORD(wParam);
		// Parse the menu selections:
		switch (wmId)
		{
		case IDC_LOGIN_BTN:
		{
#ifdef _DEBUG
			HMODULE coreDLL = LoadLibraryA("CoreDLL.dll");
			if (coreDLL == NULL)
				auto err = GetLastError();
			if (coreDLL) {
				initUser iu = (initUser)GetProcAddress(coreDLL, "initUser");
				(*iu)("", "");
				winMain iwp = (winMain)GetProcAddress(coreDLL, "wWinMain");
				DestroyWindow(hWnd);
				int ret = (*iwp)(NULL, NULL, NULL, nCmd);
				//FreeLibrary(coreDLL);
				//return ret;
			}
#else
			struct string s;
			init_string(&s);
			CURL *curl;
			struct curl_httppost *formpost = NULL;
			struct curl_httppost *lastptr = NULL;
			CURLcode res;
			curl = curl_easy_init();
			if (curl) {
				char username[1024];
				GetWindowTextA(GetDlgItem(hWnd, IDC_USER_BOX), username, 1024);
				char pass[1024];
				GetWindowTextA(GetDlgItem(hWnd, IDC_PASS_BOX), pass, 1024);
				curl_formadd(&formpost,
					&lastptr,
					CURLFORM_COPYNAME, "apikey",
					CURLFORM_COPYCONTENTS, "YOURAPIKEYHERE",
					CURLFORM_END);
				curl_formadd(&formpost,
					&lastptr,
					CURLFORM_COPYNAME, "username",
					CURLFORM_COPYCONTENTS, username,
					CURLFORM_END);
				curl_formadd(&formpost,
					&lastptr,
					CURLFORM_COPYNAME, "password",
					CURLFORM_COPYCONTENTS, pass,
					CURLFORM_END);
				curl_easy_setopt(curl, CURLOPT_URL, "https://freebrew.org/api.php?action=login");
				int code;
				//curl_easy_setopt(curl, CURLOPT_POST, 1);
				//curl_easy_setopt(curl, CURLOPT_POSTFIELDS, "apikey=YOURAPIKEYHERE&username=Mr. Chip53&password=Bobbers851");
				curl_easy_setopt(curl, CURLOPT_HTTPPOST, formpost);
				code = curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, writefunc);
				if (code != CURLE_OK) {
					//fprintf(stderr, "Failed to set writer [%s]\n", errorBuffer);
					return -1;
				}
				curl_easy_setopt(curl, CURLOPT_WRITEDATA, &s);
				int res = curl_easy_perform(curl);
				curl_easy_cleanup(curl);
				curl_formfree(formpost);
				// free slist 
				cJSON * root = cJSON_Parse(s.ptr);
				bool loggedIn = cJSON_GetObjectItem(root, "Success")->valueint;
				bool isPrem = cJSON_GetObjectItem(root, "Premium")->valueint;
				curl_global_cleanup();
				if (loggedIn) {
					MessageBoxA(NULL, "Logged in", "Logged in", NULL);
					HMODULE coreDLL = LoadLibraryA("CoreDLL.dll");
					if (coreDLL) {
						initUser iu = (initUser)GetProcAddress(coreDLL, "initUser");
						(*iu)(username, pass);
						winMain iwp = (winMain)GetProcAddress(coreDLL, "wWinMain");
						DestroyWindow(hWnd);
						int ret = (*iwp)(NULL, NULL, NULL, nCmd);
						//FreeLibrary(coreDLL);
						//return ret;
					}
				}
				else {
					MessageBoxA(NULL, "Login Failed", "Login Failed", NULL);
				}
			}
#endif
		}
		break;
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
	}
	break;
	case WM_PAINT:
	{
		PAINTSTRUCT ps;
		HDC hdc = BeginPaint(hWnd, &ps);
		// TODO: Add any drawing code that uses hdc here...
		EndPaint(hWnd, &ps);
	}
	break;
	case WM_DESTROY:
		//PostQuitMessage(0);
		//DestroyWindow(hWnd);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}
