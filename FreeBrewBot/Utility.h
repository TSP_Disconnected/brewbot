#pragma once
class Util
{
public:
	//Get working directories
	static TCHAR *getCurrentDirectoryW();
	static char *getCurrentDirectoryA();
	static COORD GetConsoleCursorPosition(HANDLE hConsoleOutput);
	static std::string getHash(TCHAR* filename);
};

