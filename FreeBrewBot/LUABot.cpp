#include "stdafx.h"
#include "LUABot.h"
#include <luabind\luabind.hpp>
#include "ADB.h"
#include "Controls.h"
#include <sstream>

void msgbox(const char* str) {
	MessageBoxA(NULL, str, str, NULL);
}

LUABot::LUABot(char* script, HWND mh)
{
	mainHwnd = mh;

	lua_State *L = luaL_newstate();

	luaopen_base(L);

	luabind::open(L);

	luabind::module(L, "BrewBot")[
		luabind::class_<ADB>("ADB")
			.def(luabind::constructor<>())
			.def("Clear", &ADB::Clear)
			.def("Click", &ADB::Click)
			.def("startApp", &ADB::startApp)
			.def("disconnectADB", &ADB::disconnectADB),
		luabind::class_<LUABot>("Bot"),
		luabind::namespace_("Controls")[
			luabind::def("createLabel", &Controls::createLabel)
		]
	];

	luaL_dofile(L, script);

	try {
		int ret = luabind::call_function<int>(L, "loop", this);
	}
	catch (luabind::error &e) {
		OutputDebugStringA(lua_tostring(L, -1));
	}

	//luaL_dofile(L, script);

	/* cleanup Lua */
	lua_close(L);
}


LUABot::~LUABot()
{
}

HWND LUABot::getMainWindow(void)
{
	return mainHwnd;
}