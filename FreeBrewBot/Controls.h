#pragma once
#include "LUABot.h"

class Controls
{
public:
	Controls();
	~Controls();
	static void createButton();
	static void createLabel(LUABot* bot, const char* label, int x, int y, int width, int height, int controlID);
	static void createRichEdit();
	static void createHorzSeperator();
	static void createTabControl();
};

