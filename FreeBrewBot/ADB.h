#pragma once

#ifndef ADB_H_
#define ADB_H_

class ADB {

	HANDLE adbInRead;
	HANDLE adbInWrite;
	HANDLE adbOutRead;
	HANDLE adbOutWrite;

	bool sendADB(char* cmd, int size);
	void closeADB();
	bool connectADB();
	bool initADB();

public:
	ADB();
	~ADB();
	void Clear();
	void Click(int x, int y);
	void startApp(const char* package);
	static void disconnectADB();
};

#endif